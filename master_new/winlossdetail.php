<?php
@session_start();
include ("../database.php");
$data = new database();
$ma = $_SESSION['ma'];
if (isset($_REQUEST['ma'])) {
    $ma = $_REQUEST['ma'];
}
$level = strlen($ma);
$today = date("m/d/Y");
$todaysql = date("Y-m-d");
$date = date('Y-m-d');
$new_date = strtotime('-3 month', strtotime($date));
$new_date = date('Ymd', $new_date);
$min = ($new_date - date('d')) + 1;
$new_date = strtotime('-0 day', strtotime($date));
$new_date = date('Ymd', $new_date);
$max1 = $new_date;
$new_date = strtotime('-1 day', strtotime($date));
$new_date = date('Y-m-d', $new_date);
$dau = $new_date;
$new_date = strtotime('-1 day', strtotime($date));
$new_date = date('m/d/Y', $new_date);
$yes = $new_date;
date_default_timezone_set("Asia/Bangkok");
if (date("H") > 11) {
    $_REQUEST['giua'] = date("m/d/Y");
} else {
    $_REQUEST['giua'] = date("m/d/Y", strtotime("-1 day"));
}
if (isset($_REQUEST['dau'])) {
    $nn = explode("/", $_REQUEST['dau']);
    $nnn = explode("/", $_REQUEST['cuoi']);
    $dau = $nn[2] . "-" . $nn[0] . "-" . $nn[1];
    $cuoi = $nnn[2] . "-" . $nnn[0] . "-" . $nnn[1];
} else
    if (isset($_REQUEST['giua'])) {
        $nn = explode("/", $_REQUEST['giua']);
        $dau = $nn[2] . "-" . $nn[0] . "-" . $nn[1];
        $cuoi = $nn[2] . "-" . $nn[0] . "-" . $nn[1];
    } else {
        $cuoi = date("Y-m-d");

    }
$nd = explode("-", $dau);
$nc = explode("-", $cuoi);
$hd = $nd[1] . "/" . $nd[2] . "/" . $nd[0];
$hc = $nc[1] . "/" . $nc[2] . "/" . $nc[0];
$tn = substr($min, 4, 2) . "/" . substr($min, 6, 2) . "/" . substr($min, 0, 4);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Win Loss Detail Report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/bundles/common/common.min.css?v=453" rel="stylesheet" type="text/css">
    <link href="../assets/bundles/site-reports/default.min.css?v=1.0.6493.20440" rel="stylesheet" type="text/css">
    <link href="../assets/bundles/default.min.css?v=1.0.6493.20440" rel="stylesheet" type="text/css">

</head>
<body>


<div class="page-title">
    <span class="pull-left">Chi tiết thắng thua của <?php echo $level == 2 ? 'Master' : $level == 4 ? 'Agent' : 'Member'; ?></span>
    <div class="list-icon">
        <ul>
            <li class="showfilter-element"><span class="icon-arrow-up-drop-circle icon-filter" title="Ẩn bộ lọc dữ liệu"></span></li>
            <li class="hidefilter-element"><span class="icon-arrow-down-drop-circle icon-filter" title="Hiện bộ lọc dữ liệu"></span></li>

            <li class="normalview-element"><span id="icon-stack-column" class="icon-stack-column icon-switchmode" title="Nh&#243;m c&#225;c cột"></span></li>
            <li class="stackcolumnview-element"><span id="icon-normal-column" class="icon-normal-column icon-switchmode" title="T&#225;ch c&#225;c cột"></span></li>
        </ul>
    </div>
</div>


<form class="form-inline filter  " role="form" id="report-form">
    <input id="IsFilterVisible" name="IsFilterVisible" type="hidden" value="true" /><input data-val="true" data-val-required="The IsHistoricReport field is required." id="IsHistoricReport" name="IsHistoricReport" type="hidden" value="False" /><input data-val="true" data-val-required="The Boolean field is required." id="IsStackMode" name="IsStackMode" type="hidden" value="False" /><input data-val="true" data-val-required="The Boolean field is required." id="IsDescendingSort" name="IsDescendingSort" type="hidden" value="False" /><input id="SortPropertyName" name="SortPropertyName" type="hidden" value="CustName" /><input data-val="true" data-val-number="The field CustId must be a number." data-val-required="The CustId field is required." id="CustId" name="CustId" type="hidden" value="19978374" /><input id="CustName" name="CustName" type="hidden" value="TY28A2" /><input id="BreadcrumbLevelJson" name="BreadcrumbLevelJson" type="hidden" value="[{&#39;CustId&#39;:19978374,&#39;CustName&#39;:&#39;TY28A2&#39;,&#39;CustLevelId&#39;:3}]" /><input id="CustLevelId" name="CustLevelId" type="hidden" value="3" />

    <div class="form-group">
        <select id="dropdown-product"
                data-has-any-product="True"
                data-label-select-products="Lựa chọn sản phẩm"
                data-label-selected="Đ&#227; chọn"
                data-label-all-products="Tất cả sản phẩm"
                data-label-there-is-no-product="Kh&#244;ng c&#243; sản phẩm n&#224;o"
                data-language="vi-VN"
                multiple="multiple" class="select-multiple">
            <option value="1"  selected=&quot;selected&quot;>Sportsbook</option>
            <option value="2"  selected=&quot;selected&quot;>Racing</option>
            <option value="3"  selected=&quot;selected&quot;>Number Game</option>
            <option value="5"  selected=&quot;selected&quot;>Virtual Sports</option>
            <option value="6"  selected=&quot;selected&quot;>Casino</option>
            <option value="8"  selected=&quot;selected&quot;>Keno</option>
            <option value="12"  selected=&quot;selected&quot;>Gold Deluxe</option>
            <option value="21"  selected=&quot;selected&quot;>Allbet</option>
            <option value="22"  selected=&quot;selected&quot;>Macau Games</option>
            <option value="23"  selected=&quot;selected&quot;> L&#244; Đề </option>
            <option value="24"  selected=&quot;selected&quot;>Bắn c&#225;</option>
            <option value="28"  selected=&quot;selected&quot;>Xổ số</option>
        </select>
        <input type="hidden" name="UserSelectedProductIds" id="product-ids" value="1,2,3,5,6,8,12,21,22,23,24,28" />
    </div>


    <div id="daterange-picker"
         class="daterange-picker"
         data-currentdate="10/14/2017 12:00:00 AM"
         data-mindate="07/01/2017"
         data-maxdate="12/31/9999">
    </div>
    <div class="form-group">
        <label>Từ</label>

        <div class="control date">
            <div id="from-date-container"
                 class="input-group date fromdate">
                <input type="text"
                       id="from-date"
                       name="dau"
                       readonly="readonly"
                       autocomplete="off"
                       class="ui_nexdatepicker_datetextbox ui_nexdatepicker_fromdate form-control"
                       value="<?php echo $hd; ?>" />
                <span class="input-group-addon">
                    <span class="icon-calendar" id="fdate_trigger"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>Đến</label>

        <div class="control date">
            <div id="to-date-container"
                 class="input-group date todate">
                <input type="text"
                       id="to-date"
                       name="cuoi"
                       readonly="readonly"
                       autocomplete="off"
                       class="ui_nexdatepicker_datetextbox ui_nexdatepicker_todate form-control"
                       value="<?php echo $hc; ?>" />
                <span class="input-group-addon">
                    <span class="icon-calendar" id="tdate_trigger"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <input type="submit" value="X&#225;c nhận" class="btn-submit" />
    </div>
    <div class="form-group daterange-container">
        <div class="dropdown daterange-dropdown" id="dropdown-daterange">
            <input type="hidden" name="UserSelectedDateRangeOption" id="hidden-range-option" value="0" />
            <button class="btn-submit dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span id="range-label">H&#244;m nay</span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="range-label">
                <li><a href="#" class="range-option" data-value="0">H&#244;m nay</a></li>
                <li><a href="#" class="range-option" data-value="1">H&#244;m qua</a></li>
                <li><a href="#" class="range-option" data-value="2">Tuần n&#224;y</a></li>
                <li><a href="#" class="range-option" data-value="3">Tuần trước</a></li>
                <li><a href="#" class="range-option" data-value="4">Th&#225;ng n&#224;y</a></li>
                <li><a href="#" class="range-option" data-value="5">Th&#225;ng trước</a></li>
                <li><a href="#" class="range-option" data-value="6">Từ th&#225;ng trước</a></li>
                <li><a href="#" class="range-option" data-value="7">Ng&#224;y ri&#234;ng biệt</a></li>
            </ul>
        </div>
    </div>



    <div class="form-group">
        <span id="icon-information" class="icon-information-outline"></span>
    </div>
    <div class="form-group">
        <span id="icon-excel" class="icon-excel" title="Xuất sang Excel"></span>
    </div>

    <div class="note">
        <span class="star">*</span>Bạn c&#243; thể tra cứu dữ liệu b&#225;o c&#225;o từ ng&#224;y 07/01/2017.Nếu muốn t&#236;m kiếm những dữ liệu cũ hơn, bấm <a id="report-switch" data-is-historic-report="false" href="#">Xem b&#225;o c&#225;o trước đ&#243;</a>
    </div>
</form>



<table class="tblRpt tblRpt-bordered alternative-row-table width-100per tablesorter tblRpt-hover
       " id="tbl-report"
       data-totalrecords="12"
       data-custid="19978374"
       data-sortedcolumn="CustName"
       data-descendingsort="False">
    <thead>
    <tr>
        <th class="sorting" rowspan="2" data-sortedname="CustName" data-colindex="0">T&#234;n đăng nhập</th>
        <th class="sorting" rowspan="2" data-sortedname="BetCount" data-colindex="1">Số lượng cược</th>
        <th rowspan="2" class="hidden-stack-mode sorting" data-sortedname="Turnover" data-colindex="2">Tiền Cược</th>
        <th rowspan="2" class="hidden-stack-mode col-netturnover-header sorting" data-sortedname="NetTurnover" data-colindex="3">Tiền cược thực</th>
        <th rowspan="2" class="hidden-stack-mode sorting" data-sortedname="GrossComm" data-colindex="4">Tổng hoa hồng</th>
        <th rowspan="2" class="visible-stack-mode col-netturnover-header col-combine sorting" data-sortedname="Turnover" data-colindex="2">
            <span class="prior-sortedcolumn">Tiền Cược</span>/Tiền cược thực/Tổng hoa hồng
        </th>

        <th colspan="3" class="col-level hidden-stack-mode" data-disabledsort="true">Member</th>
        <th class="visible-stack-mode col-combine sorting" data-sortedname="MemberNetWinLoss.WinLoss" rowspan="2" data-colindex="3">
            Member
            <br />
            (<span class="prior-sortedcolumn">Thắng thua</span>/Hoa hồng/Tổng cộng)
        </th>

        <th colspan="3" class="col-level hidden-stack-mode" data-disabledsort="true">Agent</th>
        <th class="visible-stack-mode col-combine sorting" data-sortedname="AgentNetWinLoss.WinLoss" rowspan="2" data-colindex="4">
            Agent
            <br />
            (<span class="prior-sortedcolumn">Thắng thua</span>/Hoa hồng/Tổng cộng)
        </th>

        <th colspan="3" class="col-level hidden-stack-mode" data-disabledsort="true">Master</th>
        <th class="visible-stack-mode col-combine sorting" data-sortedname="MasterNetWinLoss.WinLoss" rowspan="2" data-colindex="5">
            Master
            <br />
            (<span class="prior-sortedcolumn">Thắng thua</span>/Hoa hồng/Tổng cộng)
        </th>


        <th class="sorting hidden-stack-mode" rowspan="2" data-sortedname="Company" data-colindex="14">Company</th>
        <th class="sorting visible-stack-mode" rowspan="2" data-sortedname="Company" data-colindex="6">Company</th>
    </tr>
    <tr class="thead-row2">
        <th class="hidden-stack-mode col-winloss sorting" data-sortedname="MemberNetWinLoss.WinLoss" data-colindex="5">Thắng thua</th>
        <th class="hidden-stack-mode sorting" data-sortedname="MemberNetWinLoss.Comm" data-colindex="6">Hoa hồng</th>
        <th class="hidden-stack-mode sorting" data-sortedname="MemberNetWinLoss.Total" data-colindex="7">Tổng cộng</th>

        <th class="hidden-stack-mode col-winloss sorting" data-sortedname="AgentNetWinLoss.WinLoss" data-colindex="8">Thắng thua</th>
        <th class="hidden-stack-mode sorting" data-sortedname="AgentNetWinLoss.Comm" data-colindex="9">Hoa hồng</th>
        <th class="hidden-stack-mode sorting" data-sortedname="AgentNetWinLoss.Total" data-colindex="10">Tổng cộng</th>
        <th class="hidden-stack-mode col-winloss sorting" data-sortedname="MasterNetWinLoss.WinLoss" data-colindex="11">Thắng thua</th>
        <th class="hidden-stack-mode sorting" data-sortedname="MasterNetWinLoss.Comm" data-colindex="12">Hoa hồng</th>
        <th class="hidden-stack-mode sorting" data-sortedname="MasterNetWinLoss.Total" data-colindex="13">Tổng cộng</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $dauCache=$dau;
    $cuoiCache=$cuoi;
    $maSup=$ma;
    //$cacheFile = "cache/chi_tiet_thang_thua_cache_$dauCache_$cuoiCache.php";
    $cacheFile = "cache/chi_tiet_thang_thua_cache_cua_".$maSup."_".$dauCache."_".$cuoiCache.".php";
    $time_update_cache =10000; // milisecond(s)
    $remove=true;
    //if((file_exists($cacheFile)) && (time() <= (fileatime($cacheFile) + $time_update_cache) )){
    if($remove==false){
        $content=file_get_contents($cacheFile);
        echo $content;
    }else{

    ?>
    <?php
    //ob_start();
    //$_SESSION["dauCache"]=$hd;
    //$_SESSION["cuoiCache"]=$hc;
    //$datamb = $data->ExcuteObjectList("select * from master where Ma_sup = '$ma' order by Ma asc");
    //$dataag = $data->ExcuteObjectList("select * from sup_master where Ma = '$ma' ");

    $datamb = $data->ExcuteObjectList("select * from agent where Ma_mas = '$ma' order by Ma asc");
    $dataag = $data->ExcuteObjectList("select * from master where Ma = '$ma' order by Ma asc");
    $hhag = $dataag[0]['Hoa_hong'];
    $slmb = count($datamb);
    $allve = 0;
    $allcuoc = 0;
    $allgc = 0;
    $alltt = 0;
    $allhh = 0;
    $slpage=5;
    $tongpage=intval($slmb/$slpage);
    //$slmbpage=$slmb/5;
    $next=0;
    $last=0;
    if(isset($_REQUEST["ispage"])){
        $requestPage=$_REQUEST["ispage"];
        if($requestPage==1){
            $next=0;
            $last=$slpage+1;
        }else{
            $last=$requestPage * ($slpage + 1);
            $next=$last-($slpage+1);
        }
    }else{
        //default
        $next=0;
        //$last=$slpage+1;
        $last=$slmb;
    }
    for ($i = $next; $i < $last; $i++) {
    $row = $datamb[$i];
    $so = 0;
    $mb = $row['Ma'];
    $hhmb = $row['Hoa_hong'];
    $datablmy = $data->ExcuteObjectList("select * from betlistmy where ma_nguoi_dung like '$mb%' and  tinh_trang = '1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' order by ma_bet desc ");
    $datablcx = $data->ExcuteObjectList("select * from betlistchinhxac where ma_nguoi_dung like '$mb%' and  tinhtrang = '1' and ngay >= '$dau' and ngay <= '$cuoi' order by id desc ");
    $datablcl = $data->ExcuteObjectList("select * from betlistchanle where ma_nguoi_dung like '$mb%' and  tinhtrang = '1' and ngay >= '$dau' and ngay <= '$cuoi' order by id desc ");
    $slbl = 0;
    $sqlth = "select * from betlistth where ma_nguoi_dung like '$mb%'  and tinh_trang='1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' order by ma_bet DESC ";

    $du_lieu1 = $data->ExcuteObjectList($sqlth);
    $sqlntren = "select * from betlistnumtren where ma_nguoi_dung like '$mb%' and tinh_trang='1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' order by id_bet desc";
    $sqlnduoi = "select * from betlistnumduoi where ma_nguoi_dung like  '$mb%' and tinh_trang='1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' order by id_bet desc";
    $dtnduoi = $data->ExcuteObjectList($sqlnduoi);
    $dtntren = $data->ExcuteObjectList($sqlntren);
    $slnduoi = count($dtnduoi);
    $slntren = count($dtntren);
    $slth = count($du_lieu1);
    $slblmy = count($datablmy);
    $slcx = count($datablcx);
    $slcl = count($datablcl);
    $ve = $slblmy + $slcl + $slcx + $slth + $slnduoi + $slntren;
    if($ve == 0 ){
        continue;
    }
    $allve += $ve;
    //echo $ve."<br>";
    $cuoc = 0;
    $gc = 0;
    $tt = 0;
    $hh = 0;
    // union
    $sql = "select ma_nguoi_dung from betlistmy where ma_nguoi_dung like '$mb%' and  tinh_trang = '1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' group by ma_nguoi_dung 
union 
select ma_nguoi_dung from betlistchinhxac where ma_nguoi_dung like '$mb%' and  tinhtrang = '1' and ngay >= '$dau' and ngay <= '$cuoi' group by ma_nguoi_dung 
union 
select ma_nguoi_dung from betlistchanle where ma_nguoi_dung like '$mb%' and  tinhtrang = '1' and ngay >= '$dau' and ngay <= '$cuoi' group by ma_nguoi_dung 
union 
select ma_nguoi_dung from betlistth where ma_nguoi_dung like '$mb%'  and tinh_trang='1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' group by ma_nguoi_dung 
union 
select ma_nguoi_dung from betlistnumtren where ma_nguoi_dung like '$mb%' and tinh_trang='1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' group by ma_nguoi_dung 
union 
select ma_nguoi_dung from betlistnumduoi where ma_nguoi_dung like '$mb%' and tinh_trang='1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' group by ma_nguoi_dung";

    $dataso = $data->ExcuteObjectList($sql);
    $so = count($dataso);
    $sqlCountConditionMember="select ma_nguoi_dung,count(so_luong_ve) as slv,sum(tien_go) as tg from(
select ma_nguoi_dung ,ma_bet as so_luong_ve,so_tien as tien_go from betlistmy where ma_nguoi_dung like '$mb%' and  tinh_trang = '1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' 
union all
select ma_nguoi_dung,id as so_luong_ve,so_tien as tien_go from betlistchinhxac where ma_nguoi_dung like '$mb%' and  tinhtrang = '1' and ngay >= '$dau' and ngay <= '$cuoi' 
union all
select ma_nguoi_dung,ma_bet as so_luong_ve,so_tien as tien_go from betlistth where ma_nguoi_dung like '$mb%'  and tinh_trang='1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' 
union  all
select ma_nguoi_dung,id_bet as so_luong_ve,so_tien as tien_go from betlistnumtren where ma_nguoi_dung like '$mb%' and tinh_trang='1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' 
union all
select ma_nguoi_dung,id_bet as so_luong_ve,so_tien as tien_go from betlistnumduoi where ma_nguoi_dung like '$mb%' and tinh_trang='1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' 
) t
group by ma_nguoi_dung HAVING slv >=20 OR tg > 1000";
    //file_put_contents("cau_sql.txt",$sql,FILE_APPEND);
    $dataCountConditionMember = $data->ExcuteObjectList($sqlCountConditionMember);
    $soCountConditionMember = count($dataCountConditionMember);
    $bet = $data->ExcuteObjectList("select * from betlistnumtren where ma_nguoi_dung like '$mb%' and  tinh_trang = '1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' order by id_bet desc ");

    for ($k = 0; $k < count($bet); $k++) {
        $r = $bet[$k];
        $nguoidung = $r['ma_nguoi_dung'];
        $datahh = $data->ExcuteObjectList("select * from member where Ma = '$nguoidung'");
        $hhmb = $datahh[0]['Hoa_hong'];
        $tilecuoc = $bet[$k]['tile'];
        $tien = $bet[$k]['so_tien'];
        $cuoc += $tien;
        $tenhienthitran = "Điểm Số Chính Xác";
        switch ($bet[$k]['win']) {

            case 'thang':

                $win = 'Đã Thắng';
                $tt += $tien * ($tilecuoc);
                $hh += round((float)$tien * $hhmb, 2);
                $gc += $r['so_tien'] * $hhag;
                break;


            case 'thua':

                $win = 'Đã Thua';
                $tt += round((float)$tien * (-1), 2);
                $hh += round((float)$tien * $hhmb, 2);
                $gc += $r['so_tien'] * $hhag;
                break;

        }
        //if($r['ma_nguoi_dung'] == "ADTH11002")
        //  echo "CX - ".$k." ".$tt."<br>";
    }
    $bet = $data->ExcuteObjectList("select * from betlistnumduoi where ma_nguoi_dung like '$mb%' and  tinh_trang = '1' and ngay_bet >= '$dau' and ngay_bet <= '$cuoi' order by id_bet desc ");

    for ($k = 0; $k < count($bet); $k++) {
        $r = $bet[$k];
        $nguoidung = $r['ma_nguoi_dung'];
        $datahh = $data->ExcuteObjectList("select * from member where Ma = '$nguoidung'");
        $hhmb = $datahh[0]['Hoa_hong'];
        $tilecuoc = $bet[$k]['tile'];
        $tien = $bet[$k]['so_tien'];
        $tenhienthitran = "Điểm Số Chính Xác";
        $cuoc += $tien;
        switch ($bet[$k]['win']) {

            case 'thang':

                $win = 'Đã Thắng';
                $tt += $tien * ($tilecuoc);
                $hh += round((float)$tien * $hhmb, 2);
                $gc += $r['so_tien'] * $hhag;
                break;


            case 'thua':

                $win = 'Đã Thua';
                $tt += round((float)$tien * (-1), 2);
                $hh += round((float)$tien * $hhmb, 2);
                $gc += $r['so_tien'] * $hhag;
                break;

        }
        //if($r['ma_nguoi_dung'] == "ADTH11002")
        //  echo "CX - ".$k." ".$tt."<br>";
    }
    for ($k = 0; $k < $slblmy; $k++) {
        $r = $datablmy[$k];
        $nguoidung = $r['ma_nguoi_dung'];
        $datahh = $data->ExcuteObjectList("select * from member where Ma = '$nguoidung'");
        $hhmb = $datahh[0]['Hoa_hong'];
        if ($r["ma_nha"] != "") {
            $tilecuoc = $r["ma_nha"];
        }
        if ($r["ma_khach"] != "") {
            $tilecuoc = $r["ma_khach"];
        }
        if ($r["ma_tren"] != "") {
            $tilecuoc = $r["ma_tren"];
        }
        if ($r["ma_duoi"] != "") {
            $tilecuoc = $r["ma_duoi"];
        }
        if ($r["ma_1x"] != "") {
            $tilecuoc = $r["ma_1x"] - 1;
        }
        if ($r["ma_2x"] != "") {
            $tilecuoc = $r["ma_2x"] - 1;
        }
        if ($r["ma_3x"] != "") {
            $tilecuoc = $r["ma_3x"] - 1;
        }

        $cuoc += $r['so_tien'];

        if ($tilecuoc > 0) {
            //echo $tilecuoc."<br/>";
            switch ($r['win']) {
                case "thang":
                    $tt += $r['so_tien'] * $tilecuoc;
                    $hh += $r['so_tien'] * $hhmb;
                    $gc += $r['so_tien'] * $hhag;
                    break;
                case "thangnua":
                    $tt += $r['so_tien'] / 2 * $tilecuoc;
                    $hh += $r['so_tien'] / 2 * $hhmb;
                    $gc += $r['so_tien'] / 2 * $hhag;
                    break;
                case "thua":
                    $tt += $r['so_tien'] * (-1);
                    $hh += $r['so_tien'] * $hhmb;
                    $gc += $r['so_tien'] * $hhag;
                    break;
                case "thuanua":
                    $tt += $r['so_tien'] / 2 * (-1);
                    $hh += $r['so_tien'] / 2 * $hhmb;
                    $gc += $r['so_tien'] / 2 * $hhag;
                    break;
                case "hoa":
                    $tt += 0;
                    $hh += 0;
                    $gc += 0;
                    break;
            }
        } else {

            switch ($r['win']) {
                case "thang":
                    $tt += $r['so_tien'];
                    $hh += $r['so_tien'] * $hhmb;
                    $gc += $r['so_tien'] * $hhag;
                    break;
                case "thangnua":
                    $tt += $r['so_tien'] / 2;
                    $hh += $r['so_tien'] / 2 * $hhmb;
                    $gc += $r['so_tien'] / 2 * $hhag;
                    break;
                case "thua":
                    $tt += $r['so_tien'] * $tilecuoc;
                    $hh += $r['so_tien'] * $hhmb;
                    $gc += $r['so_tien'] * $hhag;
                    break;
                case "thuanua":
                    $tt += $r['so_tien'] / 2 * $tilecuoc;
                    $hh += $r['so_tien'] / 2 * $hhmb;
                    $gc += $r['so_tien'] / 2 * $hhag;
                    break;
                case "hoa":
                    $tt += 0;
                    $hh += 0;
                    $gc += 0;
                    break;
            }
        }
    }
    $bet = $data->ExcuteObjectList("select * from betlistchinhxac where ma_nguoi_dung like '$mb%' and  tinhtrang = '1' and ngay >= '$dau' and ngay <= '$cuoi' order by id desc ");

    for ($k = 0; $k < count($bet); $k++) {
        $r = $bet[$k];
        $nguoidung = $r['ma_nguoi_dung'];
        $datahh = $data->ExcuteObjectList("select * from member where Ma = '$nguoidung'");
        $hhmb = $datahh[0]['Hoa_hong'];
        $tilecuoc = $bet[$k]['tilecuoc'];
        $tien = $bet[$k]['so_tien'];
        $tenhienthitran = "Điểm Số Chính Xác";
        switch ($bet[$k]['win']) {

            case 'thang':

                $win = 'Đã Thắng';
                $tt += $tien * ($tilecuoc - 1);
                $hh += round((float)$tien * $hhmb, 2);
                $gc += $r['so_tien'] * $hhag;
                break;


            case 'thua':

                $win = 'Đã Thua';
                $tt += round((float)$tien * (-1), 2);
                $hh += round((float)$tien * $hhmb, 2);
                $gc += $r['so_tien'] * $hhag;
                break;

        }
        //if($r['ma_nguoi_dung'] == "ADTH11002")
        //  echo "CX - ".$k." ".$tt."<br>";
    }

    $bet = $data->ExcuteObjectList("select * from betlistchanle where ma_nguoi_dung like '$mb%' and  tinhtrang = '1' and ngay >= '$dau' and ngay <= '$cuoi' order by id desc ");

    for ($k = 0; $k < count($bet); $k++) {
        $r = $bet[$k];
        $nguoidung = $r['ma_nguoi_dung'];
        $datahh = $data->ExcuteObjectList("select * from member where Ma = '$nguoidung'");
        $hhmb = $datahh[0]['Hoa_hong'];
        $tilecuoc = $bet[$k]['tilecuoc'];
        $tien = $bet[$k]['so_tien'];
        $bet[$i]['lahiep'] == "" ? $tenhienthitran = "Chẵn Lẻ" : $tenhienthitran =
            "Hiệp 1 - Chẵn Lẻ";
        if ($tilecuoc > 0) {
            switch ($bet[$k]['win']) {

                case 'thang':

                    $win = 'Đã Thắng';
                    $tt += $tien * $tilecuoc;
                    $hh += round((float)$tien * $hhmb, 2);
                    $gc += $r['so_tien'] * $hhag;
                    break;


                case 'thua':

                    $win = 'Đã Thua';
                    $tt += round((float)$tien * (-1), 2);
                    $hh += round((float)$tien * $hhmb, 2);
                    $gc += $r['so_tien'] * $hhag;
                    break;

            }
        } else {
            switch ($bet[$k]['win']) {

                case 'thang':

                    $win = 'Đã Thắng';
                    $tt += $tien;
                    $hh += round((float)$tien * $hhmb, 2);
                    $gc += $r['so_tien'] * $hhag;
                    break;


                case 'thua':

                    $win = 'Đã Thua';
                    $tt += round((float)$tien * $tilecuoc * (-1), 2);
                    $hh += round((float)$tien * $hhmb, 2);
                    $gc += $r['so_tien'] * $hhag;
                    break;

            }
        }
    }
    for ($j = 0; $j < $slth; $j++) {

        $r = $du_lieu1[$j];
        $nguoidung = $r['ma_nguoi_dung'];
        $datahh = $data->ExcuteObjectList("select * from member where Ma = '$nguoidung'");
        $hhmb = $datahh[0]['Hoa_hong'];
        $tienvao = $r['so_tien'];
        $tienra = $r['so_du'];
        $thang = $r['win'];
        switch ($thang) {

            case "thang":
                $win = "Đã Thắng";
                $tt += $tienra - $tienvao;
                $hh += round((float)$tienvao * $hhmb, 2);
                $gc += $r['so_tien'] * $hhag;
                break;
            case "thua":
                $win = "Đã Thua";
                $tt += $tienvao * (-1);
                $hh += round((float)$tienvao * $hhmb, 2);
                $gc += $r['so_tien'] * $hhag;
                break;

        }
    }

    $allcuoc += round((float)$cuoc, 2);
    $alltt += round((float)$tt, 2);
    $allhh += round((float)$hh, 2);
    $allgc += round((float)$gc, 2);
    $link = "../master_new/winlossdetail.php?ma=$mb&dau=$hd&cuoi=$hc'";
    ?>
    <tr class="">
        <td data-islink="true" class="text-left col-username">
            <a href="<?php echo $link; ?>"
               class="downline-link"
               data-custname="TY28A200">
                <?php echo $mb  ?>
            </a>
        </td>

        <td class="col-number-of-tickets"><?php echo $ve; ?></td>
        <td class="hidden-stack-mode col-turnover">
            <?php echo number_format($cuoc, 2) ?>
        </td>
        <td class="hidden-stack-mode col-net-turnover">
            <?php echo number_format($cuoc, 2) ?>
        </td>
        <td class="hidden-stack-mode col-gross-com">
            <?php echo number_format($gc, 2) ?>
        </td>
        <td class="hidden-stack-mode col-member-winloss ">
            <?php echo number_format($tt, 2) ?>
        </td>
        <td class="hidden-stack-mode ">
            <?php echo number_format($hh, 2) ?>
        </td>
        <td class="hidden-stack-mode bold ">
            <?php echo number_format(($tt + $hh), 2) ?>
        </td>
        <td class="hidden-stack-mode col-agent-winloss altercol">
            0.00
        </td>
        <td class="hidden-stack-mode altercol">
            0.00
        </td>
        <td class="hidden-stack-mode bold altercol">
            0.00
        </td>
        <td class="hidden-stack-mode col-master-winloss ">
            0.00
        </td>
        <td class="hidden-stack-mode ">
            0.00
        </td>
        <td class="hidden-stack-mode bold ">
            0.00
        </td>
        <td class="col-company"><?php echo number_format(($tt + $gc) * (-1), 2) ?></td>
    </tr>
    <?php } ?>
    </tbody>
    <tfoot>
    <tr class="tbl-footer">
        <td data-islink="true" class="text-left col-username">
            Tổng cộng        </td>

        <td class="col-number-of-tickets"><?php echo $allve ?></td>
        <td class="hidden-stack-mode col-turnover">
            <?php echo number_format($allcuoc, 2) ?>
        </td>
        <td class="hidden-stack-mode col-net-turnover">
            <?php echo number_format($allcuoc, 2) ?>
        </td>
        <td class="hidden-stack-mode col-gross-com">
            <?php echo number_format($allgc, 2) ?>
        </td>
        <td class="hidden-stack-mode col-member-winloss ">
            <?php echo number_format($alltt, 2) ?>
        </td>
        <td class="hidden-stack-mode ">
            <?php echo number_format($allhh, 2) ?>
        </td>
        <td class="hidden-stack-mode bold ">
            <?php echo number_format($alltt + $allhh, 2) ?>
        </td>
        <td class="hidden-stack-mode col-agent-winloss altercol">
            0.00
        </td>
        <td class="hidden-stack-mode altercol">
            0.00
        </td>
        <td class="hidden-stack-mode bold altercol">
            0.00
        </td>
        <td class="hidden-stack-mode col-master-winloss ">
            0.00
        </td>
        <td class="hidden-stack-mode ">
            0.00
        </td>
        <td class="hidden-stack-mode bold ">
            0.00
        </td>
        <td class="col-company"><?php echo number_format(($alltt + $allgc) * (-1), 2) ?></td>
    </tr>
    </tfoot>
    <?php } ?>
</table>

<form class="form-inline hide" role="form" id="export-excel-form"
      action="/site-reports/WinLossDetail/Excel">
    <input data-val="true" data-val-number="The field CustId must be a number." data-val-required="The CustId field is required." id="CustId" name="CustId" type="hidden" value="19978374" />
    <input id="CustName" name="CustName" type="hidden" value="TY28A2" />
    <input data-val="true" data-val-date="The field FromDate must be a date." data-val-required="The FromDate field is required." id="FromDate" name="FromDate" type="hidden" value="10/14/2017 12:00:00 AM" />
    <input data-val="true" data-val-date="The field ToDate must be a date." data-val-required="The ToDate field is required." id="ToDate" name="ToDate" type="hidden" value="10/14/2017 12:00:00 AM" />
    <input id="UserSelectedProductIds" name="UserSelectedProductIds" type="hidden" value="1,2,3,5,6,8,12,21,22,23,24,28" />
    <input id="CustLevelId" name="CustLevelId" type="hidden" value="3" />
    <input data-val="true" data-val-required="The IsHistoricReport field is required." id="IsHistoricReport" name="IsHistoricReport" type="hidden" value="False" />
</form>

<div id="report-pager"
     data-labelpage="Trang"
     data-labelof="tr&#234;n"
     data-labeldisplayitems="Displaying {from} to {to} of {total} items"
     data-pageindex="1"
     data-pagesize="1000"
     data-totalrecords="12">
</div>






<form class="form-inline hide" role="form" id="export-excel-form"
      action="/site-reports/WinLossDetail/Excel">
    <input data-val="true" data-val-number="The field CustId must be a number." data-val-required="The CustId field is required." id="CustId" name="CustId" type="hidden" value="19978374" />
    <input id="CustName" name="CustName" type="hidden" value="TY28A2" />
    <input data-val="true" data-val-date="The field FromDate must be a date." data-val-required="The FromDate field is required." id="FromDate" name="FromDate" type="hidden" value="10/14/2017 12:00:00 AM" />
    <input data-val="true" data-val-date="The field ToDate must be a date." data-val-required="The ToDate field is required." id="ToDate" name="ToDate" type="hidden" value="10/14/2017 12:00:00 AM" />
    <input id="UserSelectedProductIds" name="UserSelectedProductIds" type="hidden" value="1,2,3,5,6,8,12,21,22,23,24,28" />
    <input id="CustLevelId" name="CustLevelId" type="hidden" value="3" />
    <input data-val="true" data-val-required="The IsHistoricReport field is required." id="IsHistoricReport" name="IsHistoricReport" type="hidden" value="False" />
</form>

<input data-val="true" data-val-required="The IsSetDefaultStackMode field is required." id="IsSetDefaultStackMode" name="IsSetDefaultStackMode" type="hidden" value="True" />
<input id="LoadLegendURL" name="LoadLegendURL" type="hidden" value="/site-reports/WinLossDetail/ReportLegend" />

<div id="report-pager"
     data-labelpage="Trang"
     data-labelof="tr&#234;n"
     data-labeldisplayitems="Displaying {from} to {to} of {total} items"
     data-pageindex="1"
     data-pagesize="1000"
     data-totalrecords="12">
</div>



<script src="https://mb.b88ag.com/assets/bundles/common/common.min.js?v=453" type="text/javascript"></script>
<script src="https://mb.b88ag.com/assets/bundles/site-reports/default.min.js?v=1.0.6493.20440" type="text/javascript"></script>

<script src="https://mb.b88ag.com/site-reports/assets/bundles/winlossdetail/winlossdetail.min.js?v=1.0.6493.20440" type="text/javascript"></script>
</body>
</html>