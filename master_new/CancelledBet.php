<?php
@session_start();
include("../database.php");
$data = new database();
if(!isset($_SESSION['ma']))
    return;
$ag = $_SESSION['ma'];
$new_date = date ( 'Y-m-d' ,strtotime ( '-3 day' ));
$sqlmy = "select * from betlistmy_huy  where ngay_bet >= '$new_date' and ma_nguoi_dung like '$ag%' order by ma_bet desc";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Cancelled Bet List</title>
    <link rel="shortcut icon" type="image/x-icon" href="/site-betlists/favico.ico" />

    <link href="../assets/bundles/common/common.min.css?v=453" rel="stylesheet" type="text/css">
    <link href="../assets/bundles/site-betlists/default.min.css?v=1.0.6486.18965" rel="stylesheet" type="text/css">
    <link href="https://mb.b88ag.com/site-betlists/assets/bundles/default.min.css?v=1.0.6486.18965" rel="stylesheet" type="text/css">

    <link href="https://mb.b88ag.com/assets/bundles/site-betlists/filter.min.css?v=1.0.6486.18965" rel="stylesheet" type="text/css">
    <link href="https://mb.b88ag.com/site-betlists/assets/bundles/filter.min.css?v=1.0.6486.18965" rel="stylesheet" type="text/css">
</head>
<body>
<div class="bl_title">
    Danh s&#225;ch đặt cược bị Hủy/ Từ Chối (c&#225;ch đ&#226;y 3 ng&#224;y )
    <div  class="list-icon" id="betlist-filter">
        <ul>
            <li class="showfilter-element"><span class="icon-arrow-up-drop-circle icon-filter" title="Ẩn bộ lọc dữ liệu"></span></li>
            <li class="hidefilter-element"><span class="icon-arrow-down-drop-circle icon-filter" title="Hiện bộ lọc dữ liệu"></span></li>
        </ul>
    </div>

</div>

<form class="form-inline filter  cancelled-bet" role="form" id="betlist-form">
    <div class="form-group">
        <label>Agent</label>
        <select class="dropdown-users" id="dropdown-agents" name="agentId">
            <option value="27510343" > TY28A20R (20 bets)</option>
            <option value="27080986" > TY28A20D (12 bets)</option>
            <option value="27477998" > TY28A210 (10 bets)</option>
            <option value="27157106" > TY28A20H (3 bets)</option>
            <option value="27444999" > TY28A21A (3 bets)</option>
            <option value="27433184" > TY28A231 (2 bets)</option>
            <option value="27269031" > TY28A20J (2 bets)</option>
            <option value="27375021" > TY28A20O (1 bets)</option>
            <option value="26292438" > TY28A234 (1 bets)</option>
            <option value="27053893" > TY28A204 (1 bets)</option>
            <option value="27055727" > TY28A20B (1 bets)</option>
        </select>
    </div>

    <div class="form-group">
        <a id="icon-excel" class="icon icon-excel" title="Excel"
           href="/site-betlists/CancelledBet/Excel?MasterId=0&amp;AgentId=0&amp;MemberId=0">
        </a>
    </div>
</form>


<table class="tblRpt tblRpt-bordered tblRpt-striped width-100per tablesorter tblRpt-hover tbl-betlist">
    <thead>
    <tr>
        <th class="col-number">#</th>
        <th class="col-member">Member</th>
        <th class="col-transtime">Thời gian</th>
        <th>Lựa chọn</th>
        <th class="col-odds">Tỷ lệ</th>
        <th class="col-stake">Tiền cược</th>
        <th class="col-status">Trạng th&#225;i</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $stt = 0;
    $f=1;
    $hoah=0;
    $dut = 0;
    $bet=$data->ExcuteObjectList($sqlmy);
    $countBet=count($bet);
    for($i=0;$i<$countBet;$i++) {
    $ngay = $bet[$i]['ngay_bet'];
    $tien = $bet[$i]['so_tien'];
    $r = $bet[$i];
    $chap = $bet[$i]['ma_nha'];
    $ten = $bet[$i]['doi_nha'];
    if($chap=='') {
        $chap = $bet[$i]['ma_khach'];
        $ten = $bet[$i]['doi_khach'];
    }
    $chap=$bet[$i]["chap"];
    $tentran="";
    $tenhienthitran="";
    $hienthichap=$bet[$i]["chap"];
    $tiso=$bet[$i]["tiso"];
    $tilecuoc="";
    //$tilecuoc=$bet[$i]["ma_nha"];
    $sotien=$bet[$i]["so_tien"];
    $tendoi1=$bet[$i]["doi_nha"];
    $tendoi2=$bet[$i]["doi_khach"];
    $idtran=$bet[$i]["ma_bet"];
    $showTiSo="";
    if($bet[$i]["lahiep"]!="")
    {
        $showTiSo="HT: ".$tiso;
        if($bet[$i]["ma_nha"]!="")
        {
            $tenhienthitran="1H - Handicap";
            $tentran=$bet[$i]["doi_nha"];
            $tilecuoc=$bet[$i]["ma_nha"];
        }
        if($bet[$i]["ma_khach"]!="")
        {
            $tenhienthitran="1H - Handicap";
            $tentran=$bet[$i]["doi_khach"];
            $tilecuoc=$bet[$i]["ma_khach"];
        }
        if($bet[$i]["ma_tren"]!="")
        {
            $tenhienthitran="1H - Tài/ Xỉu";
            $tentran="Tài";
            $tilecuoc=$bet[$i]["ma_tren"];
        }
        if($bet[$i]["ma_duoi"]!="")
        {
            $tenhienthitran="1H - Tài/ Xỉu";
            $tentran="Xỉu";
            $tilecuoc=$bet[$i]["ma_tren"];
        }
        if($bet[$i]["ma_1x"]!="")
        {
            $tenhienthitran="1H 1X2";
            $tentran="FT.1";
            $tilecuoc=$bet[$i]["ma_1x"]-1;
        }
        if($bet[$i]["ma_2x"]!="")
        {
            $tenhienthitran="1H 1X2";
            $tentran="FT.2";
            $tilecuoc=$bet[$i]["ma_2x"]-1;
        }
        if($bet[$i]["ma_3x"]!="")
        {
            $tenhienthitran="1H 1X2";
            $tentran="FT.X";
            $tilecuoc=$bet[$i]["ma_3x"]-1;
        }
    }
    else
    {
        $showTiSo="FT: ".$tiso;
        if($bet[$i]["ma_nha"]!="")
        {
            $tenhienthitran="Handicap";
            $tentran=$bet[$i]["doi_nha"];
            $tilecuoc=$bet[$i]["ma_nha"];
        }
        if($bet[$i]["ma_khach"]!="")
        {
            $tenhienthitran="Handicap";
            $tentran=$bet[$i]["doi_khach"];
            $tilecuoc=$bet[$i]["ma_khach"];
        }
        if($bet[$i]["ma_tren"]!="")
        {
            $tenhienthitran="Tài/ Xỉu";
            $tentran="Tài";
            $tilecuoc=$bet[$i]["ma_tren"];
        }
        if($bet[$i]["ma_duoi"]!="")
        {
            $tenhienthitran="Tài/ Xỉu";
            $tentran="Xỉu";
            $tilecuoc=$bet[$i]["ma_tren"];
        }
        if($bet[$i]["ma_1x"]!="")
        {
            $tenhienthitran="FT.1X2";
            $tentran="FT.1";
            $tilecuoc=$bet[$i]["ma_1x"]-1;
        }
        if($bet[$i]["ma_2x"]!="")
        {
            $tenhienthitran="FT.1X2";
            $tentran="FT.2";
            $tilecuoc=$bet[$i]["ma_2x"]-1;
        }
        if($bet[$i]["ma_3x"]!="")
        {
            $tenhienthitran="FT.1X2";
            $tentran="FT.X";
            $tilecuoc=$bet[$i]["ma_3x"]-1;
        }
    }
    $hhmb = 0;
    $hh = 0;




    if($tilecuoc < 0 )
    {
        switch($bet[$i]['win'])

        {

            case 'thang':

                $win = 'Win';
                $du = $tien;
                $hh = $tien*$hhmb;
                break;
            case 'thangnua':

                $win = 'Half Win';
                $du = $tien/2;
                $hh = $tien*$hhmb;
                break;
            case 'thuanua':

                $win = 'Half Lost';
                $du = $tien/2*$tilecuoc;
                $hh = $tien*$hhmb;
                break;
            case 'hoa':

                $win = 'Drow';
                $du = 0;
                $hh = 0;
                break;

            case 'thua':

                $win = 'Lost';
                $tlc = str_replace("-","",$tilecuoc );
                $du = ($tien*$tlc)*(-1);
                $hh = $tien*$hhmb;
                break;

        }
    }
    else
    {
        switch($bet[$i]['win'])

        {

            case 'thang':

                $win = 'Win';
                $du = $tien*$tilecuoc;
                $hh = round((float)$tien*$hhmb,2);
                break;
            case 'thangnua':

                $win = 'Half Win';
                $du = $tien/2*$tilecuoc;
                $hh = round((float)$tien/2*$hhmb,2);
                break;
            case 'thuanua':

                $win = 'Half Lost';
                $du = round((float)$tien/2*(-1),2);
                $hh = round((float)$tien/2*$hhmb,2);
                break;
            case 'hoa':

                $win = 'Drow';
                $du = 0;
                $hh = 0;
                break;

            case 'thua':

                $win = 'Lost';
                $du = round((float)$tien*(-1),2);
                $hh = round((float)$tien*$hhmb,2);
                break;

        }
    }
    $ma_nguoi_dung = $bet[$i]["ma_nguoi_dung"];
    $stt++;
    ?>
    <tr id="r102816988529">
        <td class='w-order text-center'><?php echo $stt; ?></td>
        <td class="text-center"><?php echo $ma_nguoi_dung; ?></td>
        <td class="trans-block text-center nonbreak">
            Ref No: <?php echo $idtran ?><div class="time"><?php echo $r['ngay_ht'] ?></div>
        </td>

        <td class="choice-block text-right sport-1">
            <div class='reject'>
                <span class="underdog"><?php echo $tentran; ?><span class='handicap'><?php echo $hienthichap ?></span><span class="favorite"> [<?php echo $tiso; ?>]</span></span>
                <div class="bettype"><?php echo $tenhienthitran; ?></div>
                <div class="match"><span><?php echo $tendoi1 ?></span>
                    <span>&nbsp;-&nbsp;vs&nbsp;-&nbsp;</span><span><?php echo $tendoi2 ?></span></div>
                <div class="league"><span class="sport">Bóng đá</span>
                    <span class="leagueName">&nbsp;<?php echo $r['giai']?></span>
                </div>
            </div>
        </td>

        <td class="odds-block bl_underdog nonbreak text-right">
            <span class="underdog"><?php echo $tilecuoc ?></span><br /><span class="oddstype">MY</span>
        </td>

        <td class="stake-block bl_underdog text-right">
            <div class="stake"><?php echo $sotien ?></div>
        </td>

        <td class="status-block text-center sport-1">
            <div class="status">Từ chối</div><div class="ip"><br /><div class="iplink" onclick="OpenIPInfo('<?php echo $r["ip_go"]?>');"></div></div>
        </td>

    </tr>
    <?php } ?>
    </tbody>
</table>
<input id="betlist-path" name="betlist-path" type="hidden" value="/site-betlists/" />
<input id="old-main-path" name="old-main-path" type="hidden" value="/ex-main/" />
<input id="results-path" name="results-path" type="hidden" value="/site-results/" />
<input id="totalbets-forecast-path" name="totalbets-forecast-path" type="hidden" value="/ex-totalbetsforecast/" />

<script src="https://mb.b88ag.com/assets/bundles/common/common.min.js?v=453" type="text/javascript"></script>
<script src="https://mb.b88ag.com/assets/bundles/site-betlists/default.min.js?v=1.0.6486.18965" type="text/javascript"></script>
<script src="https://mb.b88ag.com/site-betlists/assets/bundles/default.min.js?v=1.0.6486.18965" type="text/javascript"></script>
<div class="modal fade" id="details-modal" tabindex="-1" role="dialog" aria-labelledby="details-title">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="modal-body">
                <img id="loading-image" src="/site-betlists/assets/styles/images/ajax-loader.gif" style="margin: auto" />
                <iframe src="about:blank" frameborder="0" scrolling="yes"></iframe>
            </div>
        </div>
    </div>
</div>

<script src="https://mb.b88ag.com/assets/bundles/site-betlists/filter.min.js?v=1.0.6486.18965" type="text/javascript"></script>
<script src="https://mb.b88ag.com/site-betlists/assets/bundles/cancelled-betlist/cancelled-betlist.min.js?v=1.0.6486.18965" type="text/javascript"></script>
</body>
</html>