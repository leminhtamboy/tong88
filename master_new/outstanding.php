<?php
include('../database.php');
@session_start();
$ngay1 = date("m/d/Y", strtotime("-14 day"));
$ngay = date("Y-m-d", strtotime("-14 day"));
$ngay2 = "12/31/9999";//date("m/d/Y", strtotime("+7 day"));
$data = new database();
if(!isset($_SESSION['ma']))

    return;

if(!isset($_REQUEST['su']))
{
    $ag = $_SESSION['ma'];
}
else
{
    $ag = $_REQUEST['su'];
}
$sqlmb = "select * from master where Ma like '$ag%' order by Ma asc ";
$member = $data->ExcuteObjectList($sqlmb);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Outstanding Report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/bundles/common/common.min.css?v=453" rel="stylesheet" type="text/css">
    <link href="../assets/bundles/site-reports/default.min.css?v=1.0.6493.20440" rel="stylesheet" type="text/css">
    <link href="../assets/bundles/default.min.css?v=1.0.6493.20440" rel="stylesheet" type="text/css">
</head>
<body>


<div class="page-title">
    <span class="pull-left">Tiền chưa xử l&#253; của Master</span>
    <div class="list-icon">
        <ul>
            <li class="showfilter-element"><span class="icon-arrow-up-drop-circle icon-filter" title="Ẩn bộ lọc dữ liệu"></span></li>
            <li class="hidefilter-element"><span class="icon-arrow-down-drop-circle icon-filter" title="Hiện bộ lọc dữ liệu"></span></li>

        </ul>
    </div>
</div>


<form class="form-inline filter " role="form" id="report-form">
    <div class="form-group">
        <select id="dropdown-product"
                data-has-any-product="True"
                data-label-select-products="Lựa chọn sản phẩm"
                data-label-selected="Đ&#227; chọn"
                data-label-all-products="Tất cả sản phẩm"
                data-label-there-is-no-product="Kh&#244;ng c&#243; sản phẩm n&#224;o"
                data-language=""
                multiple="multiple" class="select-multiple">
            <option value="1"  selected=&quot;selected&quot;>Sportsbook</option>
            <option value="2"  selected=&quot;selected&quot;>Racing</option>
            <option value="3"  selected=&quot;selected&quot;>Number Game</option>
            <option value="4"  selected=&quot;selected&quot;>Live Casino</option>
            <option value="5"  selected=&quot;selected&quot;>Virtual Sports</option>
            <option value="6"  selected=&quot;selected&quot;>Casino</option>
            <option value="8"  selected=&quot;selected&quot;>Keno</option>
            <option value="12"  selected=&quot;selected&quot;>Gold Deluxe</option>
            <option value="21"  selected=&quot;selected&quot;>Allbet</option>
            <option value="22"  selected=&quot;selected&quot;>Macau Games</option>
            <option value="23"  selected=&quot;selected&quot;> L&#244; Đề </option>
            <option value="24"  selected=&quot;selected&quot;>Bắn c&#225;</option>
            <option value="28"  selected=&quot;selected&quot;>Xổ số</option>
        </select>
        <input type="hidden" name="UserSelectedProductIds" id="product-ids" value="1,2,3,4,5,6,8,12,21,22,23,24,28" />
    </div>


    <input id="CustId" name="CustId" type="hidden" value="19978374" />
    <input id="CustName" name="CustName" type="hidden" value="TY28A2" />
    <input id="CustLevelId" name="CustLevelId" type="hidden" value="3" />
    <input id="BreadcrumbLevelJson" name="BreadcrumbLevelJson" type="hidden" value="[{&#39;CustId&#39;:19978374,&#39;CustName&#39;:&#39;TY28A2&#39;,&#39;CustLevelId&#39;:3}]" />
    <input id="SortPropertyName" name="SortPropertyName" type="hidden" value="CustName" />
    <input data-val="true" data-val-required="The IsDescendingSort field is required." id="IsDescendingSort" name="IsDescendingSort" type="hidden" value="False" />
    <input data-val="true" data-val-required="The IsFilterVisible field is required." id="IsFilterVisible" name="IsFilterVisible" type="hidden" value="True" />
    <div class="form-group">
        <input type="submit" value="X&#225;c nhận" class="btn-submit" />
    </div>
</form>

<table class="tblRpt tblRpt-bordered tblRpt-striped tblRpt-hover tbl-outstanding tablesorter" id="tbl-report"
       data-totalrecords="8"
       data-sortedcolumn="CustName"
       data-descendingsort="False">
    <thead class="tableFloatingHeaderOriginal">
    <tr>
        <th rowspan="2" class="header sorting" data-colindex="0" data-sortedname="CustName">
            Master
        </th>
        <th colspan="2" class="header" data-disabledsort="true">
            Tiền chưa xử l&#253;
        </th>
    </tr>
    <tr class="thead-row2">
        <th class="sorting" data-colindex="1" data-sortedname="MemberOutstanding">
            Master
        </th>
        <th class="sorting" data-colindex="2" data-sortedname="MasterOutstanding">
            Super Master
        </th>

    </tr>
    </thead>
    <tbody>
<?php
    $tong = 0;
    $countMember = count($member);
    $tongtien = 0;
    for($j = 0; $j<$countMember ;$j++) {
        $mb = $member[$j];
        $ma = $mb['Ma'];
        $id = $ma;
        $sql = $data->ExcuteObjectList("select sum(so_tien) as t from betlistth where ma_nguoi_dung like '$id%' and tinh_trang = '0' ");
        $sql1 = $data->ExcuteObjectList("select sum(so_tien) as t from betlistmy where ma_nguoi_dung like '$id%'  and tinh_trang = '0'");
        $sql2 = $data->ExcuteObjectList("select sum(so_tien) as t from betlistchinhxac where ma_nguoi_dung like '$id%' and tinhtrang = '0'");
        $sql4 = $data->ExcuteObjectList("select sum(so_tien) as t from betlistnumtren where ma_nguoi_dung like '$id%'  and tinh_trang = '0'");
        $sql5 = $data->ExcuteObjectList("select sum(so_tien) as t from betlistnumduoi where ma_nguoi_dung like '$id%' and tinh_trang = '0' ");
        $tong = 0;
        $tong += $sql[0]['t'];
        $tong += $sql1[0]['t'];
        $tong += $sql2[0]['t'];
        $tong += $sql4[0]['t'];
        $tong += $sql5[0]['t'];
        if($tong  == 0)
        {
            continue;
        }
        $tongtien+= $tong;
        $link = "../master/outstanding.php?mt=$id&ngay1=10/01/2017&ngay2=12/31/9999";
    ?>
    <tr>
        <td class="text-left" data-islink="true">
            <a href="<?php echo $link ?>"
               data-custname="<?php echo $ma; ?>"
               class="downline-link">
                 <?php echo $ma; ?>
            </a>
        </td>
        <td>
            <?php echo $data->chinh_so_tien($tong); ?>
        </td>
        <td>
            0.00
        </td>

    </tr>
    <?php } ?>
    </tbody>
    <tfoot>
    <tr class="tbl-footer">
        <td class="text-left">Tổng cộng</td>
        <td>
            <?php echo $data->chinh_so_tien($tongtien); ?>
        </td>
        <td>
            0.00
        </td>

    </tr>
    </tfoot>

</table>



<script src="https://mb.b88ag.com/assets/bundles/common/common.min.js?v=453" type="text/javascript"></script>
<script src="https://mb.b88ag.com/assets/bundles/site-reports/default.min.js?v=1.0.6493.20440" type="text/javascript"></script>
<script src="https://mb.b88ag.com/site-reports/assets/bundles/outstanding/outstanding.min.js?v=1.0.6493.20440" type="text/javascript"></script>

</body>
</html>