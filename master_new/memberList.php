<?php
include_once("../database.php");
$data = new database();
if(isset($_REQUEST['ma']))
    $ma = $_REQUEST['ma'];
else
    $ma = $_SESSION['ma'];
$dataMember=$data->ExcuteObjectList("select * from master where Ma_sup='$ma' order by Tinh_trang asc, Ma asc");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Customer List</title>
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Common/Agent.min.css?2017081602" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../assets/styles/fonts/Iconalpha/style.css?v=1" />
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/MemberInfo/CustomerList/CustomerList.min.css?2017081602" rel="stylesheet" type="text/css" />
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/MemberInfo/CustomerList/icons.min.css?2017081602" rel="stylesheet"type="text/css" />
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/MemberInfo/CustomerList/MenuPopup_Control/MenuPopup_Control.min.css?2017081602" rel="stylesheet"type="text/css" />
</head>
<body>
<table>
    <tr>
        <td><input type="hidden" id="arrayCustID" name="arrayCustID" /><input type="hidden" id="arrayUserName" name="arrayUserName" /><input type="hidden" id="arrayStatusRnCasino" name="arrayStatusRnCasino" /><input type="hidden" id="arrayStatus" name="arrayStatus" /><input type="hidden" id="isDisableSuspendedStatus" name="isDisableSuspendedStatus"value="false" /><input type="hidden" id="isDisableAllowOutrightStatus" name="isDisableAllowOutrightStatus"value="true" /></td>
    </tr>
</table>
<table class="width-100per">
    <tr>
        <td>
            <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/ErrorMsg/ErrorMsgNew.min.css?2017081602" rel="stylesheet" type="text/css" />
            <script src="https://mb.b88ag.com/ex-main/_Components/ErrorMsg/ErrorMsg.js?2017081602" type="text/javascript"></script>
            <div id="diverrmsg" class="width-100per">
                <div id="spmsgerr" class="msgerr"></div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="title_header" class="title-page">Danh sách Agent <a href="javascript:getPrint('page_main');" id="imgPrint" title="In" class="icon-print"></a></div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="box_header">
                <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/DoubleCommissionSearchUserName_Control/SearchUserName_Control.min.css?2017081602" rel="stylesheet" type="text/css" />
                <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/DoubleCommissionSearchUserName_Control/styles.min.css?2017081602" rel="stylesheet" type="text/css" />
                <table id="tblSearch" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>Tên đăng nhập</td>
                        <td><input type="text" class="text_italic" name="txtUserName" id="txtUserName" value="Tên đăng nhập hoặc Tên/Họ" onkeypress="onKeyPressUser('dSubmit',event);"onclick="onclickUser('Tên đăng nhập hoặc Tên/Họ')" onblur="onblurUser('Tên đăng nhập hoặc Tên/Họ')" autocomplete="off"/></td>
                        <td>Trạng thái</td>
                        <td>
                            <div id="box_option">
                                <select id="statusFilter" name="statusFilter">
                                    <option value="0" >Tất cả</option>
                                    <option value="1" >Mở</option>
                                    <option value="2" >Bị đình chỉ</option>
                                    <option value="3" >Bị khóa</option>
                                    <option value="4" >Vô hiệu hóa</option>
                                </select>
                            </div>
                            <div class="container-btn"><input id="dSubmit" type="button" value="Xác nhận" class="buttonSubmit" onclick="GetCustomer('AgentList.aspx')" /></div>
                            <div class="shadow" id="shadow" />
                            <div class="output" id="output" />
                            <script src="https://mb.b88ag.com/ex-main/_Components/DoubleCommissionSearchUserName_Control/SearchUserName_Control.js?2017081602" type="text/javascript"></script><script src="/ex-main/_Components/DoubleCommissionSearchUserName_Control/autocomplete.js?2017081602" type="text/javascript"></script>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="page_main">
                <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/MemberInfo/CustomerList/Print_Control/Print.min.css?2017081602" rel="stylesheet" type="text/css" media="print" />
                <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/PagingHeader/PagingHeader.min.css?2017081602" rel="stylesheet" type="text/css" />
                <table id="tblHeader">
                    <tr>
                        <td class="bgleft">Lưa chọn : <a href="#" id="chkAll" onclick="CheckAll_onClick(true);">Tất cả</a> | <a href="#" id="chkNone" onclick="CheckAll_onClick(false);">None</a></td>
                        <td class="bgcenter">
                            <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/Paging/Paging.css?2017081602" rel="stylesheet" type="text/css" />
                            <script src="https://mb.b88ag.com/ex-main/_Components/Paging/Paging.js?2017081602" type="text/javascript"></script>
                            <div id="_PagingTop" class="pagingHiden" pagesize="500" currentindex="1" rowcount="500" pagecount="1"><span disabled id="btnFirst_PagingTop" type="button" onclick="_PagingTop.First(this)" class="icon pagingFirst"></span><span disabled id="btnPrev_PagingTop" type="button" onclick="_PagingTop.Move(this, -1)" class="icon pagingPrev"></span><span class="pagingSeperator"></span>Trang<input id="txt_PagingTop" type="text" class="pagingCurrent" maxlength="4" size="2" value="1" onkeydown="_PagingTop.DoEnter(event, '_PagingTop.Go()')" />trên 1<span class="pagingSeperator"></span><span disabled id="btnNext_PagingTop" type="button" onclick="_PagingTop.Move(this, 1)" class="icon pagingNext"></span><span disabled id="btnLast_PagingTop" type="button" onclick="_PagingTop.Last(this)" class="icon pagingLast"></span></div>
                            <script type="text/javascript">var _PagingTop = new Paging('_PagingTop');</script>
                        </td>
                        <td class="bgright">
                            Số dòng
                            <select id="sel_PagingTop" name="sel_PagingTop" onchange="_PagingTop.SetPageSize(this.value)">
                                <option value="10">10</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="500" selected>500</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <table id="tblCustomerList" class="tblRpt width-100per">
                    <thead class="RptHeader">
                    <tr>
                        <th id="headerNo" rowspan="2" userId="19978374" subAccId="0">No.</th>
                        <th rowspan="2"  isDirectDownline="1" isShowDCom=False>
                            <div onclick="RenderMulti(this,'MultiPopup','','19978374','0');" class="btnEditMult icon-view-grid" id="EdiMulti"></div>
                        </th>
                        <th rowspan="2">Tên đăng nhập</th>
                        <th rowspan="2">Trạng thái</th>
                        <th rowspan="2">Tùy chỉnh</th>
                        <th rowspan="2">Member</th>
                        <th rowspan="2">Nhân đôi hoa hồng</th>
                        <th rowspan="2">Tên</th>
                        <th rowspan="2">Họ</th>
                        <th colspan="7" class="header_comm">Hoa hồng</th>
                        <th rowspan="2">Ngày tạo</th>
                        <th rowspan="2">IP đăng nhập</th>
                        <th rowspan="2">Tên đăng nhập</th>
                    </tr>
                    <tr class="RptHeader02">
                        <th>Nhóm A</th>
                        <th>Nhóm B</th>
                        <th>Nhóm C</th>
                        <th>Nhóm D</th>
                        <th>1 X 2</th>
                        <th>Other</th>
                        <th>Number Game</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $countMember = count($dataMember);
                    $stt = 0;
                    for($i = 0; $i< $countMember; $i++ ){
                        $idMember = $dataMember[$i]["id"];
                        $tinh_trang = $dataMember[$i]["Tinh_trang"];
                        $ma_member = $dataMember[$i]["Ma"];
                        //tinh trang la 1 ben bong that la mo ben minh la 0
                        $link_open = '../master/memberList.php?Ma=$ma_member';
                        $ten = $dataMember[$i]["Ten"];
                        $hoa_hong = $dataMember[$i]["Hoa_hong"];
                        $ngay_tao = $dataMember[$i]["ngayht"];
                        $ip_dang_nhap = $dataMember[$i]["ip"];
                        $textStatus = "Mở";
                        $classKhoaMo = "RowBgOpen";
                        $isClosed = 0;
                        if($tinh_trang == 1) // Mo
                        {
                            $textStatus = "Khoá";
                            $classKhoaMo = "closed-byUpline";
                            $isClosed = true;
                        }
                        if($tinh_trang == 2){
                            $textStatus = "Đình Chỉ";
                            $classKhoaMo = "suspended-byUpline";
                        }
                        $stt++;
                    ?>
                    <tr class="<?php echo $classKhoaMo; ?>" id="<?php echo $idMember ?>" icon_23="1" show_23="true"icon_24="1" show_24="true"icon_5="1" show_5="true"icon_28="1" show_28="true"icon_26="2"  iconSportbook2="1" iconCockFighting="2" iconCricket="2" iconhr="2" iconcs="1" iconbg="0" iconp2p="{iconp2p}" isDisabledColosusBets="2" isDisabledGoldDeluxe="1" isDisabledP2P="{isDisabledP2P}" iconLiveCasino="1" isDisabledLiveCS="0" isDisabledVS="{isDisabledVS}" iconKeno="1" isDisabledKeno="1" iconEdit="" statusKeno="0" iconAllbet="1" showAllbet="true" iconVoidbridge="1" showVoidbridge="true" unsyncedproducts="">
                        <td><?php echo $stt; ?></td>
                        <td><input type="checkbox" id="chkid_<?php echo $idMember ?>" name="chkid" value="<?php echo $ma_member; ?>" show_multiple_23="true"show_multiple_24="true"show_multiple_5="true"show_multiple_28="true" statuscs="<?php echo $tinh_trang ?>" statusracing="0" statusbingo="-1" statusLiveCasino="1" showAllbetMultiple="true" showVoidbridgeMultiple="true" /></td>
                        <td class="l">
                            <div class='text' onclick="window.location.href='<?php echo $link_open ?>'"><?php echo $ma_member ?></div>
                        </td>
                        <td>
                            <div id="IdStatus">
                                <span class="text"><span onclick="EditAgent_Single('27053471','TY28A200', 19978374);" title="Mở"><?php echo $textStatus; ?></span></span>
                                <span onclick="RenderStatus(this,'Popup','<?php echo $idMember ?>',<?php echo $isClosed ?>,false,0,false, 'chkid_<?php echo $idMember ?>');" class="icon-menu-down arrow" title="Trạng thái">&nbsp;&nbsp;&nbsp</span>
                            </div>
                        </td>
                        <td align="center">
                            <div title="Tùy chỉnh" class="icon-table-edit divOther" onclick="ShowFrmUpdOthers(this,'divUpdOthers', '<?php echo $idMember ?>', '<?php echo $ma_member ?>', 'Agent');"></div>
                        </td>
                        <td><a class="view" target="main" href="#" onclick="ViewDownLine('<?php echo $link_open ?>')" >Xem<span class="icon-arrow-down-bold-circle-outline icon-viewdown"></span></a></td>
                        <td>
                            <div class="bkgDcommDisallowed"><a onclick="">Không cho</a></div>
                        </td>
                        <td class="l"><?php echo $ten; ?></td>
                        <td class="l"></td>
                        <td><?php echo $hoa_hong; ?></td>
                        <td><?php echo $hoa_hong; ?></td>
                        <td><?php echo $hoa_hong; ?></td>
                        <td><?php echo $hoa_hong; ?></td>
                        <td><?php echo $hoa_hong; ?></td>
                        <td><?php echo $hoa_hong; ?></td>
                        <td>0</td>
                        <td class="bl_time"><?php echo $ngay_tao ?></td>
                        <td><a href="javascript:OpenIPInfo('<?php echo $ip_dang_nhap; ?>');" class="iplink"><?php echo $ip_dang_nhap; ?></a></td>
                        <td class="l">
                            <div class='text' onclick="window.location.href='<?php echo $link_open ?>'"><?php echo $ma_member ?></div>
                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="23">
                            <div id="MemberListMaster_PagingBottom" class="pagingHiden" pagesize="500" currentindex="1" rowcount="500" pagecount="1"><span disabled id="btnFirstMemberListMaster_PagingBottom" type="button" onclick="MemberListMaster_PagingBottom.First(this)" class="icon pagingFirst"></span><span disabled id="btnPrevMemberListMaster_PagingBottom" type="button" onclick="MemberListMaster_PagingBottom.Move(this, -1)" class="icon pagingPrev"></span><span class="pagingSeperator"></span>Trang<input id="txtMemberListMaster_PagingBottom" type="text" class="pagingCurrent" maxlength="4" size="2" value="1" onkeydown="MemberListMaster_PagingBottom.DoEnter(event, 'MemberListMaster_PagingBottom.Go()')" />trên 1<span class="pagingSeperator"></span><span disabled id="btnNextMemberListMaster_PagingBottom" type="button" onclick="MemberListMaster_PagingBottom.Move(this, 1)" class="icon pagingNext"></span><span disabled id="btnLastMemberListMaster_PagingBottom" type="button" onclick="MemberListMaster_PagingBottom.Last(this)" class="icon pagingLast"></span></div>
                            <script type="text/javascript">var MemberListMaster_PagingBottom = new Paging('MemberListMaster_PagingBottom');</script>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </td>
    </tr>
</table>
<script language="javascript" type="text/javascript">var isShowMyanmarOdds = "false";</script>
<ul id="Popup" class="divMenuPopup">
    <li id="tr_{id}">
        <div class="group"><input type="checkbox" id="chk_closed" value="0"  />Bị khóa</div>
    </li>
    <li id="tr_{id}">
        <div class="group"><input type="checkbox" id="chk_suspended" value="0"  />Bị đình chỉ</div>
    </li>
    <li id="tr_{id}">
        <div class="group"><input type="checkbox" id="chk_allowAutoPT" value="1"  />Cho Auto PT</div>
    </li>
    <li id="tr_{id}">
        <div class="group"><input type="checkbox" id="chk_outright" value="0"  />Cho phép Outright</div>
    </li>
</ul>
<ul id="MultiPopup" class="divMenuPopup">
    <li id="tr_sportBook"><a class="LinkPopup icon-edit" href="javascript:;" id="sportBook" value="0">Tùy chỉnh đồng loạt cho Sportsbook</a></li>
    <li id="tr_racing"><a class="LinkPopup icon-edit" href="javascript:;" id="racing" value="0">Tùy chỉnh đồng loạt cho Racing</a></li>
    <li id="tr_casino"><a class="LinkPopup icon-edit" href="javascript:;" id="casino" value="0">Tùy chỉnh đồng loạt cho Casino</a></li>
    <li id="tr_bingo"><a class="LinkPopup icon-edit" href="javascript:;" id="bingo" value="0">Tùy chỉnh đồng loạt cho Bingo</a></li>
    <li id="tr_keno"><a class="LinkPopup icon-edit" href="javascript:;" id="keno" value="0">Edit Multiple Keno</a></li>
    <li id="tr_livecasino"><a class="LinkPopup icon-edit" href="javascript:;" id="livecasino" value="0">Tùy chỉnh đồng loạt cho Live Casino</a></li>
    <li id="tr_golddeluxe"><a class="LinkPopup icon-edit" href="javascript:;" id="golddeluxe" value="0">Edit Multiple Gold Deluxe</a></li>
    <li id="tr_sportsbook2"><a class="LinkPopup icon-edit" href="javascript:;" id="sportsbook2" value="0">Edit Multiple Sportsbook 2</a></li>
    <li id="tr_allbet"><a class="LinkPopup icon-edit" href="javascript:;" id="allbet" value="0">Edit Multiple Allbet</a></li>
    <li id="tr_voidbridge"><a class="LinkPopup icon-edit" href="javascript:;" id="voidbridge" value="0">Edit Multiple Macau Games</a></li>
    <li id="tr_multiple_dcs_23"><a class="LinkPopup icon-edit" href="javascript:;" id="multiple_dcs_23" value="0">Tùy chỉnh đồng loạt  Lô Đề </a></li>
    <li id="tr_multiple_dcs_24"><a class="LinkPopup icon-edit" href="javascript:;" id="multiple_dcs_24" value="0">Tùy chỉnh đồng loạt Bắn cá</a></li>
    <li id="tr_multiple_dcs_5"><a class="LinkPopup icon-edit" href="javascript:;" id="multiple_dcs_5" value="0">Tùy chỉnh đồng loạt Virtual Sports</a></li>
    <li id="tr_multiple_dcs_28"><a class="LinkPopup icon-edit" href="javascript:;" id="multiple_dcs_28" value="0">Tùy chỉnh đồng loạt Xổ số</a></li>
    <li id="tr_doublecomm"><a class="LinkPopup icon-edit" href="javascript:;" id="doublecomm" value="0">Tùy chỉnh đồng loạt giá trị Nhân Đôi hoa hồng</a></li>
</ul>
<ul id="divUpdOthers" class="divMenuPopup">
    <li id="tr_racingPT"><a class="LinkPopup icon-edit" href="javascript:;" id="racingPT" value="0">Racing</a></li>
    <li id="tr_casinoPT"><a class="LinkPopup icon-edit" href="javascript:;" id="casinoPT" value="0">Casino</a></li>
    <li id="tr_bingoPT"><a class="LinkPopup icon-edit" href="javascript:;" id="bingoPT" value="0">Bingo</a></li>
    <li id="tr_livecasinoPT"><a class="LinkPopup icon-edit" href="javascript:;" id="livecasinoPT" value="0">Live Casino</a></li>
    <li id="tr_sportBookMinPT"><a class="LinkPopup icon-edit" href="javascript:;" id="sportBookMinPT" value="0">PT nhỏ nhất của SB</a></li>
    <li id="tr_racingMinPT"><a class="LinkPopup icon-edit" href="javascript:;" id="racingMinPT" value="0">PT nhỏ nhất của Racing</a></li>
    <li id="tr_kenoPT"><a class="LinkPopup icon-edit" href="javascript:;" id="kenoPT" value="0">Keno</a></li>
    <li id="tr_goldDeluxePT"><a class="LinkPopup icon-edit" href="javascript:;" id="goldDeluxePT" value="0">Gold Deluxe</a></li>
    <li id="tr_sportsbook2PT"><a class="LinkPopup icon-edit" href="javascript:;" id="sportsbook2PT" value="0">Sportsbook 2</a></li>
    <li id="tr_allbetPT"><a class="LinkPopup icon-edit" href="javascript:;" id="allbetPT" value="0">Allbet</a></li>
    <li id="tr_voidbridgePT"><a class="LinkPopup icon-edit" href="javascript:;" id="voidbridgePT" value="0">Macau Games</a></li>
    <li id="tr_dcs_23"><a class="LinkPopup icon-edit" href="javascript:;" id="dcs_23" value="0"> Lô Đề </a></li>
    <li id="tr_dcs_24"><a class="LinkPopup icon-edit" href="javascript:;" id="dcs_24" value="0">Bắn cá</a></li>
    <li id="tr_dcs_5"><a class="LinkPopup icon-edit" href="javascript:;" id="dcs_5" value="0">Virtual Sports</a></li>
    <li id="tr_dcs_28"><a class="LinkPopup icon-edit" href="javascript:;" id="dcs_28" value="0">Xổ số</a></li>
    <li id="tr_transfer"><a class="LinkPopup icon-edit" href="javascript:;" id="transfer" value="0">Điều kiện chuyển khoản</a></li>
    <li id="tr_editInfo"><a class="LinkPopup icon-edit" href="javascript:;" id="editInfo" value="0">Thông tin</a></li>
    <li id="tr_betSetting"><a class="LinkPopup icon-edit" href="javascript:;" id="betSetting" value="0">Giới hạn cược</a></li>
    <li id="tr_commission"><a class="LinkPopup icon-edit" href="javascript:;" id="commission" value="0">Hoa hồng</a></li>
    <li id="tr_password"><a class="LinkPopup icon-edit" href="javascript:;" id="password" value="0">Mật khẩu</a></li>
    <li id="tr_resetSecCode"><a class="LinkPopup icon-edit" href="javascript:;" id="resetSecCode" value="0">Mã bảo mật</a></li>
</ul>
<form id="target" method="post" target="AGEWndIframe"action="">
    <input type="hidden" id="isGetMultipleDCS" name="isGetMultipleDCS" value="1" />
    <input type="hidden" id="language" name="language" value="" />
    <input type="hidden" id="productId" name="productId" value="" />
    <input type="hidden" id="customerLevel" name="customerLevel" value="" />
    <input type="hidden" name="customerId" id="customerId" value="" />
    <input type="hidden" id="uplineId" name="uplineId" value="" />
    <input type="hidden" id="parentId" name="parentId" value="" />
    <input type="hidden" id="customerIds" name="customerIds" value="" />
    <input type="hidden" id="syncStatuses" name="syncStatuses" value="" />
    <input type="hidden" id="usernames" name="usernames" value="" />
    <input type="hidden" id="customerNames" name="customerNames" value="" />
    <input type="hidden" id="uplineLevel" name="uplineLevel" value="" />
    <input type="hidden" id="actorId" name="actorId" value="" />
    <input type="hidden" id="isMultiple" name="isMultiple" value="false" />
    <input type="hidden" id="actorIdUsername" name="actorIdUsername" value="" />
    <input type="hidden" id="subAccUsername" name="subAccUsername" value="" />
    <input type="hidden" id="subAccId" name="subAccId" value="" />
    <input type="hidden" id="formType" name="formType" value="1" />
    <input type="hidden" id="isForcingMode" name="isForcingMode" value="false" />
</form>
</body>
<script src="https://mb.b88ag.com/ex-main/_GlobalResources/Js/Core.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_Components/AGEWnd/AGEWnd.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_MemberInfo/CustomerList/Resources/CustomerList.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_MemberInfo/CustomerList/Resources/SportBook.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_MemberInfo/CustomerList/Resources/Racing.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_MemberInfo/CustomerList/Resources/Casino.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_MemberInfo/CustomerList/Resources/LiveCasino.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_MemberInfo/CustomerList/Resources/VirtualSports.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_MemberInfo/CustomerList/Resources/Keno.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_MemberInfo/CustomerList/Resources/GoldDeluxe.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_MemberInfo/CustomerList/Resources/ColossusBets.js?2017081602" type="text/javascript"></script>
<script src="https://mb.b88ag.com/ex-main/_MemberInfo/CustomerList/Resources/DynamicSetting.js?2017081602" type="text/javascript"></script>
</html>
<input name="__RequestVerificationToken" type="hidden" value="9fey4oBwiIG3krQ-pwNimFU9jPRMUkTbP81nr5HdOzKGohCs3UPAf6UuG_f3r8NW2uy5OD0XCNrdETOER-7lnd2Xq39mYiclBPOmp-vs2SFrmYqCykokWIjWNhFQVxLj927JBYAl_QApIxPtXs08LJq2pXc1" />
<script type="text/javascript">var _page = {'resetSecurityCodeUrl':'/site-main/SecurityCode/ResetSecurityCode','resetMainAccountPassword':'/site-main/Password/ResetMainAccountPassword','isHideTransfer':'0','editMultipleAllbet':'Edit Multiple Allbet','usernamedefault':'Tên đăng nhập hoặc Tên/Họ','strConfirmDisableDoubleComm':'Bạn có chắc muốn khóa chức năng Nhân đôi hoa hồng với những cá cược bị thua trong Sportbooks của tài khoản này không? Cảnh báo, tất cả những thành viên cấp dưới trực thuộc tài khoản này sẽ đều bị khóa chức năng đó!','popupTitle_multiple_dcs_26':'Tùy chỉnh đồng loạt Lucky 3','confirmUnSuspendDownline':'Bạn có chắc muốn bỏ trạng thái \"Đình Chỉ\" cho thành viên này không? Khuyến cáo: tất cả cấp dưới trực thuộc thành viên này sẽ được bỏ trạng thái \"Đình Chỉ\".','subUserName':'','popupTitle_multiple_dcs_24':'Tùy chỉnh đồng loạt Bắn cá','confirmSuspendDownline':'Bạn có chắc muốn đình chỉ thành viên này?\\n Khuyến cáo, tất cả cấp dưới trực thuộc thành viên này sẽ không thể đặt cược?','strConfirmEnableDoubleCommMember':'Bạn có chắc muốn mở chức năng Nhân đôi hoa hồng đối với những cá cược bi thua trong Sportbooks  của thành viên này không?','editMultipleSportsbook2':'Edit Multiple Sportsbook 2','editSingleSportsbook2':'Sportsbook 2','popupTitle_multiple_dcs_23':'Tùy chỉnh đồng loạt  Lô Đề ','strAllowed':'Cho phép','editCockFighting':'Cockfighting','popupTitle_dcs_5':'Tùy chỉnh Virtual Sports','isAgentLevel':0,'custid':19978374,'lblAlertDisableDoubleCommMember':'Bạn có chắc muốn tắt chức năng Nhân đôi hoa hồng đối với  những cá cược bi thua trong Sportbooks  của thành viên này không?','popupTitle_multiple_dcs_5':'Tùy chỉnh đồng loạt Virtual Sports','roleidLiveCasino':2,'editMultipleVoidbridge':'Edit Multiple Macau Games','editCricket':'Cricket','confirmCloseMem':'Bạn có chắc muốn thay đổi trạng thái \"Khóa\" của thành viên này không?','confirmDisableNumberGameDownline':'Bạn có chắc muốn chặn thành viên này chơi Number Game?','strConfirmEnableDoubleComm':'Bạn có chắc muốn cho phép sử dụng chức năng Nhân đôi hoa hồng đối với những cá cược bị thua trong Sportbooks của tài khoản này không?','lblConfirmClearCredit':'Bạn có chắc sẻ thu hồi hạn mức tín dụng của tài khoản { tên đăng nhập } này?','langKey':'VI','editAllbet':'Allbet','editMultipleCockFighting':'Edit Multiple Cockfighting','roleidVirtualSports':2,'loginId':19978374,'strDisallowed':'Không cho','dcsLinks':['/site-ng2/dcs/viewcustomersetting','/ex-main/dcs/viewcustomersetting','/ex-main/dcs/viewcustomersetting','/ex-main/dcs/viewcustomersetting','/ex-main/dcs/viewcustomersetting'],'popupTitle_dcs_28':'Tùy chỉnh Xổ số','confirmCloseMemDownline':'Bạn có chắc muốn khóa thành viên này?\\nKhuyến cáo: tất cả cấp dưới của tài khoản này cũng sẽ bị khóa?','confirmSuspendMem':'Bạn có chắc muốn đình chỉ thành viên này?\\n Khuyến cáo: thành viên này sẽ không thể đặt cược?','roleid':3,'popupTitle_dcs_24':'Tùy chỉnh Bắn cá','popupTitle_dcs_26':'Tùy chỉnh Lucky 3','popupTitle_multiple_dcs_28':'Tùy chỉnh đồng loạt Xổ số','dcsProductIds':[23,24,5,28,26],'editVoidbridge':'Edit Macau Games','popupTitle_dcs_23':'Tùy chỉnh  Lô Đề ','confirmUnSuspendMem':'Bạn có chắc muốn thay đổi trạng thái \"Đình Chỉ\" cho thành viên này không?','editMultipleCricket':'Edit Multiple Cricket','loginUsername':'TY28A2'};</script><script>

</script>