<?php
@session_start();
$cus = $_GET["custid"];
$amount = $_GET["amt"];
$ma = $_SESSION['ma'];
?>
<!DOCTYPE html>
<html>
<head>
    <title>Given Credit TY28A200</title>
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Common/Agent.min.css?2017081602" rel="stylesheet" type="text/css" />
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Common/Popup.min.css?2017081602" rel="stylesheet" type="text/css" />
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/MemberInfo/CreditBalance/CreditTransfer.min.css?2017081602" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="page_popup" style="padding:10px">
    <table class="width-100per">
        <tr>
            <td>
                <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/ErrorMsg/ErrorMsgNew.min.css?2017081602" rel="stylesheet" type="text/css" />
                <script src="../ex-main/_Components/ErrorMsg/ErrorMsg.js?2017081602" type="text/javascript"></script>
                <div id="diverrmsg" class="width-100per">
                    <div id="spmsgerr" class="msgerr"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table class="tblPop c">
                    <tr>
                        <td class="l">Yen:</td>
                        <td class="right"><input type="text" id="txtamout" value="<?php echo $amount ?>" onkeypress="return OnkeyUpAmt(event);" maxlength="14" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="popup-bottom">
        <input type="button" value="Cancel" onclick="window.close();" class="btn btn-cancel" />&nbsp;
        <input type="button" value="Submit" onclick="UpdCredit('sup_master_new');" class="btn btnSubmit" /></div>
    <input id="custid" value="<?php echo $cus; ?>" type="hidden" />
    <input id="roleId" value="<?php echo $ma; ?>" type="hidden" />
</div>
<script src="../ex-main/_GlobalResources/Js/Core.js?2017081602" type="text/javascript"></script>
<script src="../ex-main/_MemberInfo/CreditBalance/Resources/CreditTransfer.js?2017081602" type="text/javascript"></script>
</body>
</html>
<input name="__RequestVerificationToken" type="hidden" value="dzWBFRZGjEI9XrAMQmhUo09nwsyi5bY7HLo611fKsddpGrEy5d34W2nWbJyUJsKDruknl73hOk3u9GoFx9fyOFw4nByER4K6hvzgoCWfiyxtZ4xcLSljrLP4PfxVGpnjTZQUuIFJDV08bTeBIk1mTdDFH0M1" />
<script type="text/javascript">var _page = {'lblConfirmClosed':'Your account was closed so you\u0027re forced to logout. Please contact your upline for the assistance.','lblconfirmclosesubacc':'No permission for sub-account.'};</script>
<script>
</script>