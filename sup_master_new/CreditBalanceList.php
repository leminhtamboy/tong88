<?php
include_once("../database.php");
$data = new database();
if(isset($_REQUEST['custid']))
    $ma = $_REQUEST['custid'];
else
    $ma = $_SESSION['ma'];
$dataMember=$data->ExcuteObjectList("select * from master where Ma_sup='$ma' order by Tinh_trang asc, Ma asc");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Credit/Balance</title>
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Common/Agent.min.css?2017081602" rel="stylesheet" type="text/css">
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Reports/Reports.min.css?2017081602" rel="stylesheet" type="text/css">
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/MemberInfo/PositionTakingList/PositionTakingList.min.css?2017081602" rel="stylesheet" type="text/css">
    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/MemberInfo/CustomerList/Print_Control/Print.min.css?2017081602" rel="stylesheet" type="text/css" media="print">
</head>
<body onload="initContextMenu(0, 110)" marginwidth="0" marginheight="0">
<input id="running-betlist-url" type="hidden" value="{RunningBetListUrl}">
<table class="width-100per">
    <tbody>
    <tr>
        <td>
            <div id="page_main">
                <div id="header_main">Tín dụng/Balance <a href="javascript:getPrint('tbl-container');" id="imgPrint" title="In" class="icon-print"></a></div>
                <div id="box_header">
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/SearchUserName_Control/SearchUserName_Control.min.css?2017081602" rel="stylesheet" type="text/css">
                                <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/SearchUserName_Control/styles.min.css?2017081602" rel="stylesheet" type="text/css">
                                <table id="tblSearch">
                                    <tbody>
                                    <tr>
                                        <td>Tên đăng nhập</td>
                                        <td><input type="text" class="text_normal" name="txtUserName" id="txtUserName" value="" onkeypress="onKeyPressUser('dSubmit',event);" onclick="onclickUser('Tên đăng nhập hoặc Tên/Họ')" onblur="onblurUser('Tên đăng nhập hoặc Tên/Họ')" autocomplete="off" style="position: relative;"></td>
                                        <td>Trạng thái</td>
                                        <td>
                                            <div id="box_option">
                                                <select id="statusFilter" name="statusFilter" style="position: relative;">
                                                    <option value="0">Tất cả</option>
                                                    <option value="1" selected="">Mở</option>
                                                    <option value="2">Bị đình chỉ</option>
                                                    <option value="3">Bị khóa</option>
                                                    <option value="4">Vô hiệu hóa</option>
                                                </select>
                                            </div>
                                            <div class="container-btn"><input id="dSubmit" type="button" value="Xác nhận" class="buttonSubmit" onclick="searchByUsername('CreditBalanceList.aspx','AgentList')"></div>
                                            <div class="shadow" id="shadow" style="position: absolute; top: 91px; left: 119px; visibility: hidden;">
                                                <div class="output" id="output">
                                                    <script src="https://mb.b88ag.com/ex-main/_Components/DoubleCommissionSearchUserName_Control/SearchUserName_Control.js?2017081602" type="text/javascript"></script>
                                                    <script src="https://mb.b88ag.com/ex-main/_Components/DoubleCommissionSearchUserName_Control/autocomplete.js?2017081602" type="text/javascript"></script>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div id="tbl-container">
                    <div id="boderRight">
                        <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/PagingHeader/PagingHeader.min.css?2017081602" rel="stylesheet" type="text/css">
                        <table id="tblHeader">
                            <tbody>
                            <tr>
                                <td class="bgleft"></td>
                                <td class="bgcenter">
                                    <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/Paging/Paging.css?2017081602" rel="stylesheet" type="text/css">
                                    <script src="https://mb.b88ag.com/ex-main/_Components/Paging/Paging.js?2017081602" type="text/javascript"></script>
                                    <div id="_PagingTop" class="pagingHiden" pagesize="500" currentindex="1" rowcount="500" pagecount="1"><span disabled="" id="btnFirst_PagingTop" type="button" onclick="_PagingTop.First(this)" class="icon pagingFirst"></span><span disabled="" id="btnPrev_PagingTop" type="button" onclick="_PagingTop.Move(this, -1)" class="icon pagingPrev"></span><span class="pagingSeperator"></span>Trang<input id="txt_PagingTop" type="text" class="pagingCurrent" maxlength="4" size="2" value="1" onkeydown="_PagingTop.DoEnter(event, '_PagingTop.Go()')">trên 1<span class="pagingSeperator"></span><span disabled="" id="btnNext_PagingTop" type="button" onclick="_PagingTop.Move(this, 1)" class="icon pagingNext"></span><span disabled="" id="btnLast_PagingTop" type="button" onclick="_PagingTop.Last(this)" class="icon pagingLast"></span></div>
                                    <script type="text/javascript">var _PagingTop = new Paging('_PagingTop');</script>
                                </td>
                                <td class="bgright">
                                    Số dòng
                                    <select id="sel_PagingTop" name="sel_PagingTop" onchange="_PagingTop.SetPageSize(this.value)">
                                        <option value="10">10</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="500" selected="">500</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div id="ctxContainer" style="display: block; position: absolute;">
                            <style>.ST { display: none; } table.showST .ST { display: table-cell; *display: block; } .FN { display: none; } table.showFN .FN { display: table-cell; *display: block; } .LN { display: none; } table.showLN .LN { display: table-cell; *display: block; } .CD { display: none; } table.showCD .CD { display: table-cell; *display: block; } .BL { display: none; } table.showBL .BL { display: table-cell; *display: block; } .YB { display: none; } table.showYB .YB { display: table-cell; *display: block; } .BC { display: none; } table.showBC .BC { display: table-cell; *display: block; } .OT { display: none; } table.showOT .OT { display: table-cell; *display: block; } .TO { display: none; } table.showTO .TO { display: table-cell; *display: block; } .LL { display: none; } table.showLL .LL { display: table-cell; *display: block; } .LI { display: none; } table.showLI .LI { display: table-cell; *display: block; } .UN { display: none; } table.showUN .UN { display: table-cell; *display: block; } </style>
                            <link href="https://mb.b88ag.com/ex-main/App_Themes/apo/Components/ShowHideColumn/ContextMenuColumns_Control.min.css?2017081602" rel="stylesheet" type="text/css">
                            <div id="PopupCtx" class="divMenuPopup" style="display: none; position: absolute; left: 359px; top: 34px;">
                                <table id="Popup" cols="ST,FN,LN,CD,BL,YB,BC,OT,TO,LL,LI,UN">
                                    <tbody>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('ST', 'chk_ST', '', '', true);" type="checkbox" id="chk_ST"><label for="chk_ST"><span class="padding">Trạng thái</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('FN', 'chk_FN', '', '', true);" type="checkbox" id="chk_FN"><label for="chk_FN"><span class="padding">Tên</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('LN', 'chk_LN', '', '', true);" type="checkbox" id="chk_LN"><label for="chk_LN"><span class="padding">Họ</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('CD', 'chk_CD', '', '', true);" type="checkbox" id="chk_CD"><label for="chk_CD"><span class="padding">Hạn mức tín dụng</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('BL', 'chk_BL', '', '', true);" type="checkbox" id="chk_BL"><label for="chk_BL"><span class="padding">Balance</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('YB', 'chk_YB', '', '', true);" type="checkbox" id="chk_YB"><label for="chk_YB"><span class="padding">Hôm qua Balance</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('BC', 'chk_BC', '', '', true);" type="checkbox" id="chk_BC"><label for="chk_BC"><span class="padding">Available Credit</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('OT', 'chk_OT', '', '', true);" type="checkbox" id="chk_OT"><label for="chk_OT"><span class="padding">Tiền chưa xử lý</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('TO', 'chk_TO', '', '', true);" type="checkbox" id="chk_TO"><label for="chk_TO"><span class="padding">Tổng tiền cược của Member</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('LL', 'chk_LL', '', '', true);" type="checkbox" id="chk_LL"><label for="chk_LL"><span class="padding">Lần đăng nhập cuối</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('LI', 'chk_LI', '', '', true);" type="checkbox" id="chk_LI"><label for="chk_LI"><span class="padding">IP đăng nhập</span></label></td>
                                    </tr>
                                    <tr>
                                        <td><input class="padding" onclick="onChangeColumn('UN', 'chk_UN', '', '', true);" type="checkbox" id="chk_UN"><label for="chk_UN"><span class="padding">Tên đăng nhập</span></label></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <table id="tblMain" class=" showST .ST  showFN .FN  showLN .LN  showCD .CD  showBL .BL  showYB .YB  showBC .BC  showOT .OT  showTO .TO  showLL .LL  showLI .LI  showUN .UN  tblRpt width-100per">
                            <thead>
                            <tr id="headerTop" class="RptHeader">
                                <td class="NO">#</td>
                                <td class="ST">Trạng thái</td>
                                <td>Tên đăng nhập</td>
                                <td class="FN">Tên</td>
                                <td class="LN">Họ</td>
                                <td class="CD">Hạn mức tín dụng</td>
                                <td class="BL">Balance</td>
                                <td class="YB">Số dư đến hết hôm qua</td>
                                <td class="BC">Available Credit</td>
                                <td class="OT">Tiền chưa xử lý</td>
                                <td class="TO">Tổng tiền cược của Member</td>
                                <td class="LL" style="width: 110px;">Lần đăng nhập cuối</td>
                                <td class="LI">IP đăng nhập</td>
                                <td class="UN">Tên đăng nhập</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $countMember = count($dataMember);
                            $stt = 0;
                            for($i = 0; $i< $countMember; $i++ ){
                            $idMember = $dataMember[$i]["id"];
                            $tinh_trang = $dataMember[$i]["Tinh_trang"];
                            $ma_member = $dataMember[$i]["Ma"];
                            //tinh trang la 1 ben bong that la mo ben minh la 0
                            $link_open = '../master/CreaditBalanceList.php?Ma=$ma_member';
                            $ten = $dataMember[$i]["Ten"];
                            $hoa_hong = $dataMember[$i]["Hoa_hong"];
                            $ngay_tao = $dataMember[$i]["ngayht"];
                            $ip_dang_nhap = $dataMember[$i]["ip"];
                            $tien_dua = $dataMember[$i]["Tien_dua"];
                            $textStatus = "Mở";
                            $classKhoaMo = "RowBgOpen";
                            $isClosed = 0;
                            if($tinh_trang == 1) // Mo
                            {
                                $textStatus = "Khoá";
                                $classKhoaMo = "closed-byUpline";
                                $isClosed = true;
                            }
                            if($tinh_trang == 2){
                                $textStatus = "Đình Chỉ";
                                $classKhoaMo = "suspended-byUpline";
                            }
                            $stt++;
                            $mas_dang_xet = $ma_member;
                            $sqlt = $data->ExcuteObjectOne(" SELECT SUM( Tien_dua ) AS tong FROM agent WHERE  `Ma_mas` = '$mas_dang_xet'");
                            $max = $sqlt['tong'];
                            $sql = "select ip,timedn from dangnhap where Ma = '$ma_member'";
                            $dtdn = $data->ExcuteObjectList($sql);
                            $ip = "0.0.0.0";
                            $nip = "Chưa đăng nhập";
                            if(count($dtdn) > 0 ) {
                                $ip = $dtdn[0]['ip'];
                                $nip = $dtdn[0]['timedn'];
                            }
                            ?>
                            <tr class="BgOdd">
                                <td class="w-order"><?php echo $stt; ?></td>
                                <td class="ST">Mở</td>
                                <td class="l"><a href="javascript:viewBal('agent', <?php echo $idMember ?>)"><?php echo $ma_member; ?></a></td>
                                <td class="FN l"><?php echo $ten; ?></td>
                                <td class="LN l">&nbsp;</td>
                                <td class="CD r"><a href="javascript:void('');" onclick="openTransfer('<?php echo $idMember ?>','<?php echo $tien_dua; ?>','<?php echo$ma_member ?>','1');return false;"><?php echo $tien_dua; ?></a></td>
                                <td class="BL r"><?php echo $max; ?></td>
                                <td class="YB r">0</td>
                                <td class="BC r">0</td>
                                <td class="OT r">0</td>
                                <td class="TO r">0</td>
                                <td class="LL bl_time"><?php echo $nip; ?></td>
                                <td class="LI bl_time"><a href="javascript:OpenIPInfo('<?php echo $ip ?>');" class="iplink"><?php echo $ip; ?></a>&nbsp;</td>
                                <td class="UN l"><a href="javascript:viewBal('agent', <?php echo $idMember ?>)"><?php echo $ma_member ?></a></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    </tbody>
</table>
<script src="../ex-main/_GlobalResources/Js/Core.js?2017081602" type="text/javascript"></script>
<script src="../ex-main/_Components/AGEWnd/AGEWnd.js?2017081602" type="text/javascript"></script>
<script src="../ex-main/_Components/ShowHideColumn/Resources/ContextMenuColumns_Control.js?2017081602" type="text/javascript"></script>
<script src="../ex-main/_MemberInfo/CreditBalance/Resources/CreditBalanceList.js?2017081602" type="text/javascript"></script>
<script type="text/javascript">
    var _page = {'tableId':'tblMain','cookiePrefix':'465181626','containerId':['headerTop'],'parentId':'ctxContainer','sessionId':'5hodpkide34szcopuavd2t02','wrnTransferSuccessful':'Chuyển khoản thành công','UserNameDefault':'Tên đăng nhập hoặc Tên/Họ','CustId':19978374,'PageSize':500};</script><script>
</script>
<div style="display: none; position: absolute; top: 0px; left: 0px; opacity: 0.5; background-color: white;">
    <div style="width: 100px; height: 100px; position: relative;"></div>
</div>
</body>
</html>