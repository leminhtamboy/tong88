<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=0.7, user-scalable=yes">
    <link rel="shortcut icon" type="image/x-icon" href="https://mb.b88ag.com/site-main/favico.ico" />
    <link href="../assets/bundles/common/common.min.css" rel="stylesheet"/>

    <link href="../assets/bundles/site-main/master.min.css" rel="stylesheet"/>

    <link href="../assets/bundles/default-index.min.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://mb.b88ag.com/assets/bundles/common/support-IE8.min.js?v=453"></script>

    <![endif]-->
</head>
<body>
<div id="page-popup" class="modal fade hidden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog ex-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="icon icon-close close" data-dismiss="modal" aria-label="Kh&#243;a"><span aria-hidden="true"></span></button>
                <h4 class="modal-title" id="result-detail-title"><span class="popup-title" id="popup-title"></span></h4>
            </div>
            <div class="modal-body">
                <iframe frameborder="0" allowtransparency="true" marginheight="0" marginwidth="0" scrolling="auto" id="popup-iframe"></iframe>
            </div>
        </div>
    </div>
</div>

<div class="modal fade message-modal" id="userMessageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body" id="userMessageModalBody">
                <button type="button" class="icon icon-close close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                <div id="msg-content">
                </div>
                <a class="link hide showmore-container">Th&#234;m</a>
                <a class="link hide showless-container">R&#250;t gọn</a>
            </div>
        </div>
    </div>
</div>
<aside class="sidebar" id="side-bar">
    <div class="partial-content customer-profile" id="profile">
        <div id="profile-content" data-introduce-nick-name-tooltip-cookie-name="" data-is-default-avatar="true" data-nick-name="">

            <a href="/ex-main/ProfilePicture" target="main" class="profile-avatar">
                <img id="logoProfileImg" width="50" height="50" src="../site-main/assets/images/icon_user_default.png" alt="" title="Nhấn vào đây để sửa hình đại diện">
            </a>

            <div class="userinfo">
                <span class="userName">TY28A2</span>
                <br>
                <div class="nickname-box">
                    <span id="spanNickNameID">Biệt danh: </span>
                    <a href="/ex-main/LoginOption?custId=19978374" class="linkConfigNow" id="linkNickNameID" target="main">
                        <span id="labelNickname">Cấu hình ngay</span>
                    </a>
                </div>
                <a class="otp" href="/site-main/otp/SetUpOtp" target="main">OTP: chưa kích hoạt</a>
            </div>
        </div>

        <!--<div id="tooltip-box" style="display:none;">
            <div>
                <div>Chào những thành viên thân mến !.Bây giò bạn có thể đăng nhập bằng chính Biệt Danh của mình</div>
            </div>
        </div>
        <div id="profileTooltip" style="display:none;">
            <div>Chào quý khách hàng !  Nhằm giúp bạn tránh được các trang web lừa đảo,</div>
            <div>chúng tôi sẽ cung cấp cho bạn một tùy chọn sử dụng hình đại diện cho riêng bạn. </div>
            <div>Khi hình này được tải lên thành công, bạn có thể thấy nó ở đây, nếu không, </div>
            <div>trang web đó có thể là một trang lừa đảo.  </div>
            <a class="change-picture-link" href="/ex-main/ProfilePicture" target="main" id="changePictureLink">
                Nhấn vào đây
            </a>
            &nbsp;để tải hình lên ngay bây giờ !
        </div>-->
    </div>
    <nav class="navigation" id="navigation">
        <div>
            <ul class="main-tab">
                <li id="main-menu-header" class="mainmenu active withripple" data-target-container="mainmenu-container">
                    <a>Menu ch&#237;nh</a>
                </li>
                <li class="accountinfo withripple" id="account-info-header"
                    data-target-container="accountinfo-container">
                    <a>Th&#244;ng tin</a>
                </li>
            </ul>
            <div class="extra-box">
                <span class="icon icon-user" id="user-icon"></span>
            </div>
        </div>
        <section id="main-menu-tab" class="mainmenu-container mCustomScrollbar " data-mcs-theme="minimal-dark" role="tablist" aria-multiselectable="true">
            <ul class="list-menu" id="main-menu-items">
                <li class="menu-item " id="menu-REPORTS">
                    <a class="withripple menu-opener parent-menu ">
                        <span class="txt-item pull-left"><span class="icon icon-reports"></span>Báo cáo</span>
                        <span class="pull-right icon-arrow-right"></span>
                    </a>
                    <ul class="sub-menu" style="display: none;">
                        <li class="sub-menu-item " id="sub-menu-AGENTOUTSTANDING">
                            <a href="outstanding.php" target="main" class="redirect-item">
                                Tiền chưa xử l&#253; của Master
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-AGENTWINLOSS">
                            <a href="javascript:void(0);" target="main" class="redirect-item">
                                Thắng thua của Agent
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-AGENTWINLOSTDETAIL">
                            <a href="winlossdetail.php" target="main" class="redirect-item">
                                Chi tiết thắng thua của Master
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-MATCHWINLOSTDETAIL">
                            <a href="/site-reports/matchwinlossdetail/index" target="main" class="redirect-item">
                                Chi tiết thắng thua theo trận
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-WINLOSSANALYSIS">
                            <a href="/site-reports/winlossanalysis/agent" target="main" class="redirect-item">
                                Ph&#226;n t&#237;ch hệ số thắng thua
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-WINLOSSANALYSISCHART">
                            <a href="/site-reports/winlossanalysischart/agent" target="main" class="redirect-item">
                                Biểu đồ đ&#225;nh gi&#225; thắng thua
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-WINLOSSBYPRODUCT">
                            <a href="/site-reports/WinLossByProduct/agent" target="main" class="redirect-item">
                                Win Loss By Product
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-COMMISSIONBYBETTYPE">
                            <a href="/site-reports/commissionbybettype/agent" target="main" class="redirect-item">
                                Hoa hồng theo loại cược
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-RACINGCOMMISSION">
                            <a href="/site-reports/racingcommission/agent" target="main" class="redirect-item">
                                Hoa hồng cho Racing
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-CANCELLEDBETS">
                            <a href="CancelledBet.php" target="main" class="redirect-item">
                                Cược bị hủy
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-STATEMENT">
                            <a href="/site-reports/Statement/SMAStatement" target="main" class="redirect-item">
                                Sao k&#234;
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-RESULT">
                            <a href="/site-results/Normal" target="main" class="redirect-item">
                                Kết quả
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-PLAYFORFUNDETAIL">
                            <a href="/site-reports/playforfun/masteroragent" target="main" class="redirect-item">
                                Chi tiết &#39;Chơi Thử&#39;
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-SCHEDULE">
                            <a href="/site-ng2/Schedule" target="main" class="redirect-item">
                                Lịch Mở Thưởng
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item " id="menu-MEMBERINFO">
                    <a class="withripple menu-opener parent-menu ">
                        <span class="txt-item pull-left"><span class="icon icon-memberinfo"></span>Thông tin Thành viên</span>
                        <span class="pull-right icon-arrow-right"></span>
                    </a>
                    <ul class="sub-menu" style="display: none;">
                        <li class="sub-menu-item " id="sub-menu-NEWAGENT">
                            <a href="addMaster.php" target="main" class="redirect-item">
                                Tạo Master mới
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-SUBACCOUNT">
                            <a href="javascript:void(0);" target="main" class="redirect-item">
                                T&#224;i khoản phụ
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-AGENTLIST">
                            <a href="memberList.php" target="main" class="redirect-item">
                                Danh s&#225;ch Master
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-CREDITBALANCE">
                            <a href="CreditBalanceList.php" target="main" class="redirect-item">
                                T&#237;n dụng/Balance
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-POSITIONTAKING">
                            <a href="/ex-main/_MemberInfo/PositionTakingList/Sportbook/SportbookPositionTakingList.aspx" target="main" class="redirect-item">
                                Position Taking (%)
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-CASINOPOSITIONTAKING">
                            <a href="/ex-main/_MemberInfo/PositionTakingList/Casino/CasinoPositionTakingList.aspx" target="main" class="redirect-item">
                                PT (%) của Casino
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-MEMBERCOMMISSION">
                            <a href="/ex-main/_MemberInfo/Commission/MemberCommissionSportbook.aspx" target="main" class="redirect-item">
                                Hoa hồng của Member
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-MEMBERWINLIMIT">
                            <a href="/ex-main/MemberInformation/WinLimit/Index" target="main" class="redirect-item">
                                Giới hạn thắng cược của Member
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-BETCHOICELIMIT">
                            <a href="/ex-main/MemberInformation/BetChoiceLimit/Index" target="main" class="redirect-item">
                                Giới hạn lựa chọn
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item " id="menu-TOTALBETS">
                    <a class="withripple menu-opener parent-menu ">
                        <span class="txt-item pull-left"><span class="icon icon-totalbets"></span>Tổng Cược</span>
                        <span class="pull-right icon-arrow-right"></span>
                    </a>
                    <ul class="sub-menu" style="display: none;">
                        <li class="sub-menu-item " id="sub-menu-HOUL">
                            <a href="/ex-totalbetsforecast/_TotalBets/HDPOU.aspx" target="main" class="redirect-item">
                                Cược chấp/T&#224;i Xỉu/Trực tuyến
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-MONEYLINE">
                            <a href="/ex-totalbetsforecast/_TotalBets/MoneyLine.aspx" target="main" class="redirect-item">
                                Moneyline
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-OE1X2DND">
                            <a href="/ex-totalbetsforecast/_TotalBets/OddEven1x2.aspx" target="main" class="redirect-item">
                                Lẻ/Chẵn + 1x2 + DND
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-CORRECTSCORE">
                            <a href="/ex-main/_BetList/BetList.aspx?type=CorrectScore" target="main" class="redirect-item">
                                Tỷ số ch&#237;nh x&#225;c
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-FTHTTOTALGOAL">
                            <a href="/ex-totalbetsforecast/_TotalBets/TotalGoal.aspx" target="main" class="redirect-item">
                                Tổng Số B&#224;n Thắng
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-PARLAY">
                            <a href="/ex-main/_BetList/BetList.aspx?type=MixParlay" target="main" class="redirect-item">
                                C&#225; cược tổng hợp
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-OUTRIGHT">
                            <a href="/ex-totalbetsforecast/_TotalBets/OutRight.aspx" target="main" class="redirect-item">
                                Cược Thắng
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-HTFT">
                            <a href="/ex-totalbetsforecast/_TotalBets/HTFT.aspx" target="main" class="redirect-item">
                                Hiệp 1/To&#224;n trận
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-FTHTFGLG">
                            <a href="/ex-totalbetsforecast/_TotalBets/FGLG.aspx" target="main" class="redirect-item">
                                B&#224;n Thắng Đầu/Cuối
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-BETTYPE13">
                            <a href="/ex-main/_BetList/BetList.aspx?bettype=13&amp;type=Bettype" target="main" class="redirect-item">
                                Giữ Sạch Lưới
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-BETTYPE24">
                            <a href="/ex-main/_BetList/BetList.aspx?bettype=24&amp;type=Bettype" target="main" class="redirect-item">
                                Nh&#226;n Đ&#244;i Cơ Hội
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-HOMEDRAWAWAYNOBET">
                            <a href="/ex-totalbetsforecast/_TotalBets/HomeDrawAwayNoBet.aspx" target="main" class="redirect-item">
                                Home/Draw/Away No Bet
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-BETTYPE26">
                            <a href="/ex-main/_BetList/BetList.aspx?bettype=26&amp;type=Bettype" target="main" class="redirect-item">
                                Cả hai/Một/Kh&#244;ng Đội N&#224;o Ghi B&#224;n
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-BETTYPE27">
                            <a href="/ex-main/_BetList/BetList.aspx?bettype=27&amp;type=Bettype" target="main" class="redirect-item">
                                To win to nil
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-BETTYPE28">
                            <a href="/ex-main/_BetList/BetList.aspx?bettype=28&amp;type=Bettype" target="main" class="redirect-item">
                                Cược Chấp 3 Chiều
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-NUMBERGAME">
                            <a href="/ex-totalbetsforecast/_TotalBets/NumberGame.aspx" target="main" class="redirect-item">
                                Number Game
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-HR">
                            <a href="/ex-totalbetsforecast/_TotalBets/Racing.aspx" target="main" class="redirect-item">
                                Racing
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item " id="menu-FORECAST">
                    <a class="withripple menu-opener parent-menu ">
                        <span class="txt-item pull-left"><span class="icon icon-forecast"></span>Dự đoán</span>
                        <span class="pull-right icon-arrow-right"></span>
                    </a>
                    <ul class="sub-menu" style="display: none;">
                        <li class="sub-menu-item " id="sub-menu-HOUL">
                            <a href="/ex-totalbetsforecast/_Forecast/FCHDPOU.aspx" target="main" class="redirect-item">
                                Cược chấp/T&#224;i Xỉu/Trực tuyến
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-LBL1X2">
                            <a href="/ex-totalbetsforecast/_Forecast/FC1X2.aspx" target="main" class="redirect-item">
                                1 X 2
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-SCOREMAP">
                            <a href="/ex-totalbetsforecast/ForecastScoreMap/Index" target="main" class="redirect-item">
                                Score Map
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-NG2">
                            <a href="/site-ng2/Forecast/RealBet" target="main" class="redirect-item">
                                L&#244; Đề
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item" id="menu-TRANSFER">
                    <a class="withripple menu-opener parent-menu redirect-item" href="/ex-main/_Transfer/Master/AgentList.aspx" target="main">
                        <span class="txt-item pull-left"><span class="icon icon-transfer"></span>Chuyển khoản</span>
                    </a>
                </li>
                <li class="menu-item " id="menu-VIEWLOG">
                    <a class="withripple menu-opener parent-menu ">
                        <span class="txt-item pull-left"><span class="icon icon-viewlog"></span>Nhật ký</span>
                        <span class="pull-right icon-arrow-right"></span>
                    </a>
                    <ul class="sub-menu" style="display: none;">
                        <li class="sub-menu-item " id="sub-menu-SETTING">
                            <a href="/site-viewlogs/Setting" target="main" class="redirect-item">
                                Thiết lập
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-STATUS">
                            <a href="/site-viewlogs/Status" target="main" class="redirect-item">
                                Trạng th&#225;i
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-CREDIT">
                            <a href="/site-viewlogs/Credit" target="main" class="redirect-item">
                                T&#237;n dụng
                            </a>
                        </li>
                        <li class="sub-menu-item " id="sub-menu-LOGIN">
                            <a href="/site-viewlogs/Login" target="main" class="redirect-item">
                                Đăng nhập
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </section>

        <!--[if (gt IE 8)|!(IE)]><!-->
        <section id="account-info-tab" class="accountinfo-container mCustomScrollbar" data-mcs-theme="minimal-dark">
            <iframe frameborder="0" id="accountinfo-content"
                    name="menu" marginwidth="0" marginheight="0" allowtransparency="true" class="accinfo-content" scrolling="auto"></iframe>
        </section>
        <!--<![endif]-->
        <!--[if lt IE 9]>
        <section id="account-info-tab" class="accountinfo-container">
            <iframe frameborder="0" id="accountinfo-content"
                    name="menu" marginwidth="0" marginheight="0" allowtransparency="true" class="accinfo-content" scrolling="auto"></iframe>
        </section>
        <![endif]-->

    </nav>
</aside>
<div class="content-wrapper" id="content-wrapper">
    <!-- Searchbar for mobile -->
    <div class="top-search-bar">
        <span class="icon-search"></span>
        <input type="text" class="top-search-input typeahead" placeholder="T&#224;i khoản" data-provide="typeahead" autocomplete="off">
        <span class="icon-close"></span>
    </div>
    <header class="header" id="header">
        <section class="navbar header-nav" id="header-nav">
            <span class="icon-collapse icon-hamburger withripple" id="hamburger-icon"></span>
            <ul class="nav navbar-nav list-top">
                <li class="visible-mobile"><span class="icon icon-home"></span></li>
                <li class="visible-mobile"><span class="icon icon-account-search"></span></li>
                <li class="visible-mobile"><span class="icon icon-message"><span class="circle-red"></span></span></li>
                <li class="visible-mobile"><span class="icon icon-bell"><span class="circle-red"></span></span></li>
                <li class="hidden-mobile datetime">
                    <span class="icon icon-time"></span>
                    <span class="txt-top" id="clock"
                          data-year="2017"
                          data-month="10"
                          data-day="8"
                          data-hour="13"
                          data-minute="11"
                          data-second="50"></span>
                </li>
                <li class="hidden-mobile dropdown language">
                    <a href="javascript:;" data-target="#" class="dropdown-toggle withripple language-selector-container" data-toggle="dropdown">
    <span class="icon icon-flag language-selector langFlag-vi-VN">
    </span>
                        <span class="language-text language-selector">
    </span>
                    </a>
                    <ul class="dropdown-menu list-language" id="language" data-currentlanguage="vi-VN">
                        <li class="en-US"><a class="changelanguage-link" data-value="en-US"><span class="icon-flag langFlag-en-us"></span>English</a></li>
                        <li class="zh-TW"><a class="changelanguage-link" data-value="zh-TW"><span class="icon-flag langFlag-zh-tw"></span>繁體中文</a></li>
                        <li class="zh-CN"><a class="changelanguage-link" data-value="zh-CN"><span class="icon-flag langFlag-zh-cn"></span>简体中文</a></li>
                        <li class="ja-JP"><a class="changelanguage-link" data-value="ja-JP"><span class="icon-flag langFlag-ja-jp"></span>日本語</a></li>
                        <li class="th-TH"><a class="changelanguage-link" data-value="th-TH"><span class="icon-flag langFlag-th-th"></span>ภาษาไทย</a></li>
                        <li class="ko-KR"><a class="changelanguage-link" data-value="ko-KR"><span class="icon-flag langFlag-ko-kr"></span>한국어</a></li>
                        <li class="vi-VN"><a class="changelanguage-link" data-value="vi-VN"><span class="icon-flag langFlag-vi-vn"></span>Tiếng Việt</a></li>
                    </ul>
                </li>
                <li class="dropdown setting">
                    <a id="btn-header-menu" data-target="#" class="dropdown-toggle withripple" data-toggle="dropdown">
                        <span class="icon icon-squares top-right-menu"></span>
                    </a>

                    <ul id="header-dropdown-menu" class="dropdown-menu list-setting">
                        <li class="visible-mobile dropdown-submenu language"><a href="javascript:;" data-target="#" class="dropdown-toggle withripple language-selector-container" data-toggle="dropdown">
    <span class="icon icon-flag language-selector langFlag-vi-VN">
    </span>
                                <span class="language-text language-selector">
    </span>
                            </a>
                            <ul class="dropdown-menu list-language" id="language" data-currentlanguage="vi-VN">
                                <li class="en-US"><a class="changelanguage-link" data-value="en-US"><span class="icon-flag langFlag-en-us"></span>English</a></li>
                                <li class="zh-TW"><a class="changelanguage-link" data-value="zh-TW"><span class="icon-flag langFlag-zh-tw"></span>繁體中文</a></li>
                                <li class="zh-CN"><a class="changelanguage-link" data-value="zh-CN"><span class="icon-flag langFlag-zh-cn"></span>简体中文</a></li>
                                <li class="ja-JP"><a class="changelanguage-link" data-value="ja-JP"><span class="icon-flag langFlag-ja-jp"></span>日本語</a></li>
                                <li class="th-TH"><a class="changelanguage-link" data-value="th-TH"><span class="icon-flag langFlag-th-th"></span>ภาษาไทย</a></li>
                                <li class="ko-KR"><a class="changelanguage-link" data-value="ko-KR"><span class="icon-flag langFlag-ko-kr"></span>한국어</a></li>
                                <li class="vi-VN"><a class="changelanguage-link" data-value="vi-VN"><span class="icon-flag langFlag-vi-vn"></span>Tiếng Việt</a></li>
                            </ul></li>
                        <li><a href="/ex-main/LoginOption?custId=19978374" target="main"><span class="icon icon-account-key"></span>Biệt danh</a></li>
                        <li><a href="/ex-main/ContextualCustomer/ProfilePicture?custId=19978374" target="main"><span class="icon icon-account-circle"></span>H&#236;nh đại diện</a></li>
                        <li><a href="/site-main/Password/ChangePassword" target="main"><span class="icon icon-lock"></span>Mật khẩu</a></li>

                        <li><a href="/site-main/SecurityCode/ChangeUserOwnSecurityCode" target="main"><span class="icon icon-sercuritycode"></span>M&#227; bảo mật</a></li>
                        <li><a href="/site-main/otp/SetUpOtp" target="main"><span class="icon icon-otp"></span>OTP</a></li>
                        <li class="visible-mobile"><a class="link-logout"><span class="icon icon-logout"></span>Đăng Xuất</a></li>
                    </ul>
                </li>
                <li class="hidden-mobile logout"><a class="icon icon-logout withripple" href="../logout.php" data-toggle="tooltip" data-placement="bottom" title="Đăng Xuất"></a></li>
            </ul>
        </section>
        <section class="header-msg" id="headerMessage">
            <span class="icon icon-home withripple" id="home-icon"></span>

            <div id="marqueeMessage">
        <span id="scroller">
            <span id="movebets-message" class="maquee movebets-message"></span>
            <marquee class="maquee live-message">Dear Valued Customer, please be informed that [GD Live Casino] MOBILE VERSION is performing maintenance until further notice. Sorry for the inconvenience	. <font id="private-message-content" color="green" live-message-url="/site-main/Message/Index?messageType=0">Private message: Attn:[Soccer] Due to bets erroneously accepted in corner danger play during the match between "Pacos de Ferreira No.of Corners -vs- Rio Ave No.of Corners" [PORTUGAL LEAGUE CUP - CORNERS - 8/10*Live*], bet ID: " 102782906261 " placed at (Score 0-0) are considered VOID. Parlay counted as (1). Sorry for the inconvenience caused!</font></marquee>

        </span>
            </div>

            <div class="nav-iconbox">
                <span class="icon icon-add special-message" id="btnSpecialMessage"></span>
                <div class="email-box private-message" id="btnPrivateMessage">
                    <span class="icon icon-message"></span>
                    <span class="value" id="privateMessageCounter">0</span>
                </div>
            </div>
        </section>
    </header>
    <main class="content" id="content" data-default-url="Dashboard.php">
    </main>
</div>

<script src="https://mb.b88ag.com/assets/bundles/common/common.min.js?v=453"></script>

<script src="https://mb.b88ag.com/assets/bundles/site-main/default.min.js?v=1.0.6486.18983"></script>

<script src="https://mb.b88ag.com/site-main/assets/bundles/default-index.min.js?v=1.0.6486.18983" type="text/javascript"></script>
</body>
</html>