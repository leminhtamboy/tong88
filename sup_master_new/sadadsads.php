<?php
include_once ("database.php");
@session_start();
if(!isset($_SESSION['ma_nguoi_dung'])) {
    exit();
}
$data = new database();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>UnderOver</title>
    <link href="./UnderOver_files/M_UnderOver.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="jquery.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="./UnderOver_files/M_Util.js"></script><script language="JavaScript" type="text/javascript" src="./UnderOver_files/M_UnderOverAll.js"></script>
    <script language="JavaScript" type="text/javascript" src="./UnderOver_files/MultiSport_Def.js"></script>
    <script language="JavaScript" type="text/javascript" src="./UnderOver_files/OddsUtils.js"></script>
    <script language="JavaScript" type="text/javascript" src="./UnderOver_files/OddsKeeper.js"></script>
    <script language="JavaScript" type="text/javascript" src="./UnderOver_files/UnderOver.js"></script>
    <script language="JavaScript" type="text/javascript" src="./UnderOver_files/jquery.mCustomScrollbar.js"></script>
    <script type="text/javascript" src="./UnderOver_files/trailblazerlib.js"></script>
    <script type="text/javascript" src="http://knockoutjs.com/downloads/knockout-3.4.2.js"></script>
    <style type="text/css">

        <!--

        #Layer1 {

            position: absolute;

            left: 1px;

            top: 20px;

            width: 424px;

            height: 311px;

            z-index: 1;

        }

        -->

        .nut

        {

            background-image: url(../images/layout/button.png);

            background-repeat: no-repeat;

            background-position: right top;

            text-shadow: 1px 1px 1px #FFF;

            color: #000000;

            float: left;

            height: 19px;

            padding-right: 4px;

            margin-right: 4px;

            margin-left: 4px;

            text-decoration: none;

            cursor: pointer;

        }

        .nutlive

        {

            background-image: url(../images/layout/button.png);

            background-repeat: no-repeat;

            background-position: right top;

            text-shadow: 1px 1px 1px #FFF;

            color: #000000;

            float: left;

            height: 19px;

            padding-right: 4px;

            margin-right: 4px;

            margin-left: 4px;

            text-decoration: none;

            cursor: pointer;

        }

        .loadlai

        {

            background-image: url(../images/layout/icon_RefreshWait_L.gif);

            background-repeat: no-repeat;

            background-position: right top;

        }
        .hover-color:hover{
            background-color:#f5eeb8 !important;
        }
        .hover-color:hover .tam_color{
            background-color:#f5eeb8 !important;
        }
    </style>
</head>
<?php
$soluongbanghai=30;
?>
<body id="UnderOver" onload="loaddautien()" lang="vn">
<style type="text/css">
    .promoNewVersion{
        position: fixed;
        width: 100%;
        height: 100vh;
        left: 0;
        top: 0;
        z-index: 999999;
    }

    .promoNewVersion .content{
        font-family: Tahoma, "Microsoft YaHei", "Microsoft JhengHei";
        font-size: 12px;
        width: 492px;
        padding: 5px;
        border: 3px solid #fff;
        background-color: #fff;
        border-radius: 5px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.4);
        text-align: center;
        position: absolute;
        top: 230px;
        margin-top: -220px;
        left: 145px;
    }

    .promoNewVersion .close{
        background: url(http://gtr-1-1.bongstatic.com/template/sportsbook/public/images/layout/icon_UI01.png) 132px -18px;
        position: absolute;
        right: -10px;
        top: -10px;
        width: 16px;
        height: 16px;
        border-radius: 16px;
        border: 3px solid #fff;
        background-color: #ccc;
        color: #fff;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.2);
        cursor: pointer;
    }

    .promoNewVersion .close:hover{
        background-color: #666;
    }

    .promoNewVersion h1.title{
        color: #f27321;
        margin-top: 0;
    }

    .promoNewVersion ul{
        list-style: none;
        text-align: left;
        margin-left: 5.3em;
    }

    .promoNewVersion li{
        background: url(http://gtr-1-1.bongstatic.com/template/sportsbook/public/images/layout/welcom-icon.png) left top no-repeat;
        padding: 0 10px 6px 30px;
        font-size: 16px;
    }

    .promoNewVersion .btnAera{
        padding: 0 10px;
    }

    .promoNewVersion .btnAera > div{
        padding: 8px;
        cursor: pointer;
    }
    .promoNewVersion .btnAera > div:hover{
        opacity: .8;
    }
    .promoNewVersion .btnAera > div.btn{
        display: inline-block;
        color: #fff;
        font-size: 1.5em;
        box-sizing: border-box;
        border-radius: 3px;
        width: 72%;
        background-color: #7490c3;
    }

    .promoNewVersion .btnAera > div.second{
        width: 25%;
        background-color: #b1b1b1;
        margin-left: 1%;
    }
    .promoNewVersion .btnAera > div.third{
        color: #bebebe;
        text-align: right;
        padding: 4px 8px 2px;
        text-decoration: underline;
    }
</style>
<script>
    jQuery("#PromoNewVersionPopup").hide();
    function HidePromoPage(isHide){
        jQuery("#PromoNewVersionPopup").hide();
    }
</script>
<div id="PromoNewVersionPopup" class="promoNewVersion" style="display: none">
    <div class="content">
        <div class="close" title="ĐÓNG" onclick="javascript:HidePromoPage(true);"></div>
        <img src="http://gtr-1-1.bongstatic.com/template/sportsbook/public/images/layout/welcom.png?v=20161216">
        <h1 class="title">Giới thiệu Phiên bản mới</h1>
        <ul>
            <li>Cập nhật Tỷ lệ cược liên tục bởi công nghệ Push</li>
            <li>Cược nhanh</li>
            <li>Thiết Kế Web Có Độ Phản Hồi Cao</li>
        </ul>
        <div class="btnAera">
            <div class="btn" onclick="javascript:void(0);">HÃY THỬ NGAY BÂY GIỜ</div>
            <div class="btn second" onclick="javascript:HidePromoPage(true);">Để sau</div>
            <div class="third" onclick="javascript:HidePromoPage();">Không hiển thị nữa</div>
        </div>
    </div>
</div>
<div style="width: 100%; height: 15px;"></div>
<?php include_once("cotphai.php"); ?>
<div id="lo" style="margin-top: -20px;">
    <div id="column1" class="titleBar">
        <div class="title"><?php getLang('su_kien_hom_nay')?></div>
        <div class="right"> <a id="b_SwitchToParlay1" href="javascript:window.parent.frames[1].ShowOdds('P');"   class="button mark" style="display: block;" title="Parlay"><span><?php getLang('ca_cuoc_tong_hop')?></span></a>
            <div id="selLeagueType_Div1" tabindex="6" hidefocus="" onkeypress="onKeyPressSelecter(&#39;selLeagueType&#39;,event);return false;" onclick="onClickSelecter(&#39;selLeagueType&#39;);" class="button select icon">
                <input type="hidden" name="selLeagueType" id="selLeagueType" value="0">
                <span id="selLeagueType_Txt" title="All Markets">
                            <div id="selLeagueType_Icon1" class="icon_All"></div>
                        </span>
                <ul id="selLeagueType_menu" class="submenu">
                    <li title="Tất Cả Trận Đấu" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick="setSelecter(&#39;selLeagueType&#39;,this,&#39;0&#39;,true);changeLeagueType(0);">
                        <div class="icon_All"></div>
                        Tất Cả Trận Đấu</li>
                    <li title="Các Trận Đấu Chính" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick="setSelecter(&#39;selLeagueType&#39;,this,&#39;1&#39;,true);changeLeagueType(1);">
                        <div class="icon_Main"></div>
                        Các Trận Đấu Chính</li>
                </ul>
            </div>
            <div id="aSorter_Div" tabindex="6" hidefocus="" onkeypress="onKeyPressSelecter(&#39;aSorter&#39;,event);return false;" onclick="onClickSelecter(&#39;aSorter&#39;);" class="button select icon ">
                <input type="hidden" name="aSorter" id="aSorter" value="1">
                <span id="aSorter_Txt" title="Normal Sorting">
                            <div id="aSorter_Icon" class="icon_ST"></div>
                        </span>
                <ul id="aSorter_menu" class="submenu" style="width: 170px;">
                    <li title="Sort by Time" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick="javascript::location('UnderOver.php?action='">
                        <div class="icon_ST"></div>
                        Chọn Lựa Theo Thời Gian</li>
                    <li title="Normal Sorting" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick="setSelecter(&#39;aSorter&#39;,this,&#39;0&#39;,true);">
                        <div class="icon_NO"></div>
                        Lựa chọn bình thường</li>
                </ul>
            </div>
            <a href="javascript:true;" class="button selectLeague" style="display:inline-block;" title="Select League">
                        <span>
                            <div id="League_New" class="">
                                <div id="SelLeagueIcon"  class="displayOff">
                                    <div class="icon">
                                    </div>
                                </div>
                                <div class="events">
                                    <div class="normal">(</div><div id="CustSelL1" class="selected displayOff">0</div><div id="AllSelL1" class="displayOn">24</div><div class="normal">/</div><div id="TotalLeagueCnt" class="normal">77</div><div class="normal">)</div>
                                </div>
                                <?php getLang('giai')?></div>
                            <div id="League_Old" class="displayOff">Select League</div>
                        </span>
            </a>
            <div id="disstyle_Div" tabindex="6" hidefocus="" onkeypress="//onKeyPressSelecter(&#39;disstyle&#39;,event);return false;" onclick="//onClickSelecter(&#39;disstyle&#39;);" class="button select icon">
                <input type="hidden" name="disstyle" id="disstyle" value="3">
                <span id="disstyle_Txt" title="Double Line">
                            <div id="disstyle_Icon" class="icon_DL"></div>
                        </span>
                <ul id="disstyle_menu" class="submenu" style="visibility: hidden;">
                    <li title="Một Dòng" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick="setSelecter(&#39;disstyle&#39;,this,&#39;1&#39;,true);changeDisplayMode(&#39;1&#39;,&#39;bong88.com&#39;); parent.focus();">
                        <div class="icon_SL"></div>
                        Một Dòng</li>
                    <li title="Double Line" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick="setSelecter(&#39;disstyle&#39;,this,&#39;3&#39;,true);changeDisplayMode(&#39;3&#39;,&#39;bong88.com&#39;); parent.focus();">
                        <div class="icon_DL"></div>
                        Dòng Kép</li>
                    <li title="Toàn Thời Gian" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick="setSelecter(&#39;disstyle&#39;,this,&#39;1F&#39;,true);changeDisplayMode(&#39;1F&#39;,&#39;bong88.com&#39;); parent.focus();">
                        <div class="icon_FT"></div>
                        Toàn Thời Gian</li>
                    <li title="Hiệp 1" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick="setSelecter(&#39;disstyle&#39;,this,&#39;1H&#39;,true);changeDisplayMode(&#39;1H&#39;,&#39;bong88.com&#39;); parent.focus();">
                        <div class="icon_HT"></div>Hiệp 1</li>
                </ul>
            </div>
            <div id="selOddsType_Div" tabindex="6" hidefocus="" onkeypress="onKeyPressSelecter('selOddsType',event);return false;" onclick="onClickSelecter('selOddsType');" class="button select icon">
                <input type="hidden" name="selOddsType" id="selOddsType" value="4">
                <span id="selOddsType_Txt" title="Malay Odds"><div id="selOddsType_Icon" class="icon_MY"></div></span>
                <ul id="selOddsType_menu" class="submenu" style="visibility: hidden;">
                    <li title="Hong Kong Odds" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick=""><div class="icon_HK"></div>Hong Kong Odds</li>
                    <li title="Decimal Odds" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick=""><div class="icon_Dec"></div>Decimal Odds</li>
                    <li title="Malay Odds" onmouseover="onOver(this)" onmouseout="onOut(this)" onclick="top.mainFrame.location='mytong.php'"><div class="icon_MY"></div>Malay Odds</li>
                </ul>
            </div>
            <a href="#" onclick="load1();" id="btnRefresh_D" class="button" style="" title="Refresh">
                        <span id="load1">
                            <div class="icon-refresh" title="Refresh"></div>
                        </span>
            </a>
            <a href="#" onclick="load2();" id="btnRefresh_L" class="button" title="Live">
                        <span id="load2">
                            <div class="icon-refresh" title="Trực tiếp"></div>
                        </span>
            </a>
        </div>
    </div>
    <div id="lo_1">
        <div class="column3" id="column2">
            <div id="OddsTr" style="display: block;">
                <div class="tabbox" id="tabbox">
                    <div class="tabbox_F" id="oTableContainer_L"></div>
                    <div class="tabbox_F" id="oTableContainer_D">
                        <table id="tmplTable" class="oddsTable" width="100%" cellpadding="0" cellspacing="0" border="0">
                            <thead>
                            <tr>
                                <th width="6%" nowrap="">Giờ</th>
                                <th width="34%" colspan="2" align="left" class="even">Lựa Chọn</th>
                                <th style="min-width:78px;max-width:90px;" nowrap="nowrap" class="text-ellipsis" title="Toàn Thời Gian Handicap">FT. HDP</th>
                                <th style="min-width:78px;max-width:90px;" nowrap="nowrap" class="text-ellipsis" title="Toàn Thời Gian Over/Under">FT. O/U</th>
                                <th style="min-width:48px;max-width:60px;" nowrap="nowrap" class="text-ellipsis" title="Toàn Thời Gian 1X2">FT. 1X2</th>
                                <th style="min-width:78px;max-width:90px;" nowrap="nowrap" class="even tabt_L text-ellipsis" title="Hiệp 1 Handicap">1H. HDP</th>
                                <th style="min-width:78px;max-width:90px;" nowrap="nowrap" class="even text-ellipsis" title="Hiệp 1 Over/Under">1H. O/U</th>
                                <th style="min-width:48px;max-width:60px;" nowrap="nowrap" class="even text-ellipsis" title="Hiệp 1 1X2">1H. 1X2</th>
                                <th width="1" nowrap="nowrap"></th>
                            </tr>
                            </thead>
                            <tbody data-bind="foreach: { data: mang_giai , as: 'giai' }" class="bindingmana">
                            <tr id="l_99" valign="middle" onclick="refreshData_D();"  >
                                <td class="tabtitle"></td>
                                <td colspan="7" class="tabtitle"><span id="fav_641" class="iconOdds favorite"></span><!--ko text: giai.TenGiai--><!--/ko--></td>
                                <td colspan="2" class="tabtitle" align="right"><a name="btnRefresh_D" onclick="load1()" class="btnIcon right" title="Tải Lại"><div class="icon-refresh" title="Tải Lại"></div></a></td>
                            </tr>
                            <!-- ko foreach: { data: tran_dau, as: 'tran' } -->
                            <tr id="text: tran.MaTran" class="bgcpe hover-color" data-bind="attr: { 'style' : tran.BGColor }">
                                <td rowspan="2" class="text_time">
                                    <font color="red"><!--ko text: tran.TiSo--><!--/ko--></font>
                                    <br />
                                    <!--ko text: tran.ThoiGian--><!--/ko-->
                                </td>
                                <td rowspan="2" class="line_unR" valign="top">
                                    <div data-bind="attr: { class: tran.KQ_HDP != '' && tran.KQ_HDP !='0'  ? 'FavTeamClass' : 'UdrDogTeamClass' }">
                                        <!--ko text: tran.Doi1--><!--/ko-->
                                    </div>
                                    <div data-bind="attr: { class: tran.KQ_HDP2 != '' && tran.KQ_HDP2 !='0'  ? 'FavTeamClass' : 'UdrDogTeamClass' }" >
                                        <!--ko text: tran.Doi2--><!--/ko-->
                                        <div class="HdpGoalClass"><!--ko text: tran.KetQua --><!--/ko--></div>
                                </td>
                                <td align="right" rowspan="2" nowrap="nowrap">
                                    <!--ko if: tran.htc == '0' -->
                                    <a href="javascript:true;" title="Live Information"><span class="iconOdds liveInfo"></span></a>
                                    <a href='#' title="Live Chart"><span class="iconOdds liveChart"></span></a>
                                    <a href='javascript:true;' title="Add My Favorite"><span name="fav_0117329460" class="iconOdds favorite"></span></a>
                                    <!--/ko-->
                                </td>
                                <td valign="top" class="none_rline none_dline">
                                    <div class="line_divL HdpGoalClass">
                                        <!--ko text: tran.KQ_HDP--><!--/ko-->
                                        <br />
                                        <!--ko text: tran.KQ_HDP2--><!--/ko-->
                                    </div>
                                    <div class="line_divR OddsDiv ">
                                        <a class="UdrDogOddsClass"
                                           data-bind="attr: { 'data-react' : tran.a  , style: tran.KQ_Nha < 0  ? 'color:#B50000!important;cursor:pointer' : 'cursor:pointer'}"
                                           onclick="xu_ly_them_cuoc_xanh(this)"
                                        >
                                            <!--ko text: tran.KQ_Nha--><!--/ko-->
                                        </a>
                                        <br>
                                        <a class="UdrDogOddsClass"
                                           onclick="xu_ly_them_cuoc_xanh(this)"
                                           data-bind="attr: { 'data-react' : tran.b  , style: tran.KQ_Khach < 0  ? 'color:#B50000!important;cursor:pointer' : 'cursor:pointer'}"
                                        >
                                            <!--ko text: tran.KQ_Khach--><!--/ko-->
                                        </a>
                                        <br>
                                    </div>
                                </td>
                                <td valign="top" class="none_dline none_rline">
                                    <div class="line_divL HdpGoalClass">
                                        <!--ko text: tran.KQ_BanThang--><!--/ko-->
                                        <br />
                                        u
                                    </div>
                                    <div class="line_divR OddsDiv ">
                                        <a class="UdrDogOddsClass"
                                           onclick="xu_ly_them_cuoc_xanh(this)"
                                           data-bind="attr: { 'data-react' : tran.c  , style: tran.KQ_Tren < 0  ? 'color:#B50000!important;cursor:pointer' : 'cursor:pointer'}"
                                        >
                                            <!--ko text: tran.KQ_Tren--><!--/ko-->
                                        </a>
                                        <br/>
                                        <a class="UdrDogOddsClass" onclick="xu_ly_them_cuoc_xanh(this)"
                                           data-bind="attr: { 'data-react' : tran.d  , style: tran.KQ_Duoi < 0  ? 'color:#B50000!important;cursor:pointer' : 'cursor:pointer'}"
                                        >
                                            <!--ko text: tran.KQ_Duoi--><!--/ko-->
                                        </a>
                                    </div>
                                </td>
                                <td rowspan="2" align="right" valign="top" class="tabt_R">
                                    <div class="line_divL line_divR UdrDogOddsClass ">
                                        <a onclick="xu_ly_them_cuoc_xanh(this)"
                                           data-bind="attr: { 'data-react' : tran.e ,'style': 'cursor:pointer;display:block'  }">
                                            <!--ko text: tran.KQ_1x2_1--><!--/ko-->
                                        </a>
                                        <a onclick="xu_ly_them_cuoc_xanh(this)" data-bind="attr: { 'data-react' : tran.f ,'style': 'cursor:pointer;display:block' }" >
                                            <!--ko text: tran.KQ_1x2_2--><!--/ko-->
                                        </a>
                                        <a   onclick="xu_ly_them_cuoc_xanh(this)" data-bind="attr: { 'data-react' : tran.g ,'style': 'cursor:pointer;display:block'  }">
                                            <!--ko text: tran.KQ_1x2_3--><!--/ko-->
                                        </a>
                                    </div>
                                </td>
                                <td valign="top" class="none_rline none_dline">
                                    <div class="line_divL HdpGoalClass">
                                        <!--ko text: tran.Hiep1_HDP--><!--/ko-->
                                        <br/>
                                        <!--ko text: tran.Hiep1_HDP2--><!--/ko-->
                                    </div>
                                    <div class="line_divR OddsDiv ">
                                        <a class="UdrDogOddsClass"
                                           onclick="xu_ly_them_cuoc_xanh(this)"
                                           data-bind="attr: { 'data-react' : tran.a_1  , style: tran.Hiep1_Nha < 0  ? 'color:#B50000!important;cursor:pointer' : 'cursor:pointer'}"
                                        >
                                            <!--ko text: tran.Hiep1_Nha--><!--/ko-->
                                        </a>
                                        <br>
                                        <a class="UdrDogOddsClass"
                                           onclick="xu_ly_them_cuoc_xanh(this)" data-bind="attr: { 'data-react' : tran.b_1  , style: tran.Hiep1_Khach < 0  ? 'color:#B50000!important;cursor:pointer' : 'cursor:pointer'}"><!--ko text: tran.Hiep1_Khach--><!--/ko--></a>
                                        <br>
                                    </div>
                                </td>
                                <td valign="top" class="none_dline none_rline">
                                    <div class="line_divL HdpGoalClass">
                                        <!--ko text: tran.Hiep1_BanThang--><!--/ko-->
                                        <br />
                                        u
                                    </div>
                                    <div class="line_divR OddsDiv ">
                                        <a class="UdrDogOddsClass" onclick="xu_ly_them_cuoc_xanh(this)" data-bind="attr: { 'data-react' : tran.c_1  , style: tran.Hiep1_Tren < 0  ? 'color:#B50000!important;cursor:pointer' : 'cursor:pointer'}"><!--ko text: tran.Hiep1_Tren--><!--/ko--></a><br>
                                        <a class="UdrDogOddsClass" onclick="xu_ly_them_cuoc_xanh(this)" data-bind="attr: {'data-react' : tran.d_1  , style: tran.Hiep1_Duoi < 0  ? 'color:#B50000!important;cursor:pointer' : 'cursor:pointer'}"><!--ko text: tran.Hiep1_Duoi--><!--/ko--></a><br>
                                    </div>
                                </td>
                                <td rowspan="2" align="right" valign="top">
                                    <div class="line_divL line_divR UdrDogOddsClass ">
                                        <a onclick="xu_ly_them_cuoc_xanh(this)" data-bind="attr: { 'data-react' : tran.e_1 ,'style': 'cursor:pointer;display:block' }"><!--ko text: tran.Hiep1_1x2_1--><!--/ko--></a>
                                        <a onclick="xu_ly_them_cuoc_xanh(this)" data-bind="attr: { 'data-react' : tran.f_1 ,'style': 'cursor:pointer;display:block' }"><!--ko text: tran.Hiep1_1x2_2--><!--/ko--></a>
                                        <a onclick="xu_ly_them_cuoc_xanh(this)" data-bind="attr: { 'data-react' : tran.g_1 ,'style': 'cursor:pointer;display:block' }"><!--ko text: tran.Hiep1_1x2_3--><!--/ko--></a>
                                    </div>
                                </td>
                                <!--ko if: tran.thong_ke != '' -->
                                <td rowspan="2" align="center">
                                    <a
                                        data-bind="attr: { 'href' : 'javascript:openThongKe('+tran.thong_ke+')'}" title="Statistic Information"><span class="iconOdds stats"></span></a>
                                    <br>
                                    <a href="#"  title="Score Map" style="padding-right:1px;"><span class="iconOdds scoreMap"></span></a><br/>
                                    <a href='#' title="Live Chart" style="padding-right: 1px"><span class="iconOdds liveChart"></span></a>
                                </td>
                                <!--/ko-->
                                <!--ko if: tran.thong_ke == '' -->
                                <td rowspan="2" align="center"><br></td>
                                <!--/ko-->
                            </tr>
                            <tr>
                                <td class=" <?php echo $hienthimau ?> moreBetType displayOff" colspan="10"></td>
                            </tr>
                            <!-- /ko -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="TrNoInfo" style="display: none;">
                <table class="oddsTable" width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                    <tr>
                        <td align="center" class="tabtitle" style="border-radius: 0;">Không có trận đấu nào vào lúc này.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="BetList" align="center" style="display: block;"><img src="images/layout/loading.gif" vspace="2"/></div>
            <div>
                <div style="width:780px; font-size: 9px; font-family: Arial; color: #666666; text-align: center; margin-top:10px;"> © Copyright 2010-2012. bong88.com. All Rights Reserved.</div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script language="JavaScript" type="text/javascript">
    SelMainMarket=parseInt("0",10);
    var REFRESH_INTERVAL_L = 20000;
    var REFRESH_INTERVAL_D = 60000;
    var REFRESH_COUNTDOWN = 720;
    var RES_REFRESH = "Tải Lại";
    var RES_PLEASE_WAIT = "Xin vui lòng chờ đợi";
    var RES_UNDER = "u";
    var RES_LIVE = "Trực tiếp";
    var PAGE_MARKET="t";
    var RES_DRAW="Hòa";
    var RES_MORE="More Bet Types";
    var RES_DOMAIN = "bong88.com";
    var RES_NOW = "Now";
    var RES_NormalSort="Lựa chọn bình thường";
    var RES_SortByTime="Chọn Lựa Theo Thời Gian";
    var DisableCasino = "true";
    var SyncCasino = "true";
    var x = 2;
    var ll1 = 60;
    var ll2 = 20;
    var ao_l_1=60;
    var ao_l_2=20;
    var w;
    var tempRefresh = 0; var temp1 = 0; var tempLive = 0; var i; var j; var k;
    var doi_tuong_2=null;
    k = self.setInterval(function() { setChop(); }, 1000);
    function startCountDonw1(){
        i = self.setInterval(function() { l1(); }, 1000);
    }
    function startCountDonw2() {
        j = self.setInterval(function() { l2(); }, 1000);
    }
    function setChop() {
        if (temp1 % 2 == 0) {
            //$(".chop").css("background", "#FFAF8C");
        } else {
            //$(".chop").css("background", "");
        }
        temp1 = temp1 + 1;
    }
    function openThongKe(id){
        window.open('/Stat/'+id+'.htm', 'Statistic', 'width=1000,height=600,top=50,left=100,toolbars=no,scrollbars=yes,status=no,resizable=yes');
    }
    function hienthilai1() {
        startCountDonw1();
    }
    function hienthilai2() {
        startCountDonw2();
    }
    function clearAllload2(la_clear) {
        document.getElementById("btnRefresh_L").setAttribute("class","button disable");
        clearInterval(j);
        ll2=20;
    }
    function clearAllload1(la_clear) {
        document.getElementById("btnRefresh_D").setAttribute("class","button disable");
        clearInterval(i);
        ll1=60;
    }
    function l1() {
        ll1 = ll1 -1;
        ao_l_1=ao_l_1-1;
        if(ll1 > 0){
            document.getElementById('load1').innerHTML = '<div class="icon-refresh" title="Tải Lại"></div>'+ll1;
        }
        else {
            btnstop(0);
            //btnstart(0);
            ll1=60;
            clearAllload1(true);
            xu_ly_load1();
            hienthilai1();
        }
    }
    function l2() {
        ll2 = ll2 -1;
        ao_l_2=ao_l_2-1;
        if(ll2 > 0) {
            document.getElementById('load2').innerHTML = '<div class="icon-refresh" title="Tải Lại"></div>'+ao_l_2;
        }
        /*if(ll2==2){
            btnstop(1);
        }*/
        if(ll2==0){
            btnstop(1);

            clearAllload2(true);

            xu_ly_load2();

            hienthilai2();
        }
        if(ao_l_2==0){
            //xu_ly_load2();
            ao_l_2=20;
        }
    }
    function load() {
    }
    function loaddautien() {
        document.getElementById('BetList').style.display='block';
        //xu_ly_load2();
        //hienthilai2();
        xu_ly_load_dau()

    }
    function Loadtrongtruonghopxanh() {
        top.mainFrame.location = 'A_my_xanh.php';
    }
    function load1() {
        btnstop(0);
        clearAllload1(true);
        xu_ly_load1();
        hienthilai1();
        //btnstart(0);
        //isLoad1=false;
    }
    function load2() {
        btnstop(1);
        clearAllload2(true);
        xu_ly_load2();
        hienthilai2();
        ao_l_2=20;
        //btnstart(1);
        //isLoad2=false;
        //btnstart(1);
        //ll2=20;
    }
    function setOutputcomment2(){
        if(doi_tuong_2.readyState == 4){
            document.getElementById('oTableContainer_L').innerHTML=doi_tuong_2.responseText;
            document.getElementById('BetList').style.display='none';
            btnstart(1);
            isLoad2 = false;
        }
    }
    function setOutputcomment1(){
        if(httpObject1.readyState == 4){
            document.getElementById('oTableContainer_D').innerHTML=httpObject1.responseText;
            btnstart(0);
            isLoad1 = false;
        }
    }
    isLoad2 = false;
    isLoad1 = false;
    soluotclick_1=0;
    soluotclick_2=0;
    function xu_ly_load_dau(){
        jQuery.ajax({
            type:'get',
            url:'A_Tren_My.php',
            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
            cache: false,
            asyncBoolean:false,
            success:function(result) {
                document.getElementById('oTableContainer_L').innerHTML=result;
                btnstart(1);
                hienthilai2();
                xu_ly_load1();
                hienthilai1();
            }
        });


    }
    function xu_ly_load2() {
        jQuery.ajax({
            type:'get',
            url:'A_Tren_My.php',
            contentType:'application/x-www-form-urlencoded; charset=UTF-8',
            cache: false,
            asyncBoolean:false,
            success:function(result) {
                document.getElementById('oTableContainer_L').innerHTML=result;
                btnstart(1);
                isLoad2 = false;
                soluotclick_2=0;
            }
        });
    }
    function ViewModel() {
        var self = this;
        self.mang_giai = ko.observableArray();
    };
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
    var isClickXanh = false;
    function xu_ly_load1(){

        /*if(isLoad1 == false){
            isLoad1=true;
            jQuery.ajax({
                type:'get',
                url:'A_Duoi_MY.php',
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                cache: false,
                asyncBoolean:false,
                success:function(result) {
                    document.getElementById('oTableContainer_D').innerHTML=result;
                    btnstart(0);
                    isLoad1 = false;
                    soluotclick_1=0;
                }
            });
        }else{
            soluotclick_1++;
            if(soluotclick_1 > 6){
                var mota="xanh";
                var ma_nguoi_dung='<?php echo $_SESSION['ma_nguoi_dung'] ?>';
					jQuery.ajax({
                    type: 'POST',
                    data:{ma_nguoi_dung:ma_nguoi_dung,mota:mota},
                    url: "khongdanguoidung.php",
                    error:function(err){
                        console.log(err);
                    },
                    success:function(result){
						top.location ="../login888.aspx";
                    }
                 });
				}else{
					return;
				}
			}*/
        //var viewModel = null;
        //
        if(isClickXanh == false) {
            jQuery.getJSON("A_Duoi_MY.php",function (json) {
                setTimeout(function () {
                    btnstart(0);
                    isClickXanh = false;
                    viewModel.mang_giai(json);
                }, 3000)
            });
        }
        isClickXanh = true;
    }
    // Get the HTTP Object
    function getHTTPObject(){
        if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
        else if (window.XMLHttpRequest) return new XMLHttpRequest();
        else {
            alert("Your browser does not support AJAX.");
            return null;
        }
    }
    function getHTTPObject1(){
        if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
        else if (window.XMLHttpRequest) return new XMLHttpRequest();
        else {
            alert("Your browser does not support AJAX.");
            return null;
        }
    }
    function getHTTPObject2() {
        if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
        else if (window.XMLHttpRequest) return new XMLHttpRequest();
        else {
            alert("Your browser does not support AJAX.");
            return null;
        }
    }
    function setOutputcomment(){
        if(httpObject.readyState == 4){
            window.parent.frames[1].document.getElementById('tra_ve').style.display='block';
            window.parent.frames[1].document.getElementById('tra_ve').innerHTML=httpObject.responseText;
            window.parent.frames[1].document.getElementById('subnavbg').style.display='none';
            window.parent.frames[1].document.getElementById('lefttra_ve').style.display='block';
            window.parent.frames[1].document.getElementById('lefttra_ve_1').style.display='block';
            window.parent.frames[1].document.getElementById('MenuContainer').style.display='none';
            if(window.parent.frames[1].document.getElementById('div_WaitingBets').style.display='none') {
                window.parent.frames[1].document.getElementById('div_WaitingBets').style.display='none';
                window.parent.frames[1].document.getElementById('div_BetListMini').style.display='none';
            }
            else {
                window.parent.frames[1].document.getElementById('div_WaitingBets').style.display='block';
                window.parent.frames[1].document.getElementById('div_BetListMini').style.display='block';
            }
            window.parent.frames[1].document.getElementById('div_menu').style.display='none';
            window.parent.frames[1].document.getElementById('subnav_head').style.display='none';
            window.parent.frames[1].document.getElementById('tien').focus();
            isClickVeXanh=false;
            solanclickvexanh=0;
        }
    }
    isClickVeDo=false;
    solanclickvedo=0;
    isClickVeXanh=false;
    solanclickvexanh=0;
    function xu_ly_them_cuoc_xanh(vari){
        var react_id = jQuery(vari).attr("data-react")
        var arrayReact = react_id.split("|");
        var loaibet = arrayReact[0];
        var tilecuoc = arrayReact[1];
        var idgiai = arrayReact[2];
        var idtran = arrayReact[3];
        var nguoidung = '<?php echo $_SESSION["ma_nguoi_dung"] ?>';
        if(isClickVeXanh==false){
            httpObject = getHTTPObject();
            if (httpObject != null) {
                window.parent.frames[1].aaa = 0;
                window.parent.frames[1].temxoa=0;
                httpObject.open("GET", "A_Bet_MYxanh.php?loaibet=" +loaibet+"&tilecuoc="+tilecuoc+"&giai="+idgiai+"&tran="+idtran+"&nguoidung="+nguoidung, true);
                httpObject.send(null);
                httpObject.onreadystatechange = setOutputcomment; //chu?n b? chuy?n hóa sang chu?i kích ho?t tr?ng thái output
            }
        }else{
            solanclickvexanh++;
            if(solanclickvexanh > 20){
                var mota="clickxanh";
                var ma_nguoi_dung='<?php echo $_SESSION['ma_nguoi_dung'] ?>';
                jQuery.ajax({
                    type: 'POST',
                    data:{ma_nguoi_dung:ma_nguoi_dung,mota:mota},
                    url: "khongdanguoidung.php",
                    error:function(err){
                        console.log(err);
                    },
                    success:function(result){
                        top.location ="../login888.aspx";
                    }
                });
            }
        }

    }

    function xu_ly_them_cuoc(loaibet,tilecuoc,idgiai,idtran,nguoidung){
        if(isClickVeDo==false){
            isClickVeDo=false;
            window.parent.frames[1].aaa = 0;
            jQuery.ajax({
                type:'post',
                url:'A_Bet_MY.php',
                data:{loaibet:loaibet,tilecuoc:tilecuoc,giai:idgiai,tran:idtran,nguoidung:nguoidung},
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                cache: false,
                asyncBoolean:false,
                success:function(result) {
                    window.parent.frames[1].document.getElementById('tra_ve').style.display='block';
                    window.parent.frames[1].document.getElementById('tra_ve').innerHTML=result;
                    window.parent.frames[1].document.getElementById('subnavbg').style.display='none';
                    window.parent.frames[1].document.getElementById('lefttra_ve').style.display='block';
                    window.parent.frames[1].document.getElementById('lefttra_ve_1').style.display='block';
                    window.parent.frames[1].document.getElementById('MenuContainer').style.display='none';
                    if(window.parent.frames[1].document.getElementById('div_WaitingBets').style.display='none') {
                        window.parent.frames[1].document.getElementById('div_WaitingBets').style.display='none';
                        window.parent.frames[1].document.getElementById('div_BetListMini').style.display='none';
                    }
                    else {
                        window.parent.frames[1].document.getElementById('div_WaitingBets').style.display='block';
                        window.parent.frames[1].document.getElementById('div_BetListMini').style.display='block';
                    }
                    window.parent.frames[1].document.getElementById('div_menu').style.display='none';
                    window.parent.frames[1].document.getElementById('subnav_head').style.display='none';
                    window.parent.frames[1].document.getElementById('tien').focus();
                    isClickVeDo=false;
                    solanclickvedo=0;
                }
            });
        }else{
            solanclickvedo++;
            if(solanclickvedo > 20){
                var mota="clickdo";
                var ma_nguoi_dung='<?php echo $_SESSION['ma_nguoi_dung'] ?>';
                jQuery.ajax({
                    type: 'POST',
                    data:{ma_nguoi_dung:ma_nguoi_dung,mota:mota},
                    url: "khongdanguoidung.php",
                    error:function(err){
                        console.log(err);
                    },
                    success:function(result){
                        top.location ="../login888.aspx";
                    }
                });
            }else{
                return;
            }
        }
    }
    function xu_ly_them_cuoc_ao(loaibet,tilecuoc,idgiai,idtran,nguoidung){
        httpObject = getHTTPObject();
        if (httpObject != null) {
            window.parent.frames[1].aaa = 0;window.parent.frames[1].temxoa = 0;
            httpObject.open("GET", "A_Bet_MY_Ao.php?loaibet=" +loaibet+"&tilecuoc="+tilecuoc+"&giai="+idgiai+"&tran="+idtran+"&nguoidung="+nguoidung, true);
            httpObject.send(null);
            httpObject.onreadystatechange = setOutputcomment; //chu?n b? chuy?n hóa sang chu?i kích ho?t tr?ng thái output
        }
    }
    var httpObject = null;
    var httpObject1 =null;
    var ObjectFav=null;
    function InitObjectFav() {
        if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
        else if (window.XMLHttpRequest) return new XMLHttpRequest();
        else { alert("Your browser does not support AJAX."); }
        return null;
    }
    function AddOddsToMyFavorite() {
    }
    function AddFav(chinhno,MaTran) {
        ObjectFav=InitObjectFav();
        if( chinhno.innerHTML.indexOf("Add")>-1) {
            chinhno.innerHTML="<span  class='iconOdds favorite'></span>";
            if(ObjectFav!=null) {
                //ObjectFav.open("GET","A_Xoa_Ua_Thich.php?ma_tran="+MaTran,true);
                //ObjectFav.send(null);
            }
        }
        else {
            if(ObjectFav!=null) {
                //ObjectFav.open("GET","A_Ua_Thich.php?ma_tran="+MaTran,true);
                //chinhno.innerHTML="<span  class='iconOdds favoriteAdd'></span>";
                //ObjectFav.send(null);
            }
        }
    }
    function Chuot_ra_di_tren(idtran) {
        //document.getElementById(idtran).style.backgroundColor='#f5eeb8';
        //document.getElementById(idtran+"_tam").style.backgroundColor='#f5eeb8';
    }
    function Chuot_dua_vao_tren(idtran) {
        /*if(document.getElementById(idtran).className=='live') {
            document.getElementById(idtran).style.backgroundColor='#FFCCBC';
            document.getElementById(idtran+"_tam").style.backgroundColor='#FFCCBC';
        }else {
            document.getElementById(idtran).style.backgroundColor='#ffddd2';
            document.getElementById(idtran+"_tam").style.backgroundColor='#ffddd2';
        }*/
    }
    function Chuot_ra_di_duoi(idtran){
        /*document.getElementById(idtran).style.backgroundColor='#f5eeb8';
        document.getElementById(idtran+"_tam").style.backgroundColor='#f5eeb8';*/
    }
    function Chuot_dua_vao_duoi(idtran) {
        /*if(document.getElementById(idtran).className=='bgcpe') {
            document.getElementById(idtran).style.backgroundColor='#c6d4f1';
            document.getElementById(idtran+"_tam").style.backgroundColor='#c6d4f1';
        }
        else {
            document.getElementById(idtran).style.backgroundColor='#e4e4e4';
            document.getElementById(idtran+"_tam").style.backgroundColor='#e4e4e4';
        }*/
    }
</script>