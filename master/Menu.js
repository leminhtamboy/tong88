﻿/*
* Created 20100203@Lee - Left menu page functions
* Revision ?@? - ... 
* 
*/

function ChangeMenu(url) {
    

    top.main.location =  url;
    	
}

function MenuToggle(index) {
    var subMenu = $('div' + index);
    subMenu.style.display = (subMenu.style.display == 'none') ? 'block' : 'none';
    var aMenu = $('a' + index);
    aMenu.className = (aMenu.className == 'Bleft_Parent') ? 'Bleft_ParentAc' : 'Bleft_Parent';
}

