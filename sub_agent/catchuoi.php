<?php

function utf8convert($str) {

    if(!$str) return false;

    $utf8 = array(

            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'd'=>'đ|Đ',

            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',

            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',

            );
    foreach($utf8 as $ascii=>$uni) $str = preg_replace("/($uni)/i",$ascii,$str);

return $str;

}

function utf8tourl($text){

    $text = strtolower(utf8convert($text));

    $text = str_replace( "ß", "ss", $text);

    $text = str_replace( "%", "", $text);

    $text = preg_replace("/[^_a-zA-Z0-9 -]/", "",$text);

    $text = str_replace(array('%20', ' '), '-', $text);

    $text = str_replace("----","-",$text);

    $text = str_replace("---","-",$text);

    $text = str_replace("--","-",$text);

return $text;

}

function chuyenChuoi($str) { 
// In thường 
     $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str); 
     $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str); 
     $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str); 
     $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str); 
     $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str); 
     $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str); 
     $str = preg_replace("/(đ)/", 'd', $str);     
     $str = html_entity_decode ($str); 
     $str = str_replace(array(' ','_'), '-', $str);  
        $str = html_entity_decode ($str); 
        $str = str_replace("ç","c",$str); 
        $str = str_replace("Ç","C",$str); 
        $str = str_replace(" / ","-",$str); 
        $str = str_replace("/","-",$str); 
        $str = str_replace(" - ","-",$str); 
        $str = str_replace("_","-",$str); 
        $str = str_replace(" ","-",$str); 
        $str = str_replace( "ß", "ss", $str); 
        $str = str_replace( "&", "", $str); 
        $str = str_replace( "%", "percent", $str); 
        $str = str_replace("----","-",$str); 
        $str = str_replace("---","-",$str); 
        $str = str_replace("--","-",$str); 
        //$str = str_replace(".","-",$str); 
        $str = str_replace(",","",$str); 
// In đậm 
     $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str); 
     $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str); 
     $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str); 
     $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str); 
     $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str); 
     $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str); 
     $str = preg_replace("/(Đ)/", 'D', $str); 
     return $str; // Trả về chuỗi đã chuyển 
     } 
  
function vn2latin($cs, $tolower = false)
{
/*Mảng chứa tất cả ký tự có dấu trong Tiếng Việt*/
$marTViet=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă",
"ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề",
"ế","ệ","ể","ễ",
"ì","í","ị","ỉ","ĩ",
"ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ",
"ờ","ớ","ợ","ở","ỡ",
"ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
"ỳ","ý","ỵ","ỷ","ỹ",
"đ",
"À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă",
"Ằ","Ắ","Ặ","Ẳ","Ẵ",
"È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
"Ì","Í","Ị","Ỉ","Ĩ",
"Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ",
"Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
"Ỳ","Ý","Ỵ","Ỷ","Ỹ",
"Đ"," ");
 
/*Mảng chứa tất cả ký tự không dấu tương ứng với mảng $marTViet bên trên*/
$marKoDau=array("a","a","a","a","a","a","a","a","a","a","a",
"a","a","a","a","a","a",
"e","e","e","e","e","e","e","e","e","e","e",
"i","i","i","i","i",
"o","o","o","o","o","o","o","o","o","o","o","o",
"o","o","o","o","o",
"u","u","u","u","u","u","u","u","u","u","u",
"y","y","y","y","y",
"d",
"A","A","A","A","A","A","A","A","A","A","A","A",
"A","A","A","A","A",
"E","E","E","E","E","E","E","E","E","E","E",
"I","I","I","I","I",
"O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O",
"U","U","U","U","U","U","U","U","U","U","U",
"Y","Y","Y","Y","Y",
"D","-");
 
if ($tolower) {
    return strtolower(str_replace($marTViet,$marKoDau,$cs));
}
 
return str_replace($marTViet,$marKoDau,$cs);
 
}


function cat($text)
 
     {
		$text =trim($text);
		$text = vn2latin($text, true);
		$text = chuyenChuoi($text);
		$text =utf8tourl(utf8convert($text));

		$text =str_replace( " & " , '-', $text );
		$text =str_replace( " / " , '-', $text );
		$text =str_replace( " -" , '', $text );
		$text =str_replace( " –" , '', $text );
		$text =str_replace( "--" , '-', $text );
		$text =str_replace( "-–-" , '-', $text );
		$text =str_replace( array("♥","★" ), '-', $text );
		$text =str_replace( array("♥","★" ), '-', $text );
		$text =str_replace( array("!","@","#",":",",","'","\",",".","(",")","[","]","{","}","_","png") , '', $text );
		$text =str_replace( "Screenshot" , '', $text );
		$text =str_replace(" ","-",$text);
		
		
		return $text;
 
     }

function catchuoi($chuoi){
//	thongbao($chuoi);
//	xuat4($chuoi);
	$chuoi=str_replace("'","",$chuoi);
	$chuoi=str_replace('"','',$chuoi);
	$chuoi=trim(strip_tags($chuoi));
	$chuoi=str_replace("\n","",$chuoi);
	//$chuoi=str_replace("\t","",$chuoi);
	//echo $so."<br>";
	//echo $chuoi."<br>";
	$chuoi_mang_a="e,é,è,ẻ,ẽ,ẹ,ê,ế,ề,ể,ễ,ệ";
	$chuoi_mang_a_1="e,e1,e2,e3,e4,e5,e6,e61,e62,e63,e64,e65";
	$mang_a=explode(",",$chuoi_mang_a);
	$mang_a_1=explode(",",$chuoi_mang_a_1);
	for($i=0;$i<count($mang_a);$i++)
	{
		$chuoi=str_replace($mang_a[$i],$mang_a_1[$i],$chuoi);
	}
	$chuoi_mang_b="ý,ỳ,ỷ,ỹ,ỵ,í,ì,ỉ,ĩ,ị";
	$chuoi_mang_b_1="y1,y2,y3,y4,y5,i1,i2,i3,i4,i5";

	$mang_b=explode(",",$chuoi_mang_b);
	$mang_b_1=explode(",",$chuoi_mang_b_1);
	//in_mang1($mang_b);
	//in_mang1($mang_b_1);
	//echo $chuoi."<hr>";
	for($i=0;$i<count($mang_b);$i++)
	{
		//echo $mang_b[$i]." va ".$chuoi_mang_b_1[$i]."<br>";
		$chuoi=str_replace($mang_b[$i],$mang_b_1[$i],$chuoi);
	}
	//echo $chuoi."<hr>";
	$chuoi_mang_a="u,ú,ù,ủ,ũ,ụ,ư,ứ,ừ,ử,ữ,ự";
	$chuoi_mang_a_1="u,u1,u2,u3,u4,u5,u7,u71,u72,u73,u74,u75";
	$mang_a=explode(",",$chuoi_mang_a);
	$mang_a_1=explode(",",$chuoi_mang_a_1);
	//echo $chuoi."<br>";
	//in_mang1($mang_a);
	for($i=0;$i<count($mang_a);$i++)
	{
		$chuoi=str_replace($mang_a[$i],$mang_a_1[$i],$chuoi);
	}
	$chuoi_mang_a="o,ó,ò,ỏ,õ,ọ,ô,ố,ồ,ổ,ỗ,ộ,ơ,ớ,ờ,ở,ỡ,ợ";
	$chuoi_mang_a_1="o,o1,o2,o3,o4,o5,o6,o61,o62,o63,o64,o65,o7,o71,o72,o73,o74,o75";
	$mang_a=explode(",",$chuoi_mang_a);
	$mang_a_1=explode(",",$chuoi_mang_a_1);
	for($i=0;$i<count($mang_a);$i++)
	{
		$chuoi=str_replace($mang_a[$i],$mang_a_1[$i],$chuoi);
	}
	$chuoi_mang_a="a,á,à,ả,ã,ạ,â,ấ,ầ,ẩ,ẫ,ậ,ă,ắ,ằ,ẳ,ẵ,ặ";
	$chuoi_mang_a_1="a,a1,a2,a3,a4,a5,a6,a61,a62,a63,a64,a65,a8,a81,a82,a83,a84,a85";
	$mang_a=explode(",",$chuoi_mang_a);
	$mang_a_1=explode(",",$chuoi_mang_a_1);
	for($i=0;$i<=count($mang_a);$i++)
	{
		$chuoi=str_replace($mang_a[$i],$mang_a_1[$i],$chuoi);
	}
	////echo $chuoi."<br>";
//	xuat4($chuoi);
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
	$chuoi_mang_a="E,É,È,Ẻ,Ẽ,Ẹ,Ê,Ế,Ề,Ể,Ễ,Ệ";
	$chuoi_mang_a_1="E,E1,E2,E3,E4,E5,E6,E61,E62,E63,E64,E65";
	$mang_a=explode(",",$chuoi_mang_a);
	$mang_a_1=explode(",",$chuoi_mang_a_1);
	for($i=0;$i<count($mang_a);$i++)
	{
		$chuoi=str_replace($mang_a[$i],$chuoi_mang_a_1[$i],$chuoi);
	}
	$chuoi_mang_a="Ý,Ỳ,Ỷ,Ỹ,Ỵ,Í,Ì,Ỉ,Ĩ,Ị";
	$chuoi_mang_a_1="Y1,Y2,Y3,Y4,Y5,I1,I2,I3,I4,I5";
	$mang_a=explode(",",$chuoi_mang_a);
	$mang_a_1=explode(",",$chuoi_mang_a_1);
	for($i=0;$i<count($mang_a);$i++)
	{
		$chuoi=str_replace($mang_a[$i],$mang_a_1[$i],$chuoi);
	}
	//echo $chuoi."<br>";
	$chuoi_mang_a="U,Ú,Ù,Ủ,Ũ,Ụ,Ư,Ứ,Ừ,Ử,Ữ,Ự";
	$chuoi_mang_a_1="U,U1,U2,U3,U4,U5,U7,U71,U72,U73,U74,U75";
	$mang_a=explode(",",$chuoi_mang_a);
	$mang_a_1=explode(",",$chuoi_mang_a_1);
	//in_mang1($mang_a);
	for($i=0;$i<count($mang_a);$i++)
	{
		$chuoi=str_replace($mang_a[$i],$mang_a_1[$i],$chuoi);
	}
	//////echo $chuoi."<br>";
	$chuoi_mang_a="O,Ó,Ò,Ỏ,Õ,Ọ,Ô,Ố,Ồ,Ổ,Ỗ,Ộ,Ơ,Ớ,Ờ,Ở,Ỡ,Ợ";
	$chuoi_mang_a_1="O,O1,O2,O3,O4,O5,O6,O61,O62,O63,O64,O65,O7,O71,O72,O73,O74,O75";
	$mang_a=explode(",",$chuoi_mang_a);
	$mang_a_1=explode(",",$chuoi_mang_a_1);
	for($i=0;$i<count($mang_a);$i++)
	{
		$chuoi=str_replace($mang_a[$i],$mang_a_1[$i],$chuoi);
	}
	$chuoi_mang_a="A,Á,À,Ả,Ã,Ạ,Â,Ấ,Ầ,Ẩ,Ẫ,Ậ,Ă,Ắ,Ằ,Ẳ,Ẵ,Ặ";
	$chuoi_mang_a_1="A,A1,A2,A3,A4,A5,A6,A61,A62,A63,A64,A65,A8,A81,A82,A83,A84,A85";
	$mang_a=explode(",",$chuoi_mang_a);
	$mang_a_1=explode(",",$chuoi_mang_a_1);
	for($i=0;$i<count($mang_a);$i++)
	{
		$chuoi=str_replace($mang_a[$i],$mang_a_1[$i],$chuoi);
	}
//	xuat4($chuoi);
	//echo $chuoi."666666666666666666666<br>";
	$chuoi=str_replace("đ","d9",$chuoi);
	$chuoi=str_replace("Đ","D9",$chuoi);
//	thongbao("$chuoi");
	//echo $chuoi."tttttttttttttttttteeeeeeeeeeeeeeeeeeeee<br>";
	//kiembien1($chuoi);
	//$chuoi=substr($chuoi,0,$so);
	//break;

	//echo $chuoi."eeeeeeeeeeeeeeeeeeeee<br>";
	//exit;
//	xuat4($chuoi);
	$mang_chuoi=explode(" ",$chuoi);
	if(count($mang_chuoi)==1)
	{}
	else
	{
		unset($mang_chuoi[count($mang_chuoi)-1]);
	}
	$chuoi=implode(" ",$mang_chuoi);
	//echo $chuoi."<hr>";
	$chuoi=chuyennguoc_753($chuoi);

	return $chuoi;
}


?>