<link href="/_Announcement/Resources/AnnouncementText.css?20140911" rel="stylesheet" type="text/css" />
<div id="popup_container_text" style="top: 110px; left: 350px; position: fixed; z-index: 99999; padding: 0pt; margin: 0pt; position:absolute;" class="ui-draggable">
  <div class="toptitlepopup_vn">
    <div class='closeAd'><a href="javascript:;" onclick="RemoveAnnouncement();" title="Khóa"><img src="/_Announcement/Resources/Images/iconClose.png" style="border: 0px" alt="Khóa" /></a></div>
  </div>
  <div class="paddingleft">
    <div class="linebg">
      <div>
        <div class="message"> Chào quý khách hàng,<br />
          <p> Bắt đầu từ ngày <b>28/08/2014</b>, chúng tôi sẽ thay đổi các thiết lập Position Taking và Hoa Hồng áp dụng cho 2 loại cược Double Chance và Both/One/Neither Team to Score. </p>
          <p> • Thiết lập Position Taking của Bóng Đá - Loại Cược Khác sẽ được áp dụng cho 2 loại cược này thay thế cho Bóng Đá - Handicap.<br />
            • Thiết lập Hoa Hồng - Loại Cược Khác sẽ được áp dụng cho 2 loại cược này thay thế cho nhóm Hoa Hồng - "Asian HDP, OU, OE". </p>
          <p> Trân trọng. </p>
        </div>
      </div>
      <div class='lineblue'></div>
      <div class='linewhite'></div>
    </div>
    <div class="bottomborder"></div>
  </div>
</div>
<input id="announcementCookiesId" value="30540242" type="hidden" />
<input id="popUpId" value="-20140827" type="hidden" />
