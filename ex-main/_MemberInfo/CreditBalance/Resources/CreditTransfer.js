function UpdCredit(role) {
    age.Lock();

    ajax.Request(
        '../../'+role+'/UpdateGivenCredit.php?custid=' + $('custid').value + '&custRoleId=' + $('roleId').value + '&amount=' + $('txtamout').value,
        {onSuccess:onComplete}
    );

    function onComplete(result) {
        console.log(result);
        age.UnLock();
        if (result.responseText.indexOf("loi_max") > -1) {
            ageMsg.Show("Số tiền nhâp vượt quá tín dụng nhỏ nhất và lớn nhất của Master . Vui lòng kiểm tra lại", false);
            window.top.closePopupAndReloadMainFrameAfterUpdateSuccess(3000);
        } else {
            $('txtamout').focus();
            ageMsg.Show("Cập nhật tín dụng cho Master thành côngs",true);
        }
        AddPopupHeight(50);
    }
}

function OnkeyUpAmt(e) {
    var isok = true;
    if (!e) e = window.event;
    var key = (e.keyCode) ? e.keyCode : e.which;

    if ((key < 48 || key > 57) && key != 8 && key != 13 && key != 0 && (key < 37 || key > 40)) {
        return false;
    }
    else {
        if (key == 13) {
            UpdCredit();
            return false;
        }
    }
}

function InitCreditTransfer() {
    $('txtamout').value = String.prototype.FormatNumber($('txtamout').value);
    age.addEvent($('txtamout'), 'keyup', function (event) {
        return age.FormatNumber(event, true);
    });
    age.addEvent($('txtamout'), 'blur', function (event) {
        return age.FormatNumber(event, true);
    });
}

RegisterStartUp(InitCreditTransfer);