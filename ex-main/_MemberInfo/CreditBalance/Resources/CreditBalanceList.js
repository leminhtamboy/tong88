function InitPage() {
    initAutoComplete('txtUserName', '../../_GlobalResources/Handlers/QueryUserName.ashx?custid=' + _page.CustId + '&isdir=1');
    $('txtUserName').blur();
}

RegisterStartUp(InitPage);

function viewOutSt(runningBetListLink) {
    window.location = runningBetListLink;
}

function viewBal(action, custId) {
    window.location = "CreditBalanceList.aspx?action=" + action + "&custid=" + custId + "&pageIndex=1&pageSize=" + _page.PageSize;
}

function searchByUsername(url, site) {
    var txtname = $('txtUserName').value;
    if (txtname == _page.UserNameDefault) {
        txtname = '';
    }

    url += '?' + 'custid=' + _page.CustId;
    url += '&username=' + encodeURIComponent(txtname.escapeHtml());
    url += '&status=' + $('statusFilter').value;
    url += "&action=" + site;
    url += '&pageIndex=1&pageSize=' + _page.PageSize;
    age.DelayReloadPage(url, 1);
}

function openTransfer(custId, amt, userName, roleId) {
    var popH = 300, popW = 400;
    var url = 'http://dev.tongbong88.com/sup_master_new/CreditTransfer.php?custid=' + custId + '&amt=' + amt + '&username=' + userName + '&roleId=' + roleId;
    //top.popupManager.openByRelativeUrl(url, '', popW, popH);
    window.open('CreditTransfer.php?custid=' + custId + '&amt=' + amt + '&username=' + userName + '&roleId=' + roleId, "abc", "top=300, left=300, width="+popW+", height="+popH+"");
}
function showTransYesBalance(custId) {
    var isyes = 1;
    var popH = 534, popW = 520;
    if (navigator.userAgent.match(/iPad/i) != null) {
        popH = 430;
    }

    var url = '_Transfer/Agent/MemberTransfer.aspx?custid=' + custId;
    top.popupManager.openByRelativeUrl(url, '', popW, popH);
}

/* Overwright Transfer.js */
function DelayReloadPage() {
    age.DelayReloadPage();
}

top.OnPopupComplete = function (reload) {
    window.top.closePopupAndReloadMainFrameAfterUpdateSuccess(3000);
    DelayReloadPage();
}

String.prototype.escapeHtml = function () { // Replace all ampersands with &amp; and all <’s and >’s with &lt; and &gt;, respectively:
    var result = '';
    for (var i = 0; i < this.length; i++) {
        if (this.charAt(i) == "&" && this.length - i - 1 >= 4 && this.substr(i, 4) != "&amp;") {
            result = result + "&amp;";
        } else if (this.charAt(i) == "<") {
            result = result + "&lt;";
        } else if (this.charAt(i) == ">") {
            result = result + "&gt;";
        } else {
            result = result + this.charAt(i);
        }
    }
    return result;
};