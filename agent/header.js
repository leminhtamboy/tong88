﻿
function Change2Menu(url) {
    

    top.main.location =  url;
    SelectTopMenu(url);
}



function ClearActiveTab() {
    if (currentTab) currentTab.className = "";
}

// For keeping top menu selected correctly
var currentTab = null;

function SelectTopMenu(url) {
    var mainUrl = '';
    if (url) {
        mainUrl = url;
    } else if (top.main) {
        mainUrl = top.main.location.href;
    }

    var start = mainUrl.lastIndexOf('/');
    var end = mainUrl.lastIndexOf('?');
    if (end == -1) end = mainUrl.length;
    var name = mainUrl.substring(start + 1, end);
	if (currentTab) currentTab.className = "";
	
    switch (name) {
        case 'trangchu.php':
            currentTab = $("balance");
            break;
        case 'chuyenkhoan.php':
            currentTab = $("transfer");
            break;
        case 'baomat.php':
		case 'ChangePassword.aspx':
            currentTab = $("changepass");
            break;
        case 'nhatky.php':
		case 'LogCustomerSetting.aspx':
            currentTab = $("viewlog");
            break;
        default:
            currentTab = null;
            break;
    }

    if (currentTab) currentTab.className = "active";
    else ClearActiveTab();
}




