<?php
@session_start();
include("../database.php");
$data = new database();
if(isset($_REQUEST['ag'])) {
    $agent = $_REQUEST['ag'];
    $mes = 0;
}
else {
    $agent = $_SESSION['ma'];
    $mes = 1;
}
$sql = "select * from member where Ma_agent='$agent' order by Tinh_trang asc, Ma asc";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Limit / Credit</title>
<link href="../dstv_files/Agent.min.css" rel="stylesheet" type="text/css">
<link href="../hmtd_files/Reports.min.css" rel="stylesheet" type="text/css">
<link href="../hmtd_files/PositionTakingList.min.css" rel="stylesheet" type="text/css">
<link href="../hmtd_files/Print.css" rel="stylesheet" type="text/css" media="print">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<script src="js/bootstrap.min.js"></script>
<script>
ht = 1;
mes = <?php echo $mes?>;
function showip(n) {
	var ip = "http://cqcounter.com/whois/?query="+ n;
    window.open(ip, "Thông tin ip", "toolbar=no, scrollbars=yes, resizable=yes, top=0, left=0, width=800, height=600");
}
function suatien(a,b) {
     if(mes == 0) return;
    var c = '<?php echo $agent?>';
    d = window.open("suatien.php?id="+a+"&t="+b+"&c="+c, "abc", "top=300, left=300, width=350, height=70");
}
</script>
</head>
<body onload="">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td><div id="page_main">
          <div id="header_main">Hạn mức /Tín dụng &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:getPrint('tbl-container');" id="imgPrint" title="In"></a></div>
          <div id="box_header" style="width: 100%">
            <table class="l" cellpadding="0" cellspacing="0" border="0">
              <tbody>
                <tr>
                  <td><link href="../hmtd_files/SearchUserName_Control.min.css" rel="stylesheet" type="text/css">
                    <link href="../hmtd_files/styles.min.css" rel="stylesheet" type="text/css">
                    <table id="tblSearch" cellpadding="0" cellspacing="0" border="0">
                      <tbody>
                        <tr>
                          <td>Tài khoản</td>
                          <td><input type="text" style="width: 145px; position: relative;" class="text_italic" name="txtUserName" id="txtUserName" value="" onkeypress="onKeyPressUser(&#39;dSubmit&#39;,event);" onclick="onclickUser(&#39;Tên đăng nhập hoặc Tên/Họ&#39;)" onblur="onblurUser(&#39;Tên đăng nhập hoặc Tên/Họ&#39;)" autocomplete="off"></td>
                          <td>Trạng thái</td>
                          <td><div id="box_option">
                              <select id="statusFilter" name="statusFilter" style="position: relative;">
                                <option value="0">Tất cả</option>
                                <option value="1" selected="">Mở</option>
                                <option value="2">Bị đình chỉ</option>
                                <option value="3">Bị khóa</option>
                                <option value="4">Vô hiệu hóa</option>
                              </select>
                            </div>
                            <div style="width:65px; float:right; text-align:right">
                              <input id="dSubmit" type="button" value="Xác nhận" class="buttonSubmit" onclick="">
                            </div>
                            <div class="shadow" id="shadow" style="position: absolute; top: 63px; left: 80px; visibility: hidden;">
                              <div class="output" id="output"><script src="../hmtd_files/SearchUserName_Control.min.js" type="text/javascript"></script><script src="../hmtd_files/autocomplete.js" type="text/javascript"></script></div>
                            </div></td>
                        </tr>
                      </tbody>
                    </table></td>
                </tr>
                <tr>
                  <td><span class="warning">
                    <ul>
                      <li>Bạn được phép chuyển khoản: Hằng ngày</li>
                    </ul>
                    </span></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div id="tbl-container">
            <div id="boderRight">
              <link href="../hmtd_files/PagingHeader.min.css" rel="stylesheet" type="text/css">
              <table id="tblHeader" cellspacing="0" cellpadding="0">
                <tbody>
                  <tr>
                    <td class="bgleft"></td>
                    <td class="bgcenter">&nbsp;
                      <link href="../hmtd_files/Paging.min.css" rel="stylesheet" type="text/css">
                      <script src="../hmtd_files/Paging.js" type="text/javascript"></script>
                      <div id="_PagingTop" class="pagingHiden" pagesize="500" currentindex="1" rowcount="500" pagecount="1">
                        <input disabled="" id="btnFirst_PagingTop" type="button" onclick="_PagingTop.First()" class="icon pagingFirst pagingDisable">
                        <input disabled="" id="btnPrev_PagingTop" type="button" onclick="_PagingTop.Move(-1)" class="icon pagingPrev pagingDisable">
                        <span class="pagingSeperator"></span>Trang
                        <input id="txt_PagingTop" type="text" class="pagingCurrent" maxlength="4" size="2" value="1" onkeydown="_PagingTop.DoEnter(event, &#39;_PagingTop.Go()&#39;)">
                        of 1<span class="pagingSeperator"></span>
                        <input disabled="" id="btnNext_PagingTop" type="button" onclick="_PagingTop.Move(1)" class="icon pagingNext pagingDisable">
                        <input disabled="" id="btnLast_PagingTop" type="button" onclick="_PagingTop.Last()" class="icon pagingLast pagingDisable">
                      </div>
                      <script type="text/javascript">var _PagingTop = new Paging('_PagingTop');</script></td>
                    <td class="bgright">Dung lượng trang
                      <select id="sel_PagingTop" name="sel_PagingTop" onchange="_PagingTop.SetPageSize(this.value)">
                        <option value="10">10</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="500" selected="">500</option>
                      </select></td>
                  </tr>
                </tbody>
              </table>
              <div id="ctxContainer">
                <style>
.ST { display: none; } table.showST .ST { display: table-cell; *display: block; } .FN { display: none; } table.showFN .FN { display: table-cell; *display: block; } .LN { display: none; } table.showLN .LN { display: table-cell; *display: block; } .CD { display: none; } table.showCD .CD { display: table-cell; *display: block; } .BL { display: none; } table.showBL .BL { display: table-cell; *display: block; } .YB { display: none; } table.showYB .YB { display: table-cell; *display: block; } .BC { display: none; } table.showBC .BC { display: table-cell; *display: block; } .OT { display: none; } table.showOT .OT { display: table-cell; *display: block; } .TO { display: none; } table.showTO .TO { display: table-cell; *display: block; } .LL { display: none; } table.showLL .LL { display: table-cell; *display: block; } .LI { display: none; } table.showLI .LI { display: table-cell; *display: block; } .UN { display: none; } table.showUN .UN { display: table-cell; *display: block; } .LCS { display: none; } table.showLCS .LCS { display: table-cell; *display: block; } 
</style>
                <link href="../hmtd_files/ContextMenuColumns_Control.css" rel="stylesheet" type="text/css">
                <div id="PopupCtx" class="divMenuPopup">
                  <table id="Popup" cellspacing="2" cellpadding="1" cols="ST,FN,LN,CD,BL,YB,BC,OT,TO,LL,LI,UN,LCS">
                    <tbody>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;ST&#39;, &#39;chk_ST&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_ST">
                          <label for="chk_ST"><span class="padding">Trạng thái</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;FN&#39;, &#39;chk_FN&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_FN">
                          <label for="chk_FN"><span class="padding">Tên</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;LN&#39;, &#39;chk_LN&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_LN">
                          <label for="chk_LN"><span class="padding">Họ</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;CD&#39;, &#39;chk_CD&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_CD">
                          <label for="chk_CD"><span class="padding">Hạn mức tín dụng</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;BL&#39;, &#39;chk_BL&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_BL">
                          <label for="chk_BL"><span class="padding">Số dư tài khoản</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;YB&#39;, &#39;chk_YB&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_YB">
                          <label for="chk_YB"><span class="padding">Hôm qua Số dư tài khoản</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;BC&#39;, &#39;chk_BC&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_BC">
                          <label for="chk_BC"><span class="padding">Hạn mức khả dụng</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;OT&#39;, &#39;chk_OT&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_OT">
                          <label for="chk_OT"><span class="padding">Số  tiền chưa xử lý</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;TO&#39;, &#39;chk_TO&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_TO">
                          <label for="chk_TO"><span class="padding">Tổng tiền cược của Member</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;LL&#39;, &#39;chk_LL&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_LL">
                          <label for="chk_LL"><span class="padding">Lần đăng nhập cuối</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;LI&#39;, &#39;chk_LI&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_LI">
                          <label for="chk_LI"><span class="padding">IP đăng nhập</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;UN&#39;, &#39;chk_UN&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_UN">
                          <label for="chk_UN"><span class="padding">Tài khoản</span></label></td>
                      </tr>
                      <tr>
                        <td><input class="padding" onclick="onChangeColumn(&#39;LCS&#39;, &#39;chk_LCS&#39;, &#39;&#39;, &#39;&#39;, true);" type="checkbox" id="chk_LCS">
                          <label for="chk_LCS"><span class="padding">LCS Win %</span></label></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div id="menuPopupAd" style="display: none;">
                  <div class="left"></div>
                  <div class="bg">
                    <div class="normal">
                      <div style="margin: 5px 5px;">Nhấp chuột phải trên đầu của mỗi cột để Ẩn/ Hiện cột đó</div>
                      <div>
                        <input id="chkAd" type="checkbox" onclick="hidePopupAd();">
                        <label for="chkAd">Không hiển thị lại</label>
                      </div>
                    </div>
                  </div>
                  <div class="right"></div>
                </div>
              </div>
              <table id="tblMain" class=" showST .ST  showFN .FN  showLN .LN  showCD .CD  showBL .BL  showYB .YB  showBC .BC  showOT .OT  showTO .TO  showLL .LL  showLI .LI  showUN .UN " border="1" cellpadding="2" cellspacing="0" style="width: 100%;">
                <thead>
                  <tr id="headerTop" class="RptHeader">
                    <td class="NO">#</td>
                    <td class="ST">Trạng thái</td>
                    <td>Tài khoản</td>
                    <td class="FN">Tên</td>
                    <td class="LN">Họ</td>
                    <td class="CD">Hạn mức tín dụng</td>
                    <td class="BL">Số dư tài khoản</td>
                    
                    <td class="LCS">LCS Win %</td>
                    <td class="TO">Chưa xử lý</td>
                    <td class="TO">Tổng tiền cược của Member</td>
                    <td class="LL" style="width: 110px;">Lần đăng nhập cuối</td>
                    <td class="LI">IP đăng nhập</td>
                   
                  </tr>
                </thead>
                <tbody>
                <?php
				
				/*if(mysql_fetch_row($sql)>0)
				{*/
					$dtmb = $data->ExcuteObjectList($sql);
					for ($i = 0; $i < count($dtmb); $i++)
					{
						$row = $dtmb[$i];
                        $reset = $row['Reset'];
                        $tien = $row['tiengo'];
						switch($row['Tinh_trang'] )
                        {
							case 0:
								$tt="Mở";
								break;
							case 1:
								$tt = "Bị khóa";
								break;
							case 2:
								$tt = "Bị đình chỉ";
								break;
							default:
								$tt="Vô hiệu hóa";
								break;		
						}
						//$i++;
						if($i%2!=0)
						{
							
				?>
                  <tr class="BgOdd">
                    <td class="w-order"><?php echo ($i+1)?></td>
                    <td class="ST"><?php echo $tt?></td>
                    <td class="l"><?php echo $row['Ma']?></td>
                    <td class="FN l"><?php echo $row['Ten']?></td>
                    <td class="LN l"><?php echo $row['Ho_mem']?></td>
                    <?php if($row["cam_sua"]!="1"){ ?>
                    <td class="CD r" ><a href="javascript:true;" onclick="suatien('<?php echo $row['Ma']?>','<?php echo $row['Tien_duoc_dua']?>')"><?php echo $row['Tien_duoc_dua']?></a></td>
                    <?php }else{ ?>
                    <td class="CD r" ><a href="javascript:true;"><?php echo $row['Tien_duoc_dua']?></a></td>
                    <?php } ?>
                    <td class="BL r"><?php echo $row['so_du']?></td>
                    
                    <td class="LCS r">Không giới hạn</td>
                    <td class="OT r"><a href="ddrtv.php?member=<?php echo $row['Ma']?>&ngay2=<?php echo date("m/d/Y")?>&ngay1=<?php
          $date= date('Y-m-d');
          $new_date = strtotime ( '-7 day' , strtotime ( $date ) ) ;
          $new_date = date ( 'm/d/Y' , $new_date );      
          echo $new_date;
          $ma_nguoi_dung = $row['Ma'];
          $new_date = strtotime ( '-7 day' , strtotime ( $date ) ) ;
          $new_date = date ( 'Y-m-d' , $new_date );
          $a = mysql_fetch_array(mysql_query("Select sum(so_tien) from betlist where ma_nguoi_dung='$ma_nguoi_dung' and tinh_trang='0' and ngay_bet >='$new$new_date' and ngay_bet<='$date'"));
          $b = mysql_fetch_array(mysql_query("Select sum(so_tien) from betlistmy where ma_nguoi_dung='$ma_nguoi_dung' and tinh_trang='0' and ngay_bet >='$new$new_date' and ngay_bet<='$date'"));    
        ?>"><?php echo ($a[0] + $b[0])?></a></td>
                    <td class="TO r"><?php 
                    $a = mysql_fetch_array(mysql_query("Select sum(so_tien) from betlist where ma_nguoi_dung='$ma_nguoi_dung'"));
          $b = mysql_fetch_array(mysql_query("Select sum(so_tien) from betlistmy where ma_nguoi_dung='$ma_nguoi_dung' "));  echo ($a[0] + $b[0]);
                    ?></td>
                    <td class="LL bl_time"><?php echo $row['ngaydn']?></td>
                    <td class="LI bl_time"><a href="javascript:true;" onclick="" class="iplink"><?php echo $row['ipdn']?></a></td>
                   
                  </tr>
                  <?php 
						}
						else
						{
							
				  ?>
                  <tr class="BgEven">
                    <td class="w-order"><?php echo ($i+1)?></td>
                    <td class="ST"><?php echo $tt?></td>
                    <td class="l"><?php echo $row['Ma']?></td>
                    <td class="FN l"><?php echo $row['Ten']?></td>
                    <td class="LN l"><?php echo $row['Ho_mem']?></td>
                    <?php if($row["cam_sua"]!="1"){ ?>
                    <td class="CD r" ><a href="javascript:true;" onclick="suatien('<?php echo $row['Ma']?>','<?php echo $row['Tien_duoc_dua']?>')"><?php echo $row['Tien_duoc_dua']?></a></td>
                    <?php }else{ ?>
                    <td class="CD r" ><a href="javascript:true;"><?php echo $row['Tien_duoc_dua']?></a></td>
                    <?php } ?>
                    <td class="BL r"><?php echo $row['so_du']?></td>
                    
                    <td class="LCS r">Nolimit</td>
                    <td class="OT r"><a href="ddrtv.php?member=<?php echo $row['Ma']?>&ngay2=<?php echo date("m/d/Y")?>&ngay1=<?php
          $date= date('Y-m-d');
          $new_date = strtotime ( '-7 day' , strtotime ( $date ) ) ;
          $new_date = date ( 'm/d/Y' , $new_date );      
          echo $new_date;
          $ma_nguoi_dung = $row['Ma'];
          $new_date = strtotime ( '-7 day' , strtotime ( $date ) ) ;
          $new_date = date ( 'Y-m-d' , $new_date );
          $a = mysql_fetch_array(mysql_query("Select sum(so_tien) from betlist where ma_nguoi_dung='$ma_nguoi_dung' and tinh_trang='0' and ngay_bet >='$new$new_date' and ngay_bet<='$date'"));
          $b = mysql_fetch_array(mysql_query("Select sum(so_tien) from betlistmy where ma_nguoi_dung='$ma_nguoi_dung' and tinh_trang='0' and ngay_bet >='$new$new_date' and ngay_bet<='$date'"));    
        ?>"><?php echo ($a[0] + $b[0])?></a></td>
                    <td class="TO r">0</td>
                    <td class="LL bl_time"><?php echo $row['ngaydn']?></td>
                    <td class="LI bl_time"><a href="javascript:true;" onclick="" class="iplink"><?php echo $row['ipdn']?></a>&nbsp;</td>
                    
                  </tr>
                 <?php }} ?>
                </tbody>
              </table>
            </div>
          </div>
        </div></td>
    </tr>
  </tbody>
</table>
<script type="text/javascript" src="../hmtd_files/Core.js"></script><script type="text/javascript" src="../hmtd_files/AGEWnd.js"></script><script type="text/javascript" src="../hmtd_files/ContextMenuColumns_Control.js"></script><script type="text/javascript" src="../hmtd_files/CreditBalanceList.js"></script><script type="text/javascript">var _page = {'tableId':'tblMain','cookiePrefix':'463346590','containerId':['headerTop'],'parentId':'ctxContainer','sessionId':'kfwxj5onqi4wk1sosvuz1z3r','wrnTransferSuccessful':'Chuyển khoản thành công','UserNameDefault':'Tên đăng nhập hoặc Tên/Họ','CustId':15366409,'PageSize':500};</script>
<div style="display: none; position: absolute; top: 0px; left: 0px; opacity: 0.5; background-color: white;">
  <div style="width: 100px; height: 100px; position: relative;"></div>
</div>

</div>
</body>
</html>