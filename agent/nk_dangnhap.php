﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0154)https://mb.b88ag.com/(S(ya3r3czzoaxreqqbfrdffnjg))/_ViewLog/LogLogin.aspx?tabid=Login&pageIndex=1&custname=&fromdate=9/1/2014&todate=9/28/2014&actiontype= -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="ctl00_Head1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ViewLogLogin</title>
<link href="../nk_dangnhap_files/Agent.css" rel="stylesheet" type="text/css">
<link href="../nk_dangnhap_files/Tab.css" rel="stylesheet" type="text/css">
<link href="../nk_dangnhap_files/Table.css" rel="stylesheet" type="text/css">
<link href="../nk_dangnhap_files/Paging.css" rel="stylesheet" type="text/css">
<link href="../nk_dangnhap_files/ViewLog.css" rel="stylesheet" type="text/css">
<link href="../nk_dangnhap_files/jscal2.css" rel="stylesheet" type="text/css">
<link href="../nk_dangnhap_files/steel.css" rel="stylesheet" type="text/css">
<link href="../nk_dangnhap_files/styles.css" rel="stylesheet" type="text/css">
<style type="text/css"></style>

</head>
<body>
<form name="aspnetForm" method="post" action="" id="aspnetForm">
  <div>
    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMzIxMDYyODE4ZGTGKbiYrV1s6wB3tr3/17dSGCn+fg==">
  </div>
  <div id="page_main">
    <div>
      <div id="button_hr">
        <div id="tab_hr">
          <ul>
            <li id="CustomerSetting"><a href="javascript:MenuGoTo('CustomerSetting')"><span>Thiết lập</span></a></li>
            <li id="Status"><a href="javascript:MenuGoTo('Status')"><span>Trạng thái</span> </a></li>
            <li id="Credit"><a href="javascript:MenuGoTo('Credit')"><span>Tín dụng</span></a></li>
            <li id="Login" class="current"><a href="javascript:MenuGoTo('Login')"><span>Đăng nhập</span></a></li>
          </ul>
        </div>
      </div>
      <div id="box_header" style="border-top: 0px; padding: 5px 5px 5px 5px;">
        <div class="divUserName">
          <div id="ctl00_divUserName" class="divSearch" style="display:none;">Tài khoản
            <input maxlength="50" id="txtUserName" name="txtUserName" class="text_f1" type="text" style="width: 150px; position: relative;" title="Tên đăng nhập hoặc Tên/Họ" onkeydown="onclickUser(&quot;Tên đăng nhập hoặc Tên/Họ&quot;)" onfocus="onclickUser(&quot;Tên đăng nhập hoặc Tên/Họ&quot;)" onblur="onblurUser(&quot;Tên đăng nhập hoặc Tên/Họ&quot;)" onkeypress="KeyPress(event)" autocomplete="off">
            <span id="RequiredUserName" class="required" style="display: none;">*</span></div>
          <div class="divLogType" id="ddlLogActionType">Xem
            <select name="ctl00$ddlActionType" id="ctl00_ddlActionType" class="ActionType" onchange="DoSearch();" style="position: relative;">
            </select>
          </div>
          <link href="../nk_dangnhap_files/DateRangeSelect.css" rel="stylesheet" type="text/css">
          <script src="../nk_dangnhap_files/DateRangeSelect.js" type="text/javascript"></script><script type="text/javascript">var max_server_date = '9/28/2014';</script>
          <div id="spDateTimeSearch">
            <table cellpadding="0" cellspacing="0" border="0">
              <tbody>
                <tr style="height: 32px;">
                  <td>Xem dữ liệu trong tháng&nbsp;&nbsp;</td>
                  <td id="td1" class="l"><input type="hidden" name="fdate" id="fdate" value="09/01/2014">
                    <input type="hidden" name="tdate" id="tdate" value="09/28/2014">
                    <span id="spfdatetext">
                    <div class="monthPicker"><a href="javascript:PrevMonth();" id="btnPrevMonth" class="prev" title="Previous Month"></a><span id="lblSelectedMonth" onclick="ShowMonth();" title="9/1/2014">Tháng 9 - 2014</span><a href="javascript:NextMonth();" id="btnNextMonth" class="next disabled" title="Next Month"></a></div>
                    <div id="monthPickerDropDown">
                      <ul class="monthPickerDropDownInner">
                        <li class="year"><a id="ID_2014" class="selected" href="javascript:SelectedYear('2014');" year="2014"><span class="bullet">2014</span></a></li>
                        <li class="month" id="LI_2014_9"><a id="9/1/2014" class="selected" href="javascript:SetSelectedMonth('9/1/2014', true);" title="Tháng 9">Tháng 9</a></li>
                        <li class="month" id="LI_2014_8"><a id="8/1/2014" href="javascript:SetSelectedMonth('8/1/2014', true);" title="Tháng 8">Tháng 8</a></li>
                        <li class="month" id="LI_2014_7"><a id="7/1/2014" href="javascript:SetSelectedMonth('7/1/2014', true);" title="Tháng 7">Tháng 7</a></li>
                        <li class="month" id="LI_2014_6"><a id="6/1/2014" href="javascript:SetSelectedMonth('6/1/2014', true);" title="Tháng 6">Tháng 6</a></li>
                        <li class="month" id="LI_2014_5"><a id="5/1/2014" href="javascript:SetSelectedMonth('5/1/2014', true);" title="Tháng 5">Tháng 5</a></li>
                        <li class="month" id="LI_2014_4"><a id="4/1/2014" href="javascript:SetSelectedMonth('4/1/2014', true);" title="Tháng 4">Tháng 4</a></li>
                        <li class="month" id="LI_2014_3"><a id="3/1/2014" href="javascript:SetSelectedMonth('3/1/2014', true);" title="Tháng 3">Tháng 3</a></li>
                        <li class="month" id="LI_2014_2"><a id="2/1/2014" href="javascript:SetSelectedMonth('2/1/2014', true);" title="Tháng 2">Tháng 2</a></li>
                        <li class="month" id="LI_2014_1"><a id="1/1/2014" href="javascript:SetSelectedMonth('1/1/2014', true);" title="Tháng 1">Tháng 1</a></li>
                        <li class="year"><a id="ID_2013" href="javascript:SelectedYear('2013');" year="2013"><span class="bullet">2013</span></a></li>
                        <li class="month" id="LI_2013_12" style="display: none"><a id="12/1/2013" href="javascript:SetSelectedMonth('12/1/2013', true);" title="Tháng 12">Tháng 12</a></li>
                        <li class="month" id="LI_2013_11" style="display: none"><a id="11/1/2013" href="javascript:SetSelectedMonth('11/1/2013', true);" title="Tháng 11">Tháng 11</a></li>
                        <li class="month" id="LI_2013_10" style="display: none"><a id="10/1/2013" href="javascript:SetSelectedMonth('10/1/2013', true);" title="Tháng 10">Tháng 10</a></li>
                        <li class="month" id="LI_2013_9" style="display: none"><a id="9/1/2013" href="javascript:SetSelectedMonth('9/1/2013', true);" title="Tháng 9">Tháng 9</a></li>
                        <li class="month" id="LI_2013_8" style="display: none"><a id="8/1/2013" href="javascript:SetSelectedMonth('8/1/2013', true);" title="Tháng 8">Tháng 8</a></li>
                        <li class="month" id="LI_2013_7" style="display: none"><a id="7/1/2013" href="javascript:SetSelectedMonth('7/1/2013', true);" title="Tháng 7">Tháng 7</a></li>
                        <li class="month" id="LI_2013_6" style="display: none"><a id="6/1/2013" href="javascript:SetSelectedMonth('6/1/2013', true);" title="Tháng 6">Tháng 6</a></li>
                        <li class="month" id="LI_2013_5" style="display: none"><a id="5/1/2013" href="javascript:SetSelectedMonth('5/1/2013', true);" title="Tháng 5">Tháng 5</a></li>
                        <li class="month" id="LI_2013_4" style="display: none"><a id="4/1/2013" href="javascript:SetSelectedMonth('4/1/2013', true);" title="Tháng 4">Tháng 4</a></li>
                        <li class="month" id="LI_2013_3" style="display: none"><a id="3/1/2013" href="javascript:SetSelectedMonth('3/1/2013', true);" title="Tháng 3">Tháng 3</a></li>
                        <li class="month" id="LI_2013_2" style="display: none"><a id="2/1/2013" href="javascript:SetSelectedMonth('2/1/2013', true);" title="Tháng 2">Tháng 2</a></li>
                        <li class="month" id="LI_2013_1" style="display: none"><a id="1/1/2013" href="javascript:SetSelectedMonth('1/1/2013', true);" title="Tháng 1">Tháng 1</a></li>
                      </ul>
                      <input type="hidden" name="listYear" id="listYear" value="2014-2013">
                      <input id="maxDate" name="maxDate" type="hidden" value="9/1/2014">
                      <input id="minDate" name="minDate" type="hidden" value="1/1/2013">
                    </div>
                    </span></td>
                  <td>&nbsp;&nbsp;Từ&nbsp;</td>
                  <td><span id="spfdate">
                    <select name="ddlDateFrom" id="ddlDateFrom" onchange="ChangeDateFrom();">
                      <option value="1" selected="">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                    </select>
                    </span></td>
                  <td>&nbsp;đến&nbsp;</td>
                  <td><span id="sptdate">
                    <select name="ddlDateTo" id="ddlDateTo" onchange="ChangeDateTo();">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28" selected="">28</option>
                    </select>
                    </span></td>
                  <td class="l">&nbsp;&nbsp;
                    <input type="button" class="btn" style="width: 55px" id="dSubmit" value="Xác nhận" onclick="DisableButton(); SearchByDate();"></td>
                  <td valign="top"><div id="loading" class="" style="float: left;"></div></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div id="divShowOldValue">
            <input type="checkbox" value="true" id="chk_showOldValue" onchange="setShowOldValueCookie(this)">
            <label for="chk_showOldValue">Hiển thị giá trị cũ</label>
          </div>
        </div>
      </div>
      <div class="warning" id="divMessageWarning" style="display: none">
        <ul>
          <li>Nhật ký đăng nhập của  thành viên hiện chưa có.</li>
        </ul>
      </div>
      <div id="content">
        <table width="100%" cellpadding="0" cellspacing="0" id="DataLog">
          <tbody>
            <tr class="title">
              <th>ID</th>
              <th>Thởi điểm</th>
              <th>Tài khoản</th>
              <th>Hành động</th>
              <th>Chi tiết</th>
              <th>Domain</th>
              <th>Browser</th>
              <th>Quốc gia</th>
              <th>IP</th>
            </tr>
            <tr class="even">
              <td>808212674</td>
              <td class="datetimeInlinde">9/28/2014 <br>
                10:49:14 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.169.128');">118.68.169.128</a></td>
            </tr>
            <tr class="odd">
              <td>807746901</td>
              <td class="datetimeInlinde">9/28/2014 <br>
                09:56:04 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('113.172.214.175');">113.172.214.175</a></td>
            </tr>
            <tr class="even">
              <td>805974337</td>
              <td class="datetimeInlinde">9/26/2014 <br>
                05:18:48 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 37.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.168.146');">118.68.168.146</a></td>
            </tr>
            <tr class="odd">
              <td>805269692</td>
              <td class="datetimeInlinde">9/25/2014 <br>
                03:55:37 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.175.183');">118.68.175.183</a></td>
            </tr>
            <tr class="even">
              <td>805239138</td>
              <td class="datetimeInlinde">9/25/2014 <br>
                02:49:52 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.175.183');">118.68.175.183</a></td>
            </tr>
            <tr class="odd">
              <td>805102504</td>
              <td class="datetimeInlinde">9/24/2014 <br>
                10:33:23 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.175.183');">118.68.175.183</a></td>
            </tr>
            <tr class="even">
              <td>805102177</td>
              <td class="datetimeInlinde">9/24/2014 <br>
                10:32:47 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.175.183');">118.68.175.183</a></td>
            </tr>
            <tr class="odd">
              <td>805096179</td>
              <td class="datetimeInlinde">9/24/2014 <br>
                10:21:11 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.175.183');">118.68.175.183</a></td>
            </tr>
            <tr class="even">
              <td>804562803</td>
              <td class="datetimeInlinde">9/24/2014 <br>
                04:32:11 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 37.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.175.20');">118.68.175.20</a></td>
            </tr>
            <tr class="odd">
              <td>803732860</td>
              <td class="datetimeInlinde">9/23/2014 <br>
                12:28:26 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.175.20');">118.68.175.20</a></td>
            </tr>
            <tr class="even">
              <td>803331456</td>
              <td class="datetimeInlinde">9/22/2014 <br>
                08:58:29 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.172.129');">42.118.172.129</a></td>
            </tr>
            <tr class="odd">
              <td>803281551</td>
              <td class="datetimeInlinde">9/22/2014 <br>
                07:29:37 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.172.129');">42.118.172.129</a></td>
            </tr>
            <tr class="even">
              <td>803141817</td>
              <td class="datetimeInlinde">9/22/2014 <br>
                03:46:59 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 37.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.172.129');">42.118.172.129</a></td>
            </tr>
            <tr class="odd">
              <td>802942908</td>
              <td class="datetimeInlinde">9/22/2014 <br>
                12:28:11 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 37.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.172.129');">42.118.172.129</a></td>
            </tr>
            <tr class="even">
              <td>802896548</td>
              <td class="datetimeInlinde">9/21/2014 <br>
                11:43:10 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 37.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.172.129');">42.118.172.129</a></td>
            </tr>
            <tr class="odd">
              <td>800453889</td>
              <td class="datetimeInlinde">9/18/2014 <br>
                10:45:42 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 37.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.175.81');">42.118.175.81</a></td>
            </tr>
            <tr class="even">
              <td>799895067</td>
              <td class="datetimeInlinde">9/18/2014 <br>
                03:18:31 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.170.196');">118.68.170.196</a></td>
            </tr>
            <tr class="odd">
              <td>799824665</td>
              <td class="datetimeInlinde">9/18/2014 <br>
                01:08:11 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.173.102');">42.118.173.102</a></td>
            </tr>
            <tr class="even">
              <td>799822853</td>
              <td class="datetimeInlinde">9/18/2014 <br>
                01:04:56 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 37.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.173.102');">42.118.173.102</a></td>
            </tr>
            <tr class="odd">
              <td>799749220</td>
              <td class="datetimeInlinde">9/17/2014 <br>
                10:54:45 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.173.102');">42.118.173.102</a></td>
            </tr>
            <tr class="even">
              <td>799191835</td>
              <td class="datetimeInlinde">9/17/2014 <br>
                04:27:31 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.174.201');">42.118.174.201</a></td>
            </tr>
            <tr class="odd">
              <td>799033903</td>
              <td class="datetimeInlinde">9/16/2014 <br>
                10:41:40 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.169.33');">118.68.169.33</a></td>
            </tr>
            <tr class="even">
              <td>798998158</td>
              <td class="datetimeInlinde">9/16/2014 <br>
                09:31:52 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.169.33');">118.68.169.33</a></td>
            </tr>
            <tr class="odd">
              <td>798826921</td>
              <td class="datetimeInlinde">9/16/2014 <br>
                02:57:01 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH Result:Failed (Password problem)</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 38.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.171.206');">118.68.171.206</a></td>
            </tr>
            <tr class="even">
              <td>798826740</td>
              <td class="datetimeInlinde">9/16/2014 <br>
                02:56:41 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH Result:Failed (Password problem)</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 38.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.171.206');">118.68.171.206</a></td>
            </tr>
            <tr class="odd">
              <td>798826603</td>
              <td class="datetimeInlinde">9/16/2014 <br>
                02:56:21 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH Result:Failed (Password problem)</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 38.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.171.206');">118.68.171.206</a></td>
            </tr>
            <tr class="even">
              <td>796983129</td>
              <td class="datetimeInlinde">9/14/2014 <br>
                08:13:01 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('27.66.170.189');">27.66.170.189</a></td>
            </tr>
            <tr class="odd">
              <td>796929629</td>
              <td class="datetimeInlinde">9/14/2014 <br>
                06:55:41 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('171.240.29.48');">171.240.29.48</a></td>
            </tr>
            <tr class="even">
              <td>796911435</td>
              <td class="datetimeInlinde">9/14/2014 <br>
                06:26:36 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('171.240.29.48');">171.240.29.48</a></td>
            </tr>
            <tr class="odd">
              <td>796901394</td>
              <td class="datetimeInlinde">9/14/2014 <br>
                06:10:20 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('171.240.29.48');">171.240.29.48</a></td>
            </tr>
            <tr class="even">
              <td>796886390</td>
              <td class="datetimeInlinde">9/14/2014 <br>
                05:45:02 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('171.240.29.48');">171.240.29.48</a></td>
            </tr>
            <tr class="odd">
              <td>796737128</td>
              <td class="datetimeInlinde">9/14/2014 <br>
                12:47:40 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('113.172.177.169');">113.172.177.169</a></td>
            </tr>
            <tr class="even">
              <td>796722107</td>
              <td class="datetimeInlinde">9/14/2014 <br>
                12:21:04 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('113.172.177.169');">113.172.177.169</a></td>
            </tr>
            <tr class="odd">
              <td>796702045</td>
              <td class="datetimeInlinde">9/13/2014 <br>
                11:46:27 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('113.172.177.169');">113.172.177.169</a></td>
            </tr>
            <tr class="even">
              <td>796055879</td>
              <td class="datetimeInlinde">9/13/2014 <br>
                06:05:48 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.172.121');">42.118.172.121</a></td>
            </tr>
            <tr class="odd">
              <td>795950260</td>
              <td class="datetimeInlinde">9/13/2014 <br>
                02:41:43 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.172.121');">42.118.172.121</a></td>
            </tr>
            <tr class="even">
              <td>795949546</td>
              <td class="datetimeInlinde">9/13/2014 <br>
                02:40:09 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.172.121');">42.118.172.121</a></td>
            </tr>
            <tr class="odd">
              <td>795467691</td>
              <td class="datetimeInlinde">9/12/2014 <br>
                08:31:05 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Firefox 31.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('123.21.124.171');">123.21.124.171</a></td>
            </tr>
            <tr class="even">
              <td>795329080</td>
              <td class="datetimeInlinde">9/12/2014 <br>
                03:08:20 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.171.64');">118.68.171.64</a></td>
            </tr>
            <tr class="odd">
              <td>794704767</td>
              <td class="datetimeInlinde">9/11/2014 <br>
                02:31:05 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('42.118.178.188');">42.118.178.188</a></td>
            </tr>
            <tr class="even">
              <td>794132365</td>
              <td class="datetimeInlinde">9/10/2014 <br>
                04:59:07 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.169.216');">118.68.169.216</a></td>
            </tr>
            <tr class="odd">
              <td>794109326</td>
              <td class="datetimeInlinde">9/10/2014 <br>
                03:48:43 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.169.216');">118.68.169.216</a></td>
            </tr>
            <tr class="even">
              <td>794072116</td>
              <td class="datetimeInlinde">9/10/2014 <br>
                02:12:51 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.169.216');">118.68.169.216</a></td>
            </tr>
            <tr class="odd">
              <td>793952838</td>
              <td class="datetimeInlinde">9/9/2014 <br>
                10:06:21 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.169.216');">118.68.169.216</a></td>
            </tr>
            <tr class="even">
              <td>793943749</td>
              <td class="datetimeInlinde">9/9/2014 <br>
                09:48:18 PM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.169.216');">118.68.169.216</a></td>
            </tr>
            <tr class="odd">
              <td>793537671</td>
              <td class="datetimeInlinde">9/9/2014 <br>
                06:49:16 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.169.216');">118.68.169.216</a></td>
            </tr>
            <tr class="even">
              <td>793389283</td>
              <td class="datetimeInlinde">9/9/2014 <br>
                12:52:00 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.169.216');">118.68.169.216</a></td>
            </tr>
            <tr class="odd">
              <td>792925707</td>
              <td class="datetimeInlinde">9/8/2014 <br>
                07:26:02 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.174.54');">118.68.174.54</a></td>
            </tr>
            <tr class="even">
              <td>792883706</td>
              <td class="datetimeInlinde">9/8/2014 <br>
                06:09:45 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Firefox 32.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.174.54');">118.68.174.54</a></td>
            </tr>
            <tr class="odd">
              <td>792835128</td>
              <td class="datetimeInlinde">9/8/2014 <br>
                04:46:43 AM</td>
              <td class="EditedBy">KP39MATH</td>
              <td class="ActionName">Đăng nhập thông qua trang web  Agent</td>
              <td class="actionDesc">Agent:KP39MATH -  [Before Login] Result:Success</td>
              <td>www.b88ag.com</td>
              <td class="Browser">Chrome 36.0</td>
              <td>Unknown</td>
              <td class="Ip"><a class="iplink" href="javascript:OpenIPInfo('118.68.174.54');">118.68.174.54</a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <link href="../nk_dangnhap_files/Paging(1).css" rel="stylesheet" type="text/css">
      <script src="../nk_dangnhap_files/Paging.js" type="text/javascript"></script>
      <div id="paging" class="pagingContainer" pagesize="50" currentindex="1" rowcount="70" pagecount="2">
        <input disabled="" id="btnFirstpaging" type="button" onclick="paging.First()" class="icon pagingFirst pagingDisable">
        <input disabled="" id="btnPrevpaging" type="button" onclick="paging.Move(-1)" class="icon pagingPrev pagingDisable">
        <span class="pagingSeperator"></span>Trang
        <input id="txtpaging" type="text" class="pagingCurrent" maxlength="4" size="2" value="1" onkeydown="paging.DoEnter(event, &#39;paging.Go()&#39;)">
        of 2<span class="pagingSeperator"></span>
        <input {disablednext}="" id="btnNextpaging" type="button" onclick="paging.Move(1)" class="icon pagingNext">
        <input {disabledlast}="" id="btnLastpaging" type="button" onclick="paging.Last()" class="icon pagingLast">
        <select id="selpaging" name="selpaging" onchange="paging.SetPageSize(this.value)">
          <option value="10">10</option>
          <option value="50" selected="">50</option>
          <option value="100">100</option>
          <option value="500">500</option>
        </select>
      </div>
      <script type="text/javascript">var paging = new Paging('paging');</script></div>
  </div>
</form>
<script src="../nk_dangnhap_files/Core.js" type="text/javascript"></script><script src="../nk_dangnhap_files/AGEWnd.js" type="text/javascript"></script><script src="../nk_dangnhap_files/autocomplete.js" type="text/javascript"></script><script src="../nk_dangnhap_files/ViewLogLibs.js" type="text/javascript"></script><script type="text/javascript" language="javascript">var ctrInput = "9/28/2014 10:49:39 PM";
var ddlActionType = 'ctl00_ddlActionType';
var role = '2';
var enterusername = 'Tên đăng nhập hoặc Tên/Họ';
RegisterStartUp('Initialize()');</script>
<div class="shadow" id="shadow" style="position: absolute; top: 3px; left: 2px; visibility: hidden;">
  <div class="output" id="output"></div>
</div>
<div style="display: none; position: absolute; top: 0px; left: 0px; opacity: 0.5; background-color: white;">
  <div style="width: 100px; height: 100px; position: relative;"></div>
</div>
</body>
</html>