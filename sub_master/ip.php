<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title></title>
<link href="/_IPInfo/Resources/IpInfo.css?20140916" rel="stylesheet" type="text/css" />
<style>
body
{
	margin: 0;
}

table
{
	margin: 0;
	padding:2px 1px 2px 2px;
}

table td {
    border-bottom:1px solid #E0E0E0;
    border-right:1px solid #E0E0E0;
    font-family: Tahoma,Arial ,helvetica ,sans-serif;
    font-size:11px;
}

.tdvalue
{
	font-weight:bold;
}

.note
{
	padding:2px 1px 1px 2px;
	font-size:11px;
	color:#333;
}
</style>
</head>
<body>
<table width="100%" class="" cellpadding="1" cellspacing="0">
  <tr>
    <td>IP</td>
    <td class="tdvalue">113.190.252.37</td>
  </tr>
  <tr>
    <td>Quốc gia</td>
    <td class="tdvalue">VIET NAM <img src='../Flags/VN.gif' /></td>
  </tr>
  <tr>
    <td>Thành phố</td>
    <td class="tdvalue">HA NOI</td>
  </tr>
</table>
<div class="note">Thông tin hỗ trợ tại DNSstuff:<a href="Whois.ashx?ip=113.190.252.37" target="_blank"> click here</a>.
  Lưu ý:Thông tin về Quốc gia, Thành Phố được cung cấp bởi  IP2Location™ và DNSstuff chỉ được dùng với mục đích tham khào .Vui lòng kiềm tra thêm từ những nơi khác để có được nguồn thông tin chính xác nếu cần</div>
</body>
</html>