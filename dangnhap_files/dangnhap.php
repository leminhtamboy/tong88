<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0033)https://www.b88ag.com/SignIn.aspx -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sign In</title>
<link href="./dangnhap_files/SignIn.Moon.css" rel="stylesheet" type="text/css">
<style type="text/css"></style>

</head>
<body>
<div id="table_main">
  <h1>Đăng nhập</h1>
  <div id="center">
    <form method="post" name="fLogin" action="./dangnhap_files/dangnhap.htm">
      <table border="0" align="left" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td height="25" align="right" style="color: #FF0000;">&nbsp;</td>
            <td height="25" align="right" style="color: #FF0000;"><table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                  <tr>
                    <td width="5%" height="10" align="right" style="color: #FF0000;">&nbsp;</td>
                    <td width="95%" height="10" align="left" valign="top" style="color: #FF0000;"><div id="errmsg" class="errmsg"></div></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td width="1" align="right" valign="middle"></td>
            <td height="25" align="left" valign="middle"><table width="100%" border="0" cellpadding="1" cellspacing="0">
                <tbody>
                  <tr>
                    <td height="25" align="right" valign="middle"><div id="spnLanguage" class="language" style="background-position: 24px 0px;"></div></td>
                    <td align="left" valign="middle"><select id="selLanguage" onchange="SelectLanguage(this.value)">
                        <option value="0">English</option>
                        <option value="1">繁體中文</option>
                        <option value="4">简体中文</option>
                        <option value="2">日本語</option>
                        <option value="3">ภาษาไทย</option>
                        <option value="5">한국어</option>
                        <option value="6">Tiếng Việt</option>
                      </select>
                      <input type="hidden" value="vi-VN" id="hidLanguage" name="hidLanguage"></td>
                    <td><span id="spnUserName" style="font-weight: bold;">Tên đăng nhập</span></td>
                    <td align="left" valign="middle"><input name="txtUserName" type="text" id="txtUserName" value=""></td>
                    <td valign="top"><span id="spnPassWord" style="font-weight: bold;">Mật khẩu</span></td>
                    <td height="25" align="left" valign="middle"><div id="div1">
                        <input name="pass_temp" id="pass_temp" type="text">
                      </div>
                      <div id="div2" style="display: none">
                        <input name="txtPassWord" id="txtPassWord" type="password" autocomplete="off">
                      </div></td>
                    <td height="25" align="center" valign="middle"><img src="./dangnhap_files/dot.png" width="8" height="21"></td>
                    <td align="left" valign="middle" onclick="GetValidateImage()"><img src="./dangnhap_files/Captcha.ashx" id="imgCode" alt=""></td>
                    <td><span id="spnCaptcha" style="font-weight: bold;">Mã xác nhận</span></td>
                    <td height="25" align="center" valign="middle"><input id="txtCaptcha" name="txtCaptcha" type="text" style="width: 85px;" maxlength="4" autocomplete="off" onkeydown="DoEnter(event, &#39;DoLogin()&#39;)"></td>
                    <td align="right" valign="middle"><span>
                      <input id="btnLogin" type="button" class="buttonsite" value="Đăng nhập" onclick="return DoLogin()">
                      </span></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td align="right" valign="middle"></td>
            <td align="right" valign="middle"><div id="divBrowser" class="text_brow" style="display: none;">Recommended Browsers</div></td>
          </tr>
          <tr>
            <td colspan="2" align="center" valign="middle"><div style="padding-top: 8px; padding-left: 8px;">
                <input type="checkbox" id="showPwdChk">
                <label for="showPwdChk" id="showPwdLabel">Hiển thị mật khẩu</label>
              </div></td>
          </tr>
        </tbody>
      </table>
      <input name="__di" id="__di" type="hidden" value="a6251ce8880f71619eb2db9b1a4b51eb09a2ab0c9792cb88598915ff5271ffd8af368589904e52468f96b372e0beaec1bd1dea15418a065454b130bbddbe27c7234955ca3ed2b642f25d0c2ba1f5fe373fd7cf3fe54f10acab67f73816323a4a51981e9b52306e03ae8c8c29c453430a2a3a36895570884bcf31808d42357b837fce1cd4d3a5ef3ac35029a41331d9c0e1f02ec74229fa024e0e2310f24770393e6f8807ca131c854f040e567a3b09366f144d4d8f80fb12bd6015158e911713f6b786650f8b686407df0c3145268f4a58e11ebc60721f3ac0754b4a5d8e9b685329e3fae32c31e33911982a735a88a81a1d6ee1dd425af08e55b2ef66b1018605af42ea5c2f7a380e80c01473ec524b2da2247ff1fc38b66a406c2ffaf45d429afae93811be4c90817957f5460cfbf6817abda0394158f0837dd145a5dd08376fe4440a18e4b6eb919a0ae8024ca056e462104aab8bf043accb54aa4b768b62ef0936caf31f9ac01da9750fc3fcb8947df7b5610911066b325027fa6451578b1f3805c31dddce60430e58c8eae952c32fabc9f9e97342893f22787fa6282275a80ccd7036fe668fe130a1a48e012b9a18">
    </form>
    <p>&nbsp;</p>
  </div>
</div>
<script src="./dangnhap_files/Core.js" type="text/javascript"></script><script src="./dangnhap_files/SignIn.Moon.js" type="text/javascript"></script><script type="text/javascript" src="./dangnhap_files/activator.ashx"></script> 
<script type="text/javascript">
	var _page = {'language':0,'ip':'171.240.29.48','__utms':'CBA82BB37B76B8A8A98FFBD8905886','domain':'b88ag.com','captcha':'1124'};
</script>
</body>
<div></div>
</html>